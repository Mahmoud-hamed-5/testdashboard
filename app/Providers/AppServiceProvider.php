<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //  this needs to be activated in production environment

        // \Illuminate\Support\Facades\URL::forceScheme('https');


        view()->composer('layouts.dashboard', function ($view) {
            $view->with('current_locale', app()->getLocale());
            $view->with('available_locales', config('app.available_locales'));
            $view->with('locales_flags', config('app.locales_flags'));
        });

        view()->composer('admin', function ($view) {
            $view->with('current_locale', app()->getLocale());
            $view->with('available_locales', config('app.available_locales'));
            $view->with('locales_flags', config('app.locales_flags'));
        });

    }
}
