<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class Provider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        $services = [];
        foreach ($this->services as $tmp_service ) {
            $service = new stdClass;
            $service->id = $tmp_service->id;
            $service->name = $tmp_service->name;
            array_push($services, $service);
        }

        return [
            'id'                =>   $this->id,
            'name'              =>   $this->user->name,
            'gender'            =>   $this->user->gender === null ? '' : $this->user->gender,
            'phone'             =>   $this->user->phone === null ? '' : $this->user->phone,
            'address'           =>   $this->user->address === null ? '' : $this->user->address,
            'type'              =>   $this->type,
            'description'       =>   $this->description === null ? '' : $this->description,
            'rating'            =>   $this->rating(),
            'user_image'        =>   $this->user->user_image === '' ? '' :  asset($this->user->user_image),
            'services'          =>   $services
        ];
    }
}
