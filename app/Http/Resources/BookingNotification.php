<?php

namespace App\Http\Resources;

use App\Models\Booking;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingNotification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $booking = Booking::find($this->booking_id);
        return  [
            'service_name'      =>  $booking->service->name,
            'service_image'     =>  $booking->service->image === '' ? '' : asset($booking->service->image),
            'booking_id'        =>  $booking->id,
            'booking_state'     =>  $this->content,
            'provider_id'       =>  $booking->provider_id,
            'provider_name'     =>  $booking->provider->user->name,

        ];
    }
}
