<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Booking extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id'                    => $this->id,
            'service_name'          => $this->service->name,
            'service_type'          => $this->service->type,
            'provider_id'           => $this->service->provider->user->id,
            'service_image'         => $this->service->image === null ? '' : asset($this->service->image),
            'description'           => $this->description,
            'date'                  => $this->date,
            'min_time'              => $this->min_time === null ? '' : $this->min_time,
            'max_time'              => $this->max_time === null ? '' : $this->max_time,
            'time'                  => $this->time === null ? '' : $this->time,
            'user_name'             => $this->user->name,
            'phone'                 => $this->phone,
            'address'               => $this->address,
            'provider'              => $this->service->provider->user->name,
            'handyman'              => $this->handyman === null ? '' : $this->handyman->user->name,
            'state'                 => $this->state,
            'amount'                => $this->amount === null ? 0 : $this->amount,
            //'payment_status'        => $this->code,
            'reason_of_rejection'   => $this->reason === null ? '' : $this->reason,
            'coupon_code'           => $this->coupon_code === null ? '' : $this->coupon_code,
        ];
    }
}
