<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Coupon extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

       return [
        'id'           => $this->id,
        'name'         => $this->name,
        'code'         => $this->pivot == null ? '' : $this->pivot->code,
        'points'       => $this->points,
        'value'        => $this->value,
        'image'        => $this->image === '' ? '' : asset($this->image)
        ];
    }
}
