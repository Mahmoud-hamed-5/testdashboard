<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Service extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $cities = [];
        $index = 0;
        foreach ($this->cities()->get() as $city ) {
            $cities[$index] = $city->city;
            $index++;
        }

        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'provider'                  => $this->provider->user->name,
            'provider_description'      => $this->provider->description === null ? '' : $this->provider->description,
            'provider_rate'             => $this->provider->rating(),
            'provider_id'               => $this->provider->id,
            'category'                  => $this->category->name,
            'subcategory'               => $this->subcategory === null ? '' : $this->subcategory->name,
            'min_price'                 => $this->min_price,
            'max_price'                 => $this->max_price === null ? 0 : $this->max_price,
            'description'               => $this->description === null ? '' : $this->description,
            'discount'                  => $this->discount === null ? 0 : $this->discount,
            'type'                      => $this->type,
            'featured'                  => $this->featured,
            'rate'                      => $this->rating() === null ? '' : round($this->rating(), 2),
            'image'                     => $this->image === '' ? '' : asset($this->image),
            'locations'                 => $cities

        ];
    }
}
