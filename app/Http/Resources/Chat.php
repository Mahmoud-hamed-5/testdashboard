<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Chat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'sender_name'           => $this['sender']->name,
            'receiver_name'         => $this['receiver']->name,
            'sender_image'          => $this['sender_image'],
            'receiver_image'        => $this['receiver_image'],
            'message'               => $this['chat']->message,
            'status'                => $this['chat']->status,
            'msg_date'              => $this['msg_date'],
        ];

    }
}
