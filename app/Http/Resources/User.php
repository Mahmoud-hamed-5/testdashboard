<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $user_image = asset($this->user_image);
        return [
            'id'                =>   $this->id,
            'name'              =>   $this->name,
            // 'email'        =>    $this->email,
            'register_phone'    =>   $this->register_phone,
            'gender'            =>   $this->gender === null ? '' : $this->gender,
            'phone'             =>   $this->phone === null ? '' : $this->phone,
            'address'           =>   $this->address === null ? '' : $this->address,
            'points'            =>   $this->points === null ? 0 : $this->points,
            'user_image'        =>   $this->user_image === '' ? '' :  asset($this->user_image),
        ];
    }
}
