<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        $subcategories = [];
        foreach ($this->subcategories as $tmp_subcategory) {
            $subcategory = new stdClass;
            $subcategory->id = $tmp_subcategory->id;
            $subcategory->name = $tmp_subcategory->name;
            array_push($subcategories, $subcategory);
        }

        return [
            'id'                =>  $this->id,
            'name_ar'           =>  $this->name_ar,
            'name'              =>  $this->name,
            'details'           =>  $this->details === null ? '' : $this->details,
            'image'             =>  $this->image === '' ? '' :  asset($this->image),
            'subcategories'     =>  $subcategories,
            'services'          =>  Service::collection($this->services)
        ];
    }
}
