<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */


    public function handle($request, Closure $next, $guard = null) {

        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);

        // if (Auth::guard($guard)->check()) {
        //   $role = Auth::user()->roles[0]->name;

        //   switch ($role) {
        //     case 'admin':
        //        return redirect('/admin/index');
        //        break;

        //        case 'handyman':
        //         return redirect('/admin/index');
        //         break;

        //         case 'provider':
        //             return redirect('/admin/index');
        //             break;

        //     default:
        //        return redirect('/home');
        //        break;
        //   }
        // }

        // return $next($request);
      }
}
