<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Notification;
use App\Models\Provider;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{

    public function index()
    {



        return view('chat.index');
    }


    public function chat_data(Request $request)
    {
        $data = Chat::where('sender_id', $request->sender)->where('receiver_id', $request->receiver)
            ->orWhere('sender_id', $request->receiver)->where('receiver_id', $request->sender)
            ->get();

        $chats = [];
        $index = 0;
        foreach ($data as $row) {
            $sender = User::find($row->sender_id);
            $receiver = User::find($row->receiver_id);
            $chats[$index]['sender'] = $sender;
            $chats[$index]['receiver'] = $receiver;
            $chats[$index]['chat'] = $row;

            if ($sender->user_image != '') {
                $sender_image =  asset($sender->user_image);
            } else {
                if ($sender->gender == 'Male') {
                    $sender_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($sender->gender == 'Female') {
                    $sender_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($sender->gender == 'Other') {
                    $sender_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }



            $chats[$index]['sender_image'] = $sender_image;
            $chats[$index]['receiver_image'] = asset($receiver->user_image);

            $msg_date = $row->created_at;
            $now_date = Carbon::now();

            // $loc = new DateTimeZone('Asia/Kuwait');

            // change the timezone of the object without changing its time
            $now_date = $now_date->setTimezone(new DateTimeZone('Asia/Kuwait'));
            $msg_date = $msg_date->setTimezone(new DateTimeZone('Asia/Kuwait'));


            if ($msg_date->format('y m d') === $now_date->format('y m d')) {

                $msg_date = $msg_date->format('h:i A');
            } else {
                $msg_date = $msg_date->format('j-M-y, h:i A');
            }


            $chats[$index]['msg_date'] = $msg_date;
            $index++;
        }


        return $chats;
    }

    public function send_message(Request $request)
    {
        $from_user_id = $request->from_user_id;
        $to_user_id = $request->to_user_id;


        $status = "unseen";
        $chat = Chat::create([
            'sender_id' => $from_user_id,
            'receiver_id' => $to_user_id,
            'message' => $request->message,
            'status' =>  $status,
        ]);
        $chat->save();

        (new FCMController)->message_notification($chat);

    }


    public function update_message_status(Request $request)
    {
        // error_log('success');
        // error_log($request->message_id);
        Chat::where('id', $request->message_id)
            ->update(['status' => $request->status]);

        Notification::where('sender_id', $request->from_user_id)
            ->where('receiver_id', $request->to_user_id)
            ->update([
                'status' => 'seen',
                'count'  => 0,
            ]);
    }


    public function sort_users(Request $request)
    {
        if (Auth::user()->role === 'admin') {
            $users = User::where('id', '!=', Auth::user()->id)
                ->where('status', 'On')
                ->where('role', 'LIKE', $request->userRole)
                ->where('name', 'LIKE', '%' . $request->userName . '%')
                ->orderBy('created_at', 'DESC')
                ->get();
        }
        if (Auth::user()->role === 'provider') {
            $provider = Provider::where('system_user_id', Auth::user()->id)->first();
            $handymen_ids = $provider->handymen->pluck('system_user_id')->toArray();

            if ($request->userRole === '%') {

                $handymen = User::where('id', '!=', Auth::user()->id)
                    ->where('status', 'On')
                    ->where('name', 'LIKE', '%' . $request->userName . '%')
                    ->whereIn('id', $handymen_ids);

                $users = User::where('id', '!=', Auth::user()->id)
                    ->where('status', 'On')
                    ->whereIn('role', ['user', 'admin'])
                    ->where('name', 'LIKE', '%' . $request->userName . '%')
                    ->union($handymen)
                    ->orderBy('created_at', 'DESC')
                    ->get();
            }
            if ($request->userRole === 'handyman') {
                $users = User::where('id', '!=', Auth::user()->id)
                    ->where('status', 'On')
                    ->where('name', 'LIKE', '%' . $request->userName . '%')
                    ->whereIn('id', $handymen_ids)
                    ->get();
            }
            if ($request->userRole === 'user') {
                $users = User::where('id', '!=', Auth::user()->id)
                    ->where('status', 'On')
                    ->where('role', $request->userRole)
                    ->where('name', 'LIKE', '%' . $request->userName . '%')
                    ->orderBy('created_at', 'DESC')
                    ->get();
            }
        }


        $unread = [];
        $last_message = [];
        $ordered_users = [];
        $empty_chat_users = [];
        $index1 = 0;
        $index2 = 0;
        foreach ($users as $user) {

            if ($user->user_image != '') {
                $user_image =  asset($user->user_image);
            } else {
                if ($user->gender == 'Male') {
                    $user_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($user->gender == 'Female') {
                    $user_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($user->gender == 'Other') {
                    $user_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }

            $count = Chat::Where('sender_id', $user->id)->where('receiver_id', Auth::user()->id)
                ->where('status', '!=', 'seen')->count();
            $unread['user_' . $user->id] = $count;

            $me = Auth::user()->id;
            $him = $user->id;
            $msg = Chat::where(function ($query1) use ($me, $him) {
                $query1->where('sender_id', '=', $me)
                    ->where('receiver_id', '=', $him);
            })
                ->orWhere(function ($query2) use ($me, $him) {
                    $query2->where('sender_id', '=', $him)
                        ->where('receiver_id', '=', $me);
                })->orderBy('id', 'desc')->first();

            $last_message['user_' . $user->id] = $msg;

            if (!is_null($msg)) {
                $ordered_users[$index1]['user'] = $user;
                $ordered_users[$index1]['user_image'] = $user_image;
                $ordered_users[$index1]['last_message'] = $msg;
                $ordered_users[$index1]['unread'] = $count;
                $ordered_users[$index1]['last_message_date'] = null;
                $index1++;
            } else {
                $empty_chat_users[$index2]['user'] = $user;
                $empty_chat_users[$index2]['user_image'] = $user_image;
                $empty_chat_users[$index2]['last_message'] = null;
                $empty_chat_users[$index2]['unread'] = $count;
                $empty_chat_users[$index2]['last_message_date'] = null;
                $index2++;
            }
        }

        for ($i = 0; $i < count($ordered_users) - 1; $i++) {
            for ($j = $i + 1; $j < count($ordered_users); $j++) {
                $date1 = $ordered_users[$i]['last_message']->created_at;
                $date2 = $ordered_users[$j]['last_message']->created_at;
                if ($date2->gt($date1)) {
                    $tmp = $ordered_users[$i];
                    $ordered_users[$i] = $ordered_users[$j];
                    $ordered_users[$j] = $tmp;
                }
            }
        }

        $index = 0;
        foreach ($ordered_users as $ordered_user) {
            $msg_date = $ordered_user['last_message']->created_at;
            $now_date = Carbon::now();


            $now_date = $now_date->setTimezone(new DateTimeZone('Asia/Kuwait'));
            $msg_date = $msg_date->setTimezone(new DateTimeZone('Asia/Kuwait'));

            if ($msg_date->format('y m d') === $now_date->format('y m d')) {
                $msg_date = $msg_date->format('h:i A');
            } else {
                $msg_date = $msg_date->format('j-M-y, h:i A');
            }

            $ordered_users[$index]['last_message_date'] = $msg_date;
            $index++;
        }


        $index = count($ordered_users);

        foreach ($empty_chat_users as $empty_chat_user) {
            $ordered_users[$index]['user'] = $empty_chat_user['user'];
            $ordered_users[$index]['user_image'] = $empty_chat_user['user_image'];
            $ordered_users[$index]['last_message'] = $empty_chat_user['last_message'];
            $ordered_users[$index]['unread'] = $empty_chat_user['unread'];
            $ordered_users[$index]['last_message_date'] = null;
            $index++;
        }


        return $ordered_users;
    }
}
