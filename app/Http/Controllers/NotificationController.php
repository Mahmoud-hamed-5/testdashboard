<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\User;
use App\Models\Chat;
use App\Models\Notification;
use App\Models\Service;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{

    public function index()
    {
        $notifications = Notification::where('type' , 'Custom')->get();

        return view('custom-notification.index', compact('notifications'));
    }

    public function create()
    {
        return view('custom-notification.create');
    }

    // broadcast custom notification
    public function broadcast_notification(Request $request)
    {

        $this->validate($request,
            [
                'title'     =>  'required',
                'content'   =>  'required',
                'image'     =>  'image'
            ],
            [
                'title.required'     =>  'required',
                'image.image'   => 'should be image file',
            ]
        );

        $title = $request->title;
        $content = $request->content;
        $image = '';
        if($request->image != '')
        {
            $image =  time() . '-' . $request->image->getClientOriginalName();
            $request->image->move('images/notifications/', $image);

            $image = 'images/notifications/' . $image;

            // $category->update(['image' => $image]);
        }

        $msg = (new FCMController)->custom_notification($title, $content, $image);

        return redirect()->route('notifications.index')->withSuccess($msg);
    }

    public function sort_messages(Request $request)
    {
        $login_user_id = Auth::user()->id;

        $notifications = Notification::where('receiver_id', $login_user_id)
            ->where('type', 'Message')
            ->where('count', '>', 0)
            ->orderBy('updated_at', 'DESC')
            ->get();


        $ordered_messages = [];
        $index = 0;
        foreach ($notifications as $notification) {
            $sender_id = $notification->sender_id;
            $sender = User::where('id', $sender_id)->first();
            $ordered_messages[$index]['notification'] = $notification;
            $ordered_messages[$index]['sender'] = $sender;

            if ($sender->user_image != '') {
                $sender_image =  asset($sender->user_image);
            } else {
                if ($sender->gender == 'Male') {
                    $sender_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($sender->gender == 'Female') {
                    $sender_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($sender->gender == 'Other') {
                    $sender_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }

            $ordered_messages[$index]['sender_image'] = $sender_image;

            $notification_date = $this->change_to_local_time($notification->updated_at, 'Asia/Kuwait');
            $ordered_messages[$index]['notification_date'] = $notification_date;
            $index++;
        }

        for ($i = 0; $i < count($ordered_messages) - 1; $i++) {
            for ($j = $i + 1; $j < count($ordered_messages); $j++) {
                $date1 = $ordered_messages[$i]['notification']->created_at;
                $date2 = $ordered_messages[$j]['notification']->created_at;
                if ($date2->gt($date1)) {
                    $tmp = $ordered_messages[$i];
                    $ordered_messages[$i] = $ordered_messages[$j];
                    $ordered_messages[$j] = $tmp;
                }
            }
        }



        return $ordered_messages;
    }


    public function sort_notifications(Request $request)
    {
        $login_user = Auth::user();
        $login_user_id = Auth::user()->id;

        $notifications = Notification::where('receiver_id', $login_user->id)
            ->where('type', 'Booking')
            ->where('count', '>', 0)
            ->orderBy('updated_at', 'DESC')
            ->get();


        $ordered_notifications = [];
        $seen_notifications = [];
        $index1 = 0;
        $index2 = 0;
        $unseen_notifications_count = 0;
        foreach ($notifications as $notification) {
            $sender_id = $notification->sender_id;
            $sender = User::where('id', $sender_id)->first();

            $booking_id = $notification->booking_id;
            $booking = Booking::find($booking_id);
            $service_id = $booking->service_id;
            $service = Service::find($service_id);

            $notification_date = $this->change_to_local_time($notification->updated_at, 'Asia/Kuwait');



            if ($notification->status === 'unseen') {
                $unseen_notifications_count++;

                $ordered_notifications[$index1]['notification'] = $notification;
                $ordered_notifications[$index1]['sender'] = $sender;
                $ordered_notifications[$index1]['service_name'] = $service->name;
                $ordered_notifications[$index1]['notification_date'] = $notification_date;

                $index1++;
            } else {
                $seen_notifications[$index2]['notification'] = $notification;
                $seen_notifications[$index2]['sender'] = $sender;
                $seen_notifications[$index2]['service_name'] = $service->name;
                $seen_notifications[$index2]['notification_date'] = $notification_date;

                $index2++;
            }

        }

        for ($i = 0; $i < count($ordered_notifications) - 1; $i++) {
            for ($j = $i + 1; $j < count($ordered_notifications); $j++) {
                $date1 = $ordered_notifications[$i]['notification']->updated_at;
                $date2 = $ordered_notifications[$j]['notification']->updated_at;
                if ($date2->gt($date1)) {
                    $tmp = $ordered_notifications[$i];
                    $ordered_notifications[$i] = $ordered_notifications[$j];
                    $ordered_notifications[$j] = $tmp;
                }
            }
        }

        $index = count($ordered_notifications);

        foreach ($seen_notifications as $seen_notification) {
            $ordered_notifications[$index]['notification'] = $seen_notification['notification'];
            $ordered_notifications[$index]['sender'] = $seen_notification['sender'];
            $ordered_notifications[$index]['service_name'] = $seen_notification['service_name'];
            $ordered_notifications[$index]['notification_date'] = $seen_notification['notification_date'];
            $index++;
        }

        $notifications_data['ordered_notifications'] = $ordered_notifications;
        $notifications_data['unseen_notifications_count'] = $unseen_notifications_count;

        return $notifications_data;
    }


    function change_to_local_time($msg_date, $time_zone)
    {
        $now_date = Carbon::now();
        $loc = new DateTimeZone($time_zone);

        // change the timezone of the object without changing its time
        $now_date = $now_date->setTimezone($loc);
        $msg_date = $msg_date->setTimezone($loc);

        if ($msg_date->format('y m d') === $now_date->format('y m d')) {

            $msg_date = $msg_date->format('h:i A');
        } else {
            $msg_date = $msg_date->format('j-M-y, h:i A');
        }

        return $msg_date;
    }
}
