<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Handyman;
use App\Models\Provider;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{

    public function index()
    {
        $users = User::where('role', 'user')->get();


        // $send_data['users'] = $users;
        // $send_data['types'] = [];
        // foreach ($users as $user) {
        //     $provider = Provider::where('system_user_id', $user->id)->first();
        //     if (is_null($provider)) {
        //         $send_data['types'.$user->id] = '-';
        //     }else{
        //         $send_data['types'.$user->id] = $provider->type;
        //     }
        // }
        return view('users.index', compact('users'));

    }


    public function show(User $user)
    {
        return view('users.show', [ 'user' => $user ]);
    }


    public function create()
    {
         return view('users.create', ['roles' => Role::latest()->get()]);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'gender' => $request->gender,
            'phone' => $request->phone == null ? '' : $request->phone,
            'address' => $request->address == null ? '' : $request->address,
            'status' => $request->status,
        ]);

        $user->syncRoles($request->get('role'));

        $role = Role::where('id', $request->get('role'))->value('name');
        $user->update(['role' => $role]);
        if ( $role  === 'handyman'){
            Handyman::updateOrCreate(
                ['system_user_id' => $user->id]
            );
        }

        if ( $role  === 'provider'){
            Provider::updateOrCreate(
                ['system_user_id' => $user->id],
                ['system_user_id' => $user->id, 'type' => $request->type]
            );
        }

        return redirect()->route('users.index')
            ->withSuccess(__('User created successfully'));
    }


    public function edit(User $user)
    {
        $provider = Provider::where('system_user_id', $user->id)->first();

        return view('users.edit', [
            'user' => $user,
            'userRole' => $user->roles->pluck('name')->toArray(),
            'roles' => Role::latest()->get(),
            'provider' => $provider
        ]);
    }


    public function update(User $user, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $user->update([
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => $request->password,
            'gender' => $request->gender,
            // 'phone' =>$request->phone,
            'address' => $request->address,
            'status' => $request->status,
            'points' => $request->points
        ]);

        if ($request->password != '' || $request->password != null)
        {
            $user->update([
                'password' => $request->password,
            ]);
        }

        return redirect()->route('users.index')
            ->withSuccess(__('User updated successfully'));
    }

    public function disable($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $user->update(['status' => 'Off']);

         return redirect()->route('users.index')
            ->withSuccess(__('User Disabled successfully'));
    }

    public function enable($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $user->update(['status' => 'On']);

         return redirect()->route('users.index')
            ->withSuccess(__('User Enabled successfully'));
    }


    public function destroy(User $user)
    {

    }
}
