<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Coupon;
use App\Models\Service;
use App\Models\User;
use App\Notifications\SendPushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Kutia\Larafirebase\Facades\Larafirebase;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $services = Service::where('status', 'On')->get();

        return view('site.home', compact('services'));
    }


    public function coupons()
    {

        $coupons = Coupon::where('status', 'On')->get();

        return view('site.coupon', compact('coupons'));
    }


    public function customer_coupons()
    {

        $user = User::find(Auth::user()->id);

        $coupons = $user->coupons()->get();


        return view('site.customer-coupons', compact('coupons'));
    }


    public function buy_coupon(Coupon $coupon)
    {
        //$user = Auth::user();
        $user = User::find(Auth::user()->id);

        if ($user->points >= $coupon->points)
        {
            $user->coupons()->attach($coupon);
            $user->update([
                'points' => ($user->points - $coupon->points),
            ]);

            return redirect()->route('customer.coupons')
            ->withSuccess(__('coupon bought successfully'));
        }




        return redirect()->back()
            ->withSuccess(__('You dont have enough points to buy this coupon'));
    }

    public function my_bookings()
    {
        $bookings = Booking::where('user_id', Auth::user()->id)->get();
        return view('site.customer-bookings', compact('bookings'));
    }

    public function response(Booking $booking)
    {

        //return view('site.booking-response', compact('booking'));
    }





}
