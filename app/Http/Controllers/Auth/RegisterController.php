<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Throwable;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;


   protected function authenticated(Request $request, $user)
    {

        try {
            $role = $user->role;

            switch ($role) {
                case 'admin':
                    return redirect('/admin/index');
                    break;
                case 'user':
                    return redirect('/home');
                    break;

                    case 'handyman':
                    return redirect('/admin/index');
                    break;

                default:
                    return redirect('/home');
                    break;
            }

        } catch (Throwable $e) {
            report($e);

            return redirect('/home');
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

            //'gender'       =>   'required|in:Male,Female',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'gender' => $data['gender'],
            'phone' => $data['phone'] == null ? 0 : $data['phone'],
            'address' => $data['address'] == null ? '' : $data['address'],
        ]);

        $role_id = Role::where('name', 'user')->value('id');

        $user->assignRole([$role_id]);

        return $user;
    }
}


