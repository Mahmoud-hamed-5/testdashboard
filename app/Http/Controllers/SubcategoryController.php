<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SubcategoryController extends Controller
{

    public function index()
    {
        $subcategories = Subcategory::all();

        return view('subcategory.index', compact('subcategories'));
    }

    public function show(Subcategory $subcategory)
    {
        return view('subcategory.show', compact('subcategory'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('subcategory.create', compact('categories'));
    }


    public function store(Request $request)
    {

        $this->validate($request,
            ['image' =>  'image|mimes:jpg,png,jpeg|max:2048' ,
             'category' => 'required'
            ],
            [
                'image.image' => 'image',
                'image.mimes' => 'mimes',
                'image.max' => 'max',
                'category_id.required' => 'required'
            ]
        );

        $subcategory = Subcategory::create([
            'name' => $request->name,
            'details' => $request->details,
            'category_id' => $request->category,
            'status' => $request->status,
        ]);

        if($request->image != '')
        {
            $image =  time() . '-' . $request->image->getClientOriginalName();
            $request->image->move('images/subcategories/', $image);
            $image = 'images/subcategories/' . $image;

            $subcategory->update(['image' => $image]);
        }



        return redirect()->route('subcategories.index')
            ->withSuccess(__('Sub Category created successfully'));
    }


    public function edit(Subcategory $subcategory)
    {
        $categories = Category::all();
        return view('subcategory.edit', compact( 'subcategory' ,'categories'));
    }


    public function update(Request $request, Subcategory $subcategory)
    {
        $this->validate($request,[
            'name' => 'required',
            //'category' => 'required',
            'image' =>  'image|mimes:jpg,png,jpeg|max:2048'
        ]);

        $old_image = $subcategory->image;
        $new_image = null;
        if($request->has('image'))
        {
            $image = $request->image;
            $new_image = time() . '-' . $request->image->getClientOriginalName();
            $image->move('images/subcategory/',$new_image);
            //$post->photo = 'uploads/posts/'.$newPhoto;
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/subcategory/' . $new_image;

        $subcategory->update([
            'name' => $request->name,
            'details' => $request->details,
            'status' => $request->status,
            //'category_id' => $request->category,
            'image' => $image_Store
        ]);

        return redirect()->route('subcategories.index')
            ->withSuccess(__('Sub Category updated successfully'));
    }


    public function disable($subcategory_id)
    {
        $subcategory = Subcategory::where('id', $subcategory_id)->first();
        $subcategory->update(['status' => 'Off']);

         return redirect()->route('subcategories.index')
            ->withSuccess(__('Sub Category Disabled successfully'));
    }

    public function enable($subcategory_id)
    {
        $subcategory = Subcategory::where('id', $subcategory_id)->first();
        $subcategory->update(['status' => 'On']);

         return redirect()->route('subcategories.index')
            ->withSuccess(__('Sub Category Enabled successfully'));
    }

    public function destroy(Subcategory $subcategory)
    {
        //
    }
}
