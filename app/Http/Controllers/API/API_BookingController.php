<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FCMController;
use App\Http\Resources\Booking as ResourcesBooking;
use App\Models\Booking;
use App\Models\Booking_images;
use App\Models\Notification;
use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class API_BookingController extends BaseController
{

    public function index()
    {
        //
    }

    public function my_bookings()
    {

        $user = User::find(Auth::user()->id);

        $bookings = Booking::where('user_id', Auth::user()->id)->get();


        return $this->sendResponse(ResourcesBooking::collection($bookings), 'Bookings retrieved successfully');
    }


    public function book(Request $request)
    {
        $min_time = $request->min_time;
        $max_time = $request->max_time;

        $min_time = Carbon::parse($min_time)->format('H:i');
        $max_time = Carbon::parse($max_time)->format('H:i');

        $this->validate($request,
            [
                'date' =>  'required|date',
                'min_time' => 'required',
                'max_time' => 'required|after:' . $min_time,
                'files' =>  'array|max:3',
                'description' => 'required',
                'phone' => 'required|min:10|max:10|regex:/^[0-9]+$/',
                'address' => 'required',
                'lat' => 'required',
                'lng' => 'required'
            ],
            [
                'date.required' => 'You must provide a Date for your request',
                'min_time.required'     =>  'min_time is required',

                'max_time.required'     =>  'max_time is required',

                'max_time.after' => 'max_time must be after ' . $min_time,
                'files.max' => 'max 3 images',
                'description.required' => 'You must provide a Description',
                'phone.required' => 'You must provide your Phone Number',
                'phone.regex' => 'Phone field can be only numbers',
                'phone.min' => 'Phone field must be only 10 numbers',
                'phone.max' => 'Phone field must be only 10 numbers',
                'address.required' => 'You must provide your Address',
            ]
        );


        $user = User::find(Auth::user()->id);

        $service = Service::find($request->service_id);
        if (is_null($service)) {
            return $this->sendError('The Service you asked for is not Available');
        }

        $coupon = $user->coupons()->where('code', $request->coupon_code)->first();
        if(!is_null($request->coupon_code)) {
            if ( is_null($coupon) || $coupon->pivot->used === 'Yes' ) {

                return $this->sendError('The Coupon you provided is Invalid');
            }
        }


        $books = Booking::where('service_id', $service->id)->where('user_id', $user->id)->get();
        foreach ($books as $booked) {
            if (($booked->state === 'Pending') || ($booked->state === 'Pending Approval')
                || ($booked->state === 'Approved') || ($booked->state === 'Ongoing')){
                    return $this->sendError('You Already Booked this Service');
            }
        }


        $booking = Booking::create([
            'service_id'        => $request->service_id,
            'date'              => $request->date,
            'min_time'          => $request->min_time,
            'max_time'          => $request->max_time,
            'description'       => $request->description,
            'phone'             => $request->phone,
            'address'           => $request->address,

            'lat'               => $request->lat,
            'lng'               => $request->lng,

            'coupon_code'       => $request->coupon_code,

            'user_id'           => Auth::user()->id,
            'provider_id'       => $service->provider_id,

            'state'             => 'Pending',
            'payment_status'    => 'pending'
        ]);

        if (! is_null($coupon)){
            $coupon->pivot->used = 'Yes';
            $coupon->pivot->save();
        }


        if ($request->has('files')) {
            foreach ($request->file('files') as $file) {

                $new_image = time() . '-' . $file->getClientOriginalName();
                $file->move('images/bookings/',$new_image);

                $image_store = 'images/bookings/' . $new_image;

                Booking_images::updateOrCreate(['url' => $image_store , 'booking_id' => $booking->id]);
            }
        }

        // Notify the Provider ... and the Admin
        (new FCMController)->booking_notification($booking , $booking->service->provider->system_user_id, 'New Booking', 1);

        return $this->sendResponse(new ResourcesBooking($booking), 'Service Booked successfully');
    }


    // Note:
    // Disable Customer Approve,Reject Functions

    /*
    public function approve(Booking $booking)
    {
        if ($booking->state != 'Pending Approval') {
            return $this->sendError('You Cannot Take This Action.');
        }

        if ($booking->service->provider->type === 'Freelance') {
            $booking->update(['state' => 'Ongoing']);
            $msg = 'Booking is Ongoing !';
        }

        if ($booking->service->provider->type === 'Company') {
            $booking->update(['state' => 'Approved']);
            $msg = 'Booking Approved, Waitting for Handyman Assignment !';
        }

        Notification::where('sender_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['content' => $booking->state]);

        // Notify The Provider
        $result = (new FCMController)->booking_notification($booking , $booking->service->provider->system_user_id, 'Booking Approved');

        return $this->sendResponse(new ResourcesBooking($booking), $msg);
    }

    public function reject(Request $request, Booking $booking)
    {
        if ($booking->state != 'Pending Approval') {
            return $this->sendError('You Cannot Take This Action.');
        }

        $reason = 'Rejected By Customer : ' . $request->reason;
        $booking->update(['state' => 'Cancelled', 'reason' => $reason]);

        if(!is_null($booking->coupon))
        {
            DB::table('coupon_user')->where('id', $booking->coupon_user_id)->update(['used' => 'No']);
        }

        Notification::where('sender_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['content' => $booking->state]);

        // Notify The Provider
        $result = (new FCMController)->booking_notification($booking , $booking->service->provider->system_user_id, 'Booking Cancelled');
        $msg = 'You Cancelled the Booking.';
        return $this->sendResponse(new ResourcesBooking($booking), $msg);
    }

    */

}
