<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Category as ResourcesCategory;
use App\Models\Category;
use Illuminate\Http\Request;

class API_CategoryController extends BaseController
{
    public function index()
    {
        $categories = Category::where('status', 'On')->get();
        return $this->sendResponse(ResourcesCategory::collection($categories), 'All Categories retrieved successfully');
    }

    public function show($category)
    {

        $category = Category::where('id' , $category)->orWhere('name', $category)->where('status' , 'On')->first();

        if (is_null($category)) {
            return $this->sendError('Category not found');
        }
        return $this->sendResponse(new ResourcesCategory($category), 'Category retrieved successfully');
    }
}
