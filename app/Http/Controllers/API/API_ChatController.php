<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FCMController;
use App\Http\Resources\Chat as ResourcesChat;
use App\Models\Chat;
use App\Models\User;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class API_ChatController extends BaseController
{

    public function chat_data(Request $request)
    {
        $sender_id = Auth::user()->id;

        $data = Chat::where('sender_id', $sender_id)->where('receiver_id', $request->receiver)
            ->orWhere('sender_id', $request->receiver)->where('receiver_id', $sender_id)
            ->get();

        $chats = [];
        $index = 0;
        foreach ($data as $row) {
            $sender = User::find($row->sender_id);
            $receiver = User::find($row->receiver_id);
            $chats[$index]['sender'] = $sender;
            $chats[$index]['receiver'] = $receiver;

            if ($row->receiver_id === $sender_id && $row->status != 'seen')
            {
                $this->update_message_status($row->id);
                $row->status = 'seen';
            }

            $chats[$index]['chat'] = $row;

            if ($sender->user_image != '') {
                $sender_image =  asset($sender->user_image);
            } else {
                if ($sender->gender == 'Male') {
                    $sender_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($sender->gender == 'Female') {
                    $sender_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($sender->gender == 'Other') {
                    $sender_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }

            if ($receiver->user_image != '') {
                $receiver_image =  asset($receiver->user_image);
            } else {
                if ($receiver->gender == 'Male') {
                    $receiver_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($receiver->gender == 'Female') {
                    $receiver_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($receiver->gender == 'Other') {
                    $receiver_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }


            $chats[$index]['sender_image'] = $sender_image;
            $chats[$index]['receiver_image'] = $receiver_image;

            $msg_date = $row->created_at;

            // change the timezone of the object without changing its time
            $msg_date = $this->change_to_local_time($msg_date, 'Asia/Kuwait');

            $chats[$index]['msg_date'] = $msg_date;
            $index++;
        }



        return $this->sendResponse(ResourcesChat::collection($chats), 'Chats retrieved successfully');
    }


    public function update_message_status($message_id)
    {

        Chat::where('id', $message_id)
            ->update(['status' => 'seen']);


    }

    function send_message(Request $request)
    {

        $from_user_id = $request->from_user_id;
        $to_user_id = $request->to_user_id;


        $status = "unseen";
        $chat = Chat::create([
            'sender_id' => $from_user_id,
            'receiver_id' => $to_user_id,
            'message' => $request->message,
            'status' =>  $status,
        ]);
        $chat->save();

        (new FCMController)->message_notification($chat);

        
    }


    function change_to_local_time($msg_date, $time_zone = 'Asia/Kuwait')
    {
        $now_date = Carbon::now();
        $loc = new DateTimeZone($time_zone);

        // change the timezone of the object without changing its time
        $now_date = $now_date->setTimezone($loc);
        $msg_date = $msg_date->setTimezone($loc);

        if ($msg_date->format('y m d') === $now_date->format('y m d')) {

            $msg_date = $msg_date->format('h:i A');
        } else {
            $msg_date = $msg_date->format('j-M-Y , h:i A');
        }

        return $msg_date;
    }
}
