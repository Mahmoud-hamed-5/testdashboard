<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Setting;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Str;

class AuthController extends BaseController
{


    public function register_phone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'register_phone' => 'required|unique:users,register_phone|min:10|max:10|regex:/^[0-9]+$/',
            'country_code'  =>  'required',
            'register_phone' => 'required|unique:users,register_phone',
            // 'password' => 'required',
            // 'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Please Validate', $validator->errors());
        }

        $input = $request->all();


        $email_code = Str::random(6);
        $email = str_replace(' ', '', $request->name) . '@' . $email_code . '.com';
        $input['email'] = $email;
        $input['password'] = 'master password';

        $user = User::create($input);


        $response = $this->send_OTP($request->country_code, $request->register_phone);

        if ($response['status'] == 400) {

            return $this->sendError('false', 'Invalid Parameters.', 400);
        }
        if ($response['status'] == 403) {

            return $this->sendError('false', 'Access Denied.', 403);
        }
        if ($response['status'] == 200) {

            return $response;
        } else {
            return $this->sendError('false', 'Internal Server Error, Please try again later.', 500);
        }
    }



    public function send_OTP($country_code, $phone)
    {

        if ($country_code != '963') {
            $response['status'] = 200;
            $otp = Setting::where('key', 'otp')->value('value');
            $response['message'] = 'Your OTP is ' . $otp;
            $response['otp'] = $otp;
        } else {
            $message = [
                "secret" => Setting::where('key', 'api')->value('value'), // your API secret from (Tools -> API Keys) page
                "type" => "sms",
                "mode" => "devices",
                "device" => Setting::where('key', 'device')->value('value'),
                "sim" => 1,
                "phone" => $phone,
                "message" => "Your OTP is {{otp}}"
            ];

            $cURL = curl_init("https://sms.dr-social.com/api/send/otp");
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cURL, CURLOPT_POSTFIELDS, $message);
            $server_response = curl_exec($cURL);
            curl_close($cURL);

            $result = json_decode($server_response, true);

            $response['status'] = $result['status'];
            $response['message'] = $result['message'];
            $response['otp'] = $result['data'];
        }
        // do something with response
        return ($response);
    }

    public function verify_OTP(Request $request)
    {
        $result = [];
        if ($request->country_code != '963') {
            $otp = Setting::where('key', 'otp')->value('value');
            if ($request->otp === $otp) {
                $result['status'] = 200;
            } else {
                $result['status'] = 403;
            }
        } else {
            $apiSecret = Setting::where('key', 'api')->value('value'); // your API secret from (Tools -> API Keys) page
            $otp = $request->otp;

            $cURL = curl_init();
            curl_setopt($cURL, CURLOPT_URL, "https://sms.dr-social.com/api/get/otp?secret={$apiSecret}&otp={$otp}");
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($cURL);
            curl_close($cURL);

            $result = json_decode($response, true);
        }
        // do something with response

        if ($result['status'] === 200) {

            $user = User::where('country_code', $request->country_code)
                ->where('register_phone', $request->register_phone)
                ->first();

            if (!$user->is_verified) {
                $user->update(['is_verified' => 1]);
                $gift_points = Setting::where('key', 'gift')->value('value');
                if (!is_null($gift_points)) {
                    $user->update(['points' => $gift_points]);
                }
            }

            $result['token'] = $user->createToken('My%New%Token%4%Handyman%Service')->accessToken;
            $result['name'] = $user->name;
            $result['id']  =   $user->id;

            $fcm_token = 'Mobile' . $request->fcm_token;
            DB::table('fcm_user')->updateOrInsert(
                ['user_id' => $user->id],
                ['user_id' => $user->id, 'fcm_token' => $fcm_token]
            );

            return $result;
        } else if ($result['status'] === 403) {

            // Ask the user to try again
            return $result;
        } else {
            return $this->sendError('false', 'Internal Server Error, Please try again later.', 500);
        }
    }

    public function login_phone(Request $request)
    {

        $user = User::where('country_code', $request->country_code)
            ->where('register_phone', $request->register_phone)
            ->first();

        if (!is_null($user)) {

            // $success['token'] = $user->createToken('My%New%Token%4%Handyman%Service')->accessToken;
            // $success['name'] = $user->name;
            // $success['id']  =   $user->id;

            // $fcm_token = 'Mobile' . $request->fcm_token;

            // DB::table('fcm_user')->updateOrInsert(
            //     ['user_id' => $user->id],
            //     ['user_id' => $user->id, 'fcm_token' => $fcm_token]
            // );
            // $response['success'] = true;
            $response = $this->send_OTP($request->country_code, $request->register_phone);

            if ($response['status'] == 400) {

                return $this->sendError('false', 'Invalid Parameters.', 400);
            }
            if ($response['status'] == 403) {

                return $this->sendError('false', 'Access Denied.', 403);
            }
            if ($response['status'] == 200) {

                return $response;
            } else {
                return $this->sendError('false', 'Internal Server Error, Please try again later.', 500);
            }
            // return $this->sendResponse([], 'Phone number is correct');
        } else {
            return $this->sendError('Please check your phone number', ['error' => 'Unauthorized']);
        }
    }




    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Please Validate', $validator->errors());
        }

        $input = $request->all();

        // $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        $success['token'] = $user->createToken('My%New%Token%4%Handyman%Service')->accessToken;
        $success['name'] = $user->name;

        return $this->sendResponse($success, 'User registered successfully');
    }


    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = User::where('id', Auth::user()->id)->first();

            $success['token'] = $user->createToken('My%New%Token%4%Handyman%Service')->accessToken;
            $success['name'] = $user->name;
            return $this->sendResponse($success, 'User login successfully');
        } else {
            return $this->sendError('Please check your email|password', ['error' => 'Unauthorized']);
        }
    }
}
