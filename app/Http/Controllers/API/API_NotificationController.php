<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookingNotification as ResourceBookingNotification;
use App\Http\Resources\CustomNotification as ResourceCustomNotification;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class API_NotificationController extends BaseController
{

    public function user_custom_notifications()
    {
        $user_id = Auth::user()->id;
        $all_custom_notifications = Notification::where('type', 'Custom')->get();
        $user_custom_notifications = [];
        foreach ($all_custom_notifications as $notification) {
            $ids = explode(',',$notification->broadcast_ids);
            if (in_array($user_id, $ids)) {
                array_push($user_custom_notifications, $notification);
            }
        }

        return $this->sendResponse(ResourceCustomNotification::collection($user_custom_notifications), 'success');
    }

    public function custom_notification($custom_id)
    {
        $user_id = Auth::user()->id;
        $custom_notification = Notification::find($custom_id);

        if (in_array($user_id, explode(',', $custom_notification->broadcast_ids))) {
            return $this->sendResponse(new ResourceCustomNotification($custom_notification), 'success');
        }

        return $this->sendError('You are not allowed to see this custom', null, 404);
    }

    public function user_booking_notifications()
    {
        $user_id = Auth::user()->id;
        $admin_user_id = User::where('role', 'Admin')->value('id');
        $user_booking_notifications = Notification::where('type', 'Booking')
            ->where('sender_id', $user_id)
            ->where('receiver_id', '!=', $admin_user_id)
        ->get();


        return $this->sendResponse(ResourceBookingNotification::collection($user_booking_notifications), 'success');
    }

}
