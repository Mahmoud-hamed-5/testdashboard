<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Provider as ResourcesProvider;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class API_ProviderController extends BaseController
{

    public function provider_info($provider_id)
    {
        $provider = Provider::find($provider_id);

        if (!is_null($provider)) {
            return $this->sendResponse(new ResourcesProvider($provider), 'Provider Info retreived');
        }else{
            return $this->sendError([], 'Provider id is Invalid', 404);
        }
    }

    public function rate_provider(Request $request)
    {
        $request->validate([
            'rate'          =>  'required|numeric|min:0|max:5',
            'provider_id'    =>  'required'
        ]);

        $user_id = Auth::user()->id;
        $provider_id = $request->provider_id;
        $rate = $request->rate;

        DB::table('providers_reviews')->updateOrInsert(
            //condition to find if row exist
            ['provider_id' =>  $provider_id, 'user_id' => $user_id],

            // fields to update or create
            ['provider_id'    =>  $provider_id,
            'user_id'       =>  $user_id,
            'rate'          =>  $rate
            ]
        );

        return $this->sendResponse([],'Provider rated successfully');
    }
}
