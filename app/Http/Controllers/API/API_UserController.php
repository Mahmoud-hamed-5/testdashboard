<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Resources\User as ResourcesUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class API_UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function show()
    {

        $user = User::find(Auth::user()->id);

        if (is_null($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse(new ResourcesUser($user), 'User retrieved successfully');
    }



    public function update(Request $request)
    {

        $user = User::find(Auth::user()->id);


        if (is_null($user)) {
            return $this->sendError('User not found');
        }

        $this->validate($request,[
            'name' => 'required',
            'gender' => 'in:Male,Female,Other',
            // 'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            'user_image' =>  'image|mimes:jpg,png,jpeg|max:2048'
            ],
            [
            'name.required' => 'Name field is required',
            'gender.in' => 'Gender must be on of (Male,Female,Other)',
            // 'phone.regex' => 'Phone field can be only numbers',
            // 'phone.min' => 'Phone field must be only 10 numbers',
            // 'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $old_image = $user->user_image;
        $new_image = null;
        if($request->has('user_image'))
        {
            $image = $request->user_image;
            $new_image = time() . '-' . $request->user_image->getClientOriginalName();
            $image->move('images/profile-images/',$new_image);
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/profile-images/' . $new_image;

        $user->update([
            'name' => $request->name,
            //'email' => $request->email,
            // 'password' => $request->password,
            'gender' => $request->gender,
            'address' => $request->address,
            // 'phone' => $request->phone,
            'user_image' => $image_Store
        ]);

        // if ($request->password != '' || $request->password != null)
        // {
        //     $user->update([
        //         'password' => $request->password,
        //     ]);
        // }


        return $this->sendResponse(new ResourcesUser($user), 'User updated successfully');

    }


}
