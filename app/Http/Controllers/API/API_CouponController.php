<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\Coupon as ResourcesCoupon;
use App\Models\Coupon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class API_CouponController extends BaseController
{

    public function index()
    {
        $coupons = Coupon::where('status', 'On')->get();

        return $this->sendResponse(ResourcesCoupon::collection($coupons), 'Coupons retrieved successfully');
    }

    public function buy_coupon($coupon_id)
    {
        //$user = Auth::user();
        $user = User::find(Auth::user()->id);
        $coupon = Coupon::find($coupon_id);

        if (is_null($coupon) || $coupon->status === 'Off') {
            return $this->sendError('Coupon not found');
        }

        if ($user->points >= $coupon->points) {
            $code = Str::random(6);
            while (!is_null(DB::table('coupon_user')->where('code', $code)->first())) {
                $code = Str::random(6);
            }

            $user->coupons()->attach([
                $coupon->id => [
                    'used' => 'No',
                    'code' => $code
                ]
            ]);

            $user->update([
                'points' => ($user->points - $coupon->points),
            ]);

            return $this->sendResponse(new ResourcesCoupon($coupon), 'Coupon Bought successfully');
        }

        return $this->sendError('You dont have enough points to buy this coupon');
    }

    public function my_coupons()
    {

        $user = User::find(Auth::user()->id);

        $coupons = $user->coupons()->where('used', 'No')->get();

        return $this->sendResponse(ResourcesCoupon::collection($coupons), 'User Coupons retrieved successfully');
    }

    public function show($id)
    {
        //
    }
}
