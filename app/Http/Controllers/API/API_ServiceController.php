<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Http\Resources\Service as ResourcesService;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class API_ServiceController extends BaseController
{
    public function index()
    {
        $services = Service::where('status', 'On')->get();
        return $this->sendResponse(ResourcesService::collection($services), 'All Services retrieved successfully');
    }

    public function featured()
    {
        $services = Service::where('status', 'On')->where('featured', 1)->get();
        return $this->sendResponse(ResourcesService::collection($services), 'Featured Services retrieved successfully');
    }

    public function search_by_city(Request $request)
    {
        $city = $request->city;
        $services = Service::where('status', 'On')
            ->whereHas('cities', function ($query) use($city) {
                $query->where('city', $city);
                })
            ->get();
        return $this->sendResponse(ResourcesService::collection($services), 'Services retrieved successfully');
    }

    public function featured_city(Request $request)
    {
        $city = $request->city;
        $services = Service::where('status', 'On')->where('featured', 1)
            ->whereHas('cities', function ($query) use($city) {
                $query->where('city', $city);
                })
        ->get();
        
        return $this->sendResponse(ResourcesService::collection($services), 'Featured Services retrieved successfully');
    }

    public function show($service_id)
    {

        $service = Service::where('id' , $service_id)->where('status' , 'On')->first();

        if (is_null($service)) {
            return $this->sendError('Service not found');
        }
        return $this->sendResponse(new ResourcesService($service), 'Service retrieved successfully');
    }

    public function rate_service(Request $request)
    {
        $request->validate([
            'rate'          =>  'required|numeric|min:0|max:5',
            'service_id'    =>  'required'
        ]);

        $user_id = Auth::user()->id;
        $service_id = $request->service_id;
        $rate = $request->rate;

        DB::table('services_reviews')->updateOrInsert(
            //condition to find if row exist
            ['service_id' =>  $service_id, 'user_id' => $user_id],

            // fields to update or create
            ['service_id'    =>  $service_id,
            'user_id'       =>  $user_id,
            'rate'          =>  $rate
            ]
        );

        return $this->sendResponse([],'Service rated successfully');
    }

}
