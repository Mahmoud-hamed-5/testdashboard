<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function index()
    {
        $settings = Setting::all();
        return view('setting.index', compact('settings'));
    }


    public function create()
    {
        //return view('setting.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            //'key' => 'required',
            //'value' => 'required'
        ]);

        $setting = Setting::create([
            //'key' => $request->key,
            // 'value' => $request->value,
            // 'status' => $request->status,
        ]);

        return redirect()->route('settings.index')
            ->withSuccess(__('Setting created successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit(Setting $setting)
    {
        return view('setting.edit', compact('setting'));
    }


    public function update(Request $request, Setting $setting)
    {
        $this->validate($request,[
            //'key' => 'required',
            'value' => 'required'
        ]);

        $setting->update([
            //'key' => $request->key,
            'value' => $request->value,
            //'status' => $request->status,
        ]);

        return redirect()->route('settings.index')
            ->withSuccess(__('Setting updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function disable($setting_id)
    {
        $setting = Setting::where('id', $setting_id)->first();
        $setting->update(['status' => 'Off']);

         return redirect()->route('settings.index')
            ->withSuccess(__('Setting Disabled successfully'));
    }

    public function enable($setting_id)
    {
        $setting = Setting::where('id', $setting_id)->first();
        $setting->update(['status' => 'On']);

         return redirect()->route('settings.index')
            ->withSuccess(__('Setting Enabled successfully'));
    }
}
