<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Provider;
use App\Models\Service;
use App\Models\Subcategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{

    public function index()
    {
        $services = Service::all();

        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');

            $services = Service::where('provider_id', $provider_id)->get();
        }

         // calculate the rate of each provider
        $services_ratings = [];
        foreach ($services as $service) {
            $service_rating = $service->rating();
            $services_ratings['service'.$service->id] = $service_rating;
        }


        return view('service.index', compact('services', 'services_ratings'));
    }


    public function show(Service $service)
    {
        return view('service.show', compact('service'));
    }


    public function create()
    {
        $providers = Provider::all();
        $categories = Category::all();
        $cities = City::where('country','sy')->get();
        return view('service.create', compact('providers', 'categories', 'cities'));
    }


    public function store(Request $request)
    {

        $this->validate($request,
            ['image' =>  'image|mimes:jpg,png,jpeg|max:2048' ,
             'category' => 'required',
             'name'   => 'required',
             //'provider' => 'required',
             'type' => 'required',
             'cities' => 'required|array|min:1',
             'min_price' => 'required'
            ],
            [
                'image.image' => 'image',
                'image.mimes' => 'mimes',
                'image.max' => 'max',
                'category.required' => 'required'
            ]
        );

        $provider_id = 0;
        $route = '';
        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');
            $route = 'provider.services.index';
        }
        if (Auth::user()->role === 'admin') {
            $provider_id = $request->provider;
            $route = 'services.index';
        }

        $service = Service::create([
            'name' => $request->name,
            'provider_id' => $provider_id,
            'category_id' => $request->category,
            'subcategory_id' => $request->subcategory,
            'description' => $request->description == null ? '' : $request->description,
            'type' => $request->type,
            'featured'  =>  $request->featured === null ? false : true,
            'min_price' => $request->min_price,
            'max_price' => $request->type == 'Variant' ? $request->max_price : 0,
            'discount' => $request->discount,
            'status' => Auth::user()->role === 'admin' ? $request->status : 'Off',

        ]);

        $service->cities()->attach($request->cities);

        if($request->image != '')
        {
            $image =  time() . '-' . $request->image->getClientOriginalName();
            $request->image->move('images/services/', $image);
            $image = 'images/services/' . $image;

            $service->update(['image' => $image]);
        }


        return redirect()->route($route)
            ->withSuccess(__('Service created successfully'));
    }



    public function edit(Service $service)
    {
        $providers = Provider::all();
        $categories = Category::all();
        $cities = City::where('country','sy')->get();
        $service_cities = $service->cities()->get()->pluck('id')->toArray();
        return view('service.edit', compact('service','providers', 'categories','cities','service_cities'));
    }


    public function update(Request $request, Service $service)
    {
        $this->validate($request,
            ['image' =>  'image|mimes:jpg,png,jpeg|max:2048' ,
             'category' => 'required',
             'name'   => 'required',
             'type' => 'required',
             'min_price' => 'required',
             //'max_price' => 'required'
            ],
            [
                'image.image' => 'image',
                'image.mimes' => 'mimes',
                'image.max' => 'max',
                'category.required' => 'required'
            ]
        );

        $route = 'services.index';
        if (Auth::user()->role === 'provider') {
            // $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');
            $route = 'provider.services.index';
        }

        $old_image = $service->image;
        $new_image = null;
        if($request->has('image'))
        {
            $image = $request->image;
            $new_image = time() . '-' . $request->image->getClientOriginalName();
            $image->move('images/services/',$new_image);
            //$post->photo = 'uploads/posts/'.$newPhoto;
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/services/' . $new_image;

        $service->update([
            'name' => $request->name,
            'category_id' => $request->category,
            'subcategory_id' => $request->subcategory,
            'description' => $request->description == null ? '' : $request->description,
            'type' => $request->type,
            'featured'  =>  $request->featured === null ? false : true,
            'min_price' => $request->min_price,
            'max_price' => $request->type === 'Variant' ? $request->max_price : 0,
            'discount' => $request->discount,
            'status' => Auth::user()->role === 'admin' ? $request->status : 'Off', //????
            'image' => $image_Store

        ]);

        $service->cities()->sync($request->cities);

        return redirect()->route($route)
            ->withSuccess(__('Service updated successfully'));
    }


    public function disable($service_id)
    {
        $service = Service::where('id', $service_id)->first();
        $service->update(['status' => 'Off']);

         return redirect()->route('services.index')
            ->withSuccess(__('Service Disabled successfully'));
    }

    public function enable($service_id)
    {
        $service = Service::where('id', $service_id)->first();
        $service->update(['status' => 'On']);

         return redirect()->route('services.index')
            ->withSuccess(__('Service Enabled successfully'));
    }


    public function destroy(Service $service)
    {
        //
    }


    public function subcategories_list($category_id){

        $subcategories = array();

        $all_subcategories = Subcategory::where('category_id', $category_id)->get();

        foreach ($all_subcategories as $subcategory) {
            $subcat = [];
            $subcat['id'] = $subcategory->id;
            $subcat['name'] = $subcategory->name;
            array_push($subcategories,$subcat);
        }

        return  $subcategories;
    }

}
