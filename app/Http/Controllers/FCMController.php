<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Chat;
use App\Models\Notification;
use App\Models\Provider;
use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Kutia\Larafirebase\Facades\Larafirebase;
use FCM;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use stdClass;

class FCMController extends Controller
{
    public function index(Request $req)
    {
        $input = $req->all();
        return response()->json($input);
    }


    public function updateToken(Request $request)
    {
        try {
            $user_id = $request->user()->id;
            $token = $user_id . $request->token;

            $this->delete_other_users_token($token, $user_id);

            $row = DB::table('fcm_user')->where('user_id', Auth::user()->id)->where('fcm_token', $token)->first();

            if (is_null($row)) {
                DB::table('fcm_user')->insert([
                    'user_id' => Auth::user()->id,
                    'fcm_token' => $token
                ]);
            }

            session(['fcm_token' => $token]);

            return response()->json([
                'success' => true,
                'message' => 'User token updated successfully',
                'token' => $token
            ]);
        } catch (\Exception $e) {
            report($e);
            return response()->json([
                'success' => false
            ], 500);
        }
    }

    public function message_notification(Chat $chat)
    {
        $from_user_id = $chat->sender_id;
        $to_user_id = $chat->receiver_id;
        $type = 'Message';

        try {
            $fcmTokens = DB::table('fcm_user')->where('user_id', $to_user_id)->pluck('fcm_token')->toArray();

            // Notification::send(null,new SendPushNotification($request->title,$request->message,$fcmTokens));

            /* or */

            //auth()->user()->notify(new SendPushNotification($title,$message,$fcmTokens));

            /* or */

            $notification = Notification::where('sender_id', $from_user_id)
                ->where('receiver_id', $to_user_id)
                ->first();

            if (is_null($notification)) {
                $notification = Notification::create([
                    'sender_id'     => $from_user_id,
                    'receiver_id'   => $to_user_id,
                    'content'       => $chat->message,
                    'status'        => 'unseen',
                    'count'         => 1,
                    'type'          => $type,
                ]);
            } else {
                $count = $notification->count;
                $notification->update([
                    'content'       => $chat->message,
                    'status'        => 'unseen',
                    'count'         => $count + 1,
                    'type'          => $type,
                ]);
            }

            $sender = User::where('id', $chat->sender_id)->first();
            if ($sender->user_image != '') {
                $sender_image =  asset($sender->user_image);
            } else {
                if ($sender->gender == 'Male') {
                    $sender_image = asset('images/profile-images/blank-profile-male.jpg');
                } elseif ($sender->gender == 'Female') {
                    $sender_image = asset('images/profile-images/blank-profile-female.jpg');
                } elseif ($sender->gender == 'Other') {
                    $sender_image = asset('images/profile-images/blank-profile-other.jpg');
                }
            }

            $msg_date = $chat->created_at;
            $msg_date = $this->change_to_local_time($msg_date, 'Asia/Kuwait');

            $sendData = [
                'id' => $chat->id,
                'sender_id' => $chat->sender_id,
                'sender_name' => $sender->name,
                'receiver_id' => $chat->receiver_id,
                'message' => $chat->message,
                'status' =>  $chat->status,
                'msg_date' => $msg_date,
                'sender_image' =>  $sender_image,
                'notification' => $notification,
            ];


            foreach ($fcmTokens as $fcm_token) {
                if (str_starts_with($fcm_token, 'Mobile')) {
                    $notidication_title = 'Message from: ' . $sender->name;
                    $notidication_body = $chat->message;

                    // $data = [
                    //     'type'          => 'Message',
                    //     'id'            => $from_user_id,
                    //     'sender_name'   => $sender->name,
                    // ];

                    $data = new stdClass;
                    $data->type = 'Message';
                    $data->id = $from_user_id;
                    $data->sender_name = $sender->name;

                    $this->notify_mobile_user(
                        $fcm_token,
                        [
                            'title' => $notidication_title,
                            'body' => $notidication_body,

                        ],
                        $data,
                    );
                } else {
                    Larafirebase::withTitle($type)->withBody($sendData)->sendMessage($fcm_token);
                }
            }

            return $sendData;
        } catch (\Exception $e) {
            // report($e);
            return 'faild : ' . $e->getMessage();
        }
    }


    // when a Customer order(book) a service.. notify the Admin and the provider whos related to the service
    // when the Booking state changes.. notify the Admin and the Intended person (Customer or Provider)
    public function booking_notification(Booking $booking, $intended_user, $title, $notify_admin)
    {
        $from_user_id = $booking->user_id;

        try {
            $type = 'Booking';

            ////// Create Notification for the Admin
            $admin_user_id = 0;
            if ($notify_admin) {
                $admin_user_id = User::where('role', 'Admin')->value('id');
                $notification = $this->store_booking_notification_in_DB($from_user_id, $admin_user_id, $booking);
            }

            $fcmTokens = DB::table('fcm_user')
                ->whereIn('user_id', [$admin_user_id, $intended_user])
                ->pluck('fcm_token')->toArray();

            ////// Create Notification for the Intended User (Provider , Handyman, Customer)
            $notification = $this->store_booking_notification_in_DB($from_user_id, $intended_user, $booking);


            $service_name = Service::where('id', $booking->service_id)->value('name');
            $user_name = User::where('id', $booking->user_id)->value('name');
            $sendData = [
                'title'         => $title,
                'service_name'  => $service_name,
                'user_name'     => $user_name,
                'state'         => $notification->content,
            ];

            foreach ($fcmTokens as $fcm_token) {
                if (str_starts_with($fcm_token, 'Mobile')) {
                    $notidication_title = 'Booking: ' . $booking->service->name;
                    $notidication_body = $title;

                    $data = [
                        'type'          => $type,
                        'id'            => $booking->id,
                    ];

                    $this->notify_mobile_user(
                        $fcm_token,
                        [
                            'title' => $notidication_title,
                            'body' => $notidication_body,
                        ],
                        $data,
                    );
                } else {

                    Larafirebase::withTitle($type)->withBody($sendData)->sendMessage($fcm_token);
                }
            }
        } catch (\Exception $e) {
            report($e);
            return 'faild';
        }
    }


    public function custom_notification($title, $content, $image)
    {

        $admin_user_id = User::where('role', 'admin')->value('id');

        $all_customers = User::where('role', 'user')->pluck('id')->toArray();

        $all_customers_ids = implode(",",$all_customers);
        try {
            $type = 'Custom';

            $fcmTokens = DB::table('fcm_user')
                ->whereIn('user_id', $all_customers)
                ->pluck('fcm_token')->toArray();


            ////// Create Notification for the Intended Users
            // foreach ($all_customers as $customer_id) {
                $notification = Notification::create([
                    'sender_id'         =>  $admin_user_id,
                    'receiver_id'       =>  0,  // the receivers ids is in broadcast_ids column, separated by (,)
                    'title'             =>  $title,
                    'content'           =>  $content,
                    'broadcast_ids'     =>  $all_customers_ids,
                    'status'            =>  'unseen',
                    'type'              =>  $type,
                    'image'             =>  $image === '' ? '' : $image,
                ]);
            // }

            $data = [
                'type'      =>  $type,
                'id'        =>  $notification->id,
                // 'content'       =>  $content,
                // 'image'         =>  $image,
            ];

            foreach ($fcmTokens as $fcm_token) {
                if (str_starts_with($fcm_token, 'Mobile')) {
                    $notidication_title = $title;
                    $notidication_body = $content;
                    $this->notify_mobile_user(
                        $fcm_token,
                        [
                            'title' => $notidication_title,
                            'body' => $notidication_body,
                        ],
                        $data,
                    );
                }
            }
            $msg = 'Successfully Broadcasted';
        } catch (\Exception $e) {
            report($e);
            $msg = 'Failed to Broadcast , ' . $e->getMessage();
        }

        return $msg;
    }


    // to mobile notification
    public function notify_mobile_user($fcm_token, $notification, $data)
    {
        Http::acceptJson()->withToken(config('larafirebase.authentication_key'))->post(
            'https://fcm.googleapis.com/fcm/send',
            [
                'to' => $fcm_token,
                'notification' => $notification,
                'data'  => $data,
            ]
        );
    }

    public function delete_other_users_token($token, $user_id)
    {
        $all_records = DB::table('fcm_user')->get();
        $user_id_length = strlen($user_id);
        foreach ($all_records as $record) {
            $record_user_id_length = strlen($record->user_id);
            if (Str::substr($record->fcm_token, $record_user_id_length) === Str::substr($token, $user_id_length)) {
                if ($user_id != $record->user_id) {
                    DB::table('fcm_user')->where('user_id', $record->user_id)->where('fcm_token', $record->fcm_token)->delete();
                }
            }
        }
    }


    function store_booking_notification_in_DB($from_user_id, $to_user_id, Booking $booking)
    {
        $type = 'Booking';
        $to_user_id = $to_user_id == $from_user_id ? $booking->service->provider->system_user_id : $to_user_id;
        $notification = Notification::where('sender_id', $from_user_id)
            ->where('receiver_id', $to_user_id)
            ->where('booking_id', $booking->id)
            ->first();

        if (is_null($notification)) {
            $notification = Notification::create([
                'sender_id'     => $from_user_id,
                'receiver_id'   => $to_user_id,
                'content'       => $booking->state,
                'status'        => 'unseen',
                'count'         => 1,
                'type'          => $type,
                'booking_id'    => $booking->id,
            ]);
        } else {
            // $count = $notification->count;
            $notification->update([
                'content'       => $booking->state,
                'status'        => 'unseen',
                // 'count'         => $count,
                // 'type'          => $type,
            ]);
        }

        return $notification;
    }

    function store_custom_notification_in_DB($notification = null)
    {
    }

    function change_to_local_time($msg_date, $time_zone)
    {
        $now_date = Carbon::now();
        $loc = new DateTimeZone($time_zone);

        // change the timezone of the object without changing its time
        $now_date = $now_date->setTimezone($loc);
        $msg_date = $msg_date->setTimezone($loc);

        if ($msg_date->format('y m d') === $now_date->format('y m d')) {

            $msg_date = $msg_date->format('h:i A');
        } else {
            $msg_date = $msg_date->format('j-M-Y , h:i A');
        }

        return $msg_date;
    }
}
