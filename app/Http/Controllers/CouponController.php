<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CouponController extends Controller
{

    public function index()
    {
        $coupons = Coupon::all();
        return view('coupon.index', compact('coupons'));
    }


    public function create()
    {
        return view('coupon.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required',
                'points' => 'required|numeric',
                'value' => 'required|numeric',
                'image' =>  'image|mimes:jpg,png,jpeg,svg|max:1024|dimensions:max_width=250,max_height=250',
            ],
            [
                'name.required' => 'Name field is required',

                'points.required' => 'Points field is required',
                'points.numeric' => 'Points must be a number',

                'value.required' => 'Value field is required',
                'value.numeric' => 'Value must be a number',

                'image.image' => 'File must be an image',
                'image.mimes' => 'Image type must be on of (jpg , png , jpeg , svg)',
                'image.max' => 'Maximum size of image is (1 MB)',
                'image.dimensions' => 'Image dimensions cannot be greater than (250 x 250)',
        ]);



        $coupon = Coupon::create([
            'name' => $request->name,
            'points' => $request->points,
            'value' => $request->value,
            // 'code' => $code,
            'status' => $request->status,
        ]);

        if($request->image != '')
        {
            $image =  time() . '-' . $request->image->getClientOriginalName();
            $request->image->move('images/coupons/', $image);

            $image = 'images/coupons/' . $image;

            $coupon->update(['image' => $image]);
        }

        return redirect()->route('coupons.index')
            ->withSuccess(__('Coupon created successfully'));
    }


    public function show(Coupon $coupon)
    {
        //
    }


    public function edit(Coupon $coupon)
    {
        return view('coupon.edit', compact('coupon'));
    }


    public function update(Request $request, Coupon $coupon)
    {
        $this->validate($request, [
            'name' => 'required',
            'points' => 'required|numeric',
            'value' => 'required|numeric',
            'image' =>  'image|mimes:jpg,png,jpeg,svg|max:1024|dimensions:max_width=250,max_height=250',
            ],
            [
            'name.required' => 'Name field is required',

            'points.required' => 'Points field is required',
            'points.numeric' => 'Points must be a number',

            'value.required' => 'Value field is required',
            'value.numeric' => 'Value must be a number',

            'image.image' => 'File must be an image',
            'image.mimes' => 'Image type must be on of (jpg , png , jpeg , svg)',
            'image.max' => 'Maximum size of image is (1 MB)',
            'image.dimensions' => 'Image dimensions cannot be greater than (250 x 250)',
        ]);


        $old_image = $coupon->image;
        $new_image = null;
        if($request->has('image'))
        {
            $image = $request->image;
            $new_image = time() . '-' . $request->image->getClientOriginalName();
            $image->move('images/coupons/',$new_image);
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/coupons/' . $new_image;

        $coupon->update([
            'name' => $request->name,
            'points' => $request->points,
            'value' => $request->value,
            'status' => $request->status,
            'image' => $image_Store
        ]);


        return redirect()->route('coupons.index')
            ->withSuccess(__('Coupon updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        //
    }


    public function disable($coupon_id)
    {
        $coupon = Coupon::where('id', $coupon_id)->first();
        $coupon->update(['status' => 'Off']);

         return redirect()->route('coupons.index')
            ->withSuccess(__('Coupon Disabled successfully'));
    }

    public function enable($coupon_id)
    {
        $coupon = Coupon::where('id', $coupon_id)->first();
        $coupon->update(['status' => 'On']);

         return redirect()->route('coupons.index')
            ->withSuccess(__('Coupon Enabled successfully'));
    }
}
