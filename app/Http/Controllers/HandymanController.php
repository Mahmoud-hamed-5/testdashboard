<?php

namespace App\Http\Controllers;

use App\Models\Handyman;
use App\Models\Provider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class HandymanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $handymen = Handyman::all();

        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');

            $handymen = Handyman::where('provider_id', $provider_id)->get();
        }


        return view('handyman.index', compact('handymen'));
    }


    public function show(Handyman $handyman)
    {
        //
    }


    public function create()
    {
        $providers = Provider::where('type', 'Company')->get();
        return view('handyman.create', compact('providers'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'gender' => $request->gender,
            'phone' => $request->phone == null ? '' : $request->phone,
            'address' => $request->address == null ? '' : $request->address,
            'status' => $request->status,
        ]);



        $role = Role::where('name', 'handyman')->value('id');
        $user->syncRoles($role);
        $user->update(['role' => 'handyman']);


        $handyman = Handyman::updateOrCreate(
            ['system_user_id' => $user->id]
        );

        if (Auth::user()->role === 'provider'){
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');
            $route = 'provider.handymen.index';
        }else if (Auth::user()->role === 'admin'){
            $provider_id = $request->provider;
            $route = 'handymen.index';
        }



        $handyman->update(['provider_id' => $provider_id]);


        return redirect()->route($route)
            ->withSuccess(__('Handyman created successfully'));
    }


    public function edit(Handyman $handyman)
    {

        $user = $handyman->user;
        $provider = Provider::where('system_user_id', $user->id)->first();

        return view('handyman.edit', [
            'user' => $user,
            'userRole' => $user->roles->pluck('name')->toArray(),
            'roles' => Role::latest()->get(),
            'provider' => $provider
        ]);
    }


    public function update(Request $request, $system_user_id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $user = User::where('id', $system_user_id)->first();
        $user->update([
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => $request->password,
            'gender' => $request->gender,
            'phone' =>$request->phone,
            'address' => $request->address,
            'status' => $request->status
        ]);

        if ($request->password != '' || $request->password != null)
        {
            $user->update([
                'password' => $request->password,
            ]);
        }

        return redirect()->route('handymen.index')
            ->withSuccess(__('User updated successfully'));
    }




    public function destroy(Handyman $handyman)
    {
        //
    }
}
