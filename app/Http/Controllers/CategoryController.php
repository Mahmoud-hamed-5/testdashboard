<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();

        return view('category.index', compact('categories'));
    }


    public function show(Category $category)
    {
        return view('category.show', compact('category'));
    }

    public function create()
    {
        return view('category.create');
    }


    public function store(Request $request)
    {

        $this->validate($request,
            [
                'name' => 'required',
                'name_ar' => 'required',
                'image' =>  'image|mimes:jpg,png,jpeg|max:2048'],
            [
                'image.image' => 'image',
                'image.mimes' => 'mimes',
                'image.max' => 'max',
            ]
        );

        $category = Category::create([
            'name' => $request->name,
            'name_ar' => $request->name_ar,
            'details' => $request->details,
            'status' => $request->status,
        ]);

        if($request->image != '')
        {
            $image =  time() . '-' . $request->image->getClientOriginalName();
            $request->image->move('images/categories/', $image);

            $image = 'images/categories/' . $image;

            $category->update(['image' => $image]);
        }



        return redirect()->route('categories.index')
            ->withSuccess(__('Category created successfully'));
    }


    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }


    public function update(Request $request, Category $category)
    {

        $this->validate($request,[
            'name' => 'required',
            'image' =>  'image|mimes:jpg,png,jpeg|max:2048'
        ]);

        $old_image = $category->image;
        $new_image = null;
        if($request->has('image'))
        {
            $image = $request->image;
            $new_image = time() . '-' . $request->image->getClientOriginalName();
            $image->move('images/categories/',$new_image);
            //$post->photo = 'uploads/posts/'.$newPhoto;
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/categories/' . $new_image;

        $category->update([
            'name' => $request->name,
            'details' => $request->details,
            'status' => $request->status,
            'image' => $image_Store
        ]);

        return redirect()->route('categories.index')
            ->withSuccess(__('Category updated successfully'));
    }


    public function disable($category_id)
    {
        $category = Category::where('id', $category_id)->first();
        $category->update(['status' => 'Off']);

         return redirect()->route('categories.index')
            ->withSuccess(__('Category Disabled successfully'));
    }

    public function enable($category_id)
    {
        $category = Category::where('id', $category_id)->first();
        $category->update(['status' => 'On']);

         return redirect()->route('categories.index')
            ->withSuccess(__('Category Enabled successfully'));
    }

    public function destroy(Category $category)
    {
        // $category->update(['status' => 'NotAvailable']);
        // foreach($category->subcategories as $subcategory)
        // {
        //     $subcategory->update(['status' => 'NotAvailable']);
        // }
        // return redirect()->route('categories.index')
        //     ->withSuccess(__('Category deleted successfully'));
    }
}
