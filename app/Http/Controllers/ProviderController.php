<?php

namespace App\Http\Controllers;

use App\Models\Handyman;
use App\Models\Provider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class ProviderController extends Controller
{

    public function index()
    {
        $providers = Provider::all();

        // calculate the rate of each provider
        $providers_ratings = [];
        foreach ($providers as $provider) {
            $provider_rating = $provider->rating();
            $providers_ratings['provider' . $provider->id] = $provider_rating;
        }


        return view('provider.index', compact('providers', 'providers_ratings'));
    }


    public function show(Provider $provider)
    {
        //
    }


    public function create()
    {
        return view('provider.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);



        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'gender' => $request->gender,
            'phone' => $request->phone == null ? '' : $request->phone,
            'address' => $request->address == null ? '' : $request->address,
            'status' => $request->status,
        ]);

        $role = Role::where('name', 'provider')->value('id');
        $user->syncRoles($role);


        $user->update(['role' => 'provider']);



            Provider::updateOrCreate(
                ['system_user_id' => $user->id],
                    ['system_user_id' => $user->id,
                    'type' => $request->type,
                    'description' => $request->description]
            );


        return redirect()->route('providers.index')
            ->withSuccess(__('Provider created successfully'));
    }



    public function edit(Provider $provider)
    {
        $user = $provider->user;
        return view('provider.edit', [
            'user' => $user,
            'userRole' => $user->roles->pluck('name')->toArray(),
            'roles' => Role::latest()->get(),
            'provider' => $provider
        ]);
    }


    public function update(Request $request, $system_user_id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            ],
            [
            'name.required' => 'Name field is required',
            'phone.regex' => 'Phone field can be only numbers',
            'phone.min' => 'Phone field must be only 10 numbers',
            'phone.max' => 'Phone field must be only 10 numbers',
        ]);

        $user = User::where('id', $system_user_id)->first();
        $user->update([
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => $request->password,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'status' => $request->status
        ]);

        if ($request->password != '' || $request->password != null)
        {
            $user->update([
                'password' => $request->password,
            ]);
        }

        Provider::updateOrCreate(
            ['system_user_id' => $user->id],
                ['description' => $request->description]
        );

        return redirect()->route('providers.index')
            ->withSuccess(__('User updated successfully'));
    }



    public function destroy(Provider $provider)
    {
        //
    }
}
