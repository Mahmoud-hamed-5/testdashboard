<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Booking_images;
use App\Models\Coupon;
use App\Models\Handyman;
use App\Models\Notification;
use App\Models\Provider;
use App\Models\Service;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{


    public function index()
    {

        $bookings = Booking::all();

        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');

            $bookings = Booking::where('provider_id', $provider_id)->get();
        }

        if (Auth::user()->role === 'handyman') {
            $handyman_id = Handyman::where('system_user_id', Auth::user()->id)->value('id');

            $bookings = Booking::where('handyman_id', $handyman_id)->get();
        }

        return view('booking.index', compact('bookings'));
    }


    public function edit(Booking $booking)
    {
        Notification::where('receiver_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['status' => 'seen']);


        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');

            $handymen = Handyman::where('provider_id', $provider_id)->get();
            return view('booking.edit', compact('booking', 'handymen'));
        }

        if (Auth::user()->role === 'handyman') {
            return view('booking.edit', compact('booking'));
        }

        return view('booking.edit', compact('booking'));
    }


    public function provider_approve(Request $request, Booking $booking)
    {

        $time = $request->time;
        $min_time = $booking->min_time;
        $max_time = $booking->max_time;

        $min_price = $booking->service->min_price;
        $max_price = $booking->service->max_price;
        if ($booking->service->type === 'Fixed') {

            $max_price = $min_price;
        }
        $min_time = Carbon::parse($min_time)->format('H:i');
        $max_time = Carbon::parse($max_time)->format('H:i');

        $handman_required = '';
        if ($booking->service->provider->type === 'Company') {

            $handman_required = 'required';
        }

        $this->validate(
            $request,
            [
                'time' => 'required|date_format:H:i|after_or_equal:' . $min_time . '|before_or_equal:' . $max_time,
                'price' => 'required|numeric|between:' . $min_price . ',' . $max_price,
                'handyman_id'  => $handman_required
            ],
            [
                'time.required'     =>  'Time is required',
                'time.date'     =>  'Time must be',
                'time.after_or_equal' => 'Time must be equal or after ' . $min_time,
                'time.before_or_equal' => 'Time must be equal or before ' . $max_time,

                'price.required' => 'Price field is required',
                'price.between' => 'Price must be between ' . $min_price . ' and ' . $max_price,

                'handyman_id.required'  => 'Handyman is required'
            ]
        );
        $price = $booking->service->type === 'Fixed' ? $min_price : $request->price;
        $amount = $price - (($request->price * $booking->service->discount) / 100);

        $coupon_value = is_null($booking->coupon) ? 0 : $booking->coupon->value;
        $amount = $amount - $coupon_value;

        $handyman = Handyman::find($request->handyman_id);

        $state = 'Ongoing';

        $booking->update([
            'time'          => $request->time,
            'amount'        => $amount,
            'handyman_id'   => $handyman === null ? null : $handyman->id,
            'state'         => $state
        ]);


        Notification::where('receiver_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['content' => $booking->state]);

        // Notify the Customer
        (new FCMController)->booking_notification($booking , $booking->user_id, 'Booking Approved, Handyman is on the Way', 0);

        // Notify the Handyman, when the provider type is a company and there is a handyman
        if (! is_null($handyman)) {
            (new FCMController)->booking_notification($booking , $handyman->system_user_id, 'You have Work', 0);
        }

        return redirect()->route('provider.bookings.index')
                ->withSuccess(__('Booking approved !'));

        // return redirect()->back()->withSuccess(__('Booking approved !'));
    }

    // No Longer Used
/*
    public function assign_handyman(Request $request, Booking $booking)
    {
        $handyman = Handyman::find($request->handyman_id);

        $booking->update(['handyman_id' => $handyman->id, 'state' => 'Ongoing']);

        Notification::where('receiver_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['content' => $booking->state]);

        // Notify the Handyman
        (new FCMController)->booking_notification($booking , $handyman->system_user_id, 'You have Work', 0);

        // Notify the Customer
        (new FCMController)->booking_notification($booking , $booking->user->id, 'Handyman is on the Way', 0);

        return redirect()->route('provider.bookings.index')
            ->withSuccess(__('Handyman assigned successfully'));
    }
*/

    public function reject(Request $request, Booking $booking)
    {

        $this->validate(
            $request,
            ['reason' => 'required'],
            ['reason.required' => 'Reason field is required']
        );

        $reason = 'Rejected By ' . Auth::user()->role . ' : ' . $request->reason;
        $booking->update(['state' => 'Cancelled', 'reason' => $reason]);

        Notification::where('receiver_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(['content' => $booking->state]);


        // Notify the Customer ... and the Admin
        $result = (new FCMController)->booking_notification($booking , $booking->user_id, 'Booking Cancelled', 1);

        if (!is_null($booking->coupon_code)) {
            DB::table('coupon_user')->where('code', $booking->coupon_code)->update(['used' => 'No']);
        }

        return redirect()->route(Auth::user()->role . '.bookings.index')
            ->withSuccess(__('Booking Rejected !'));
    }


    public function finish(Request $request, Booking $booking)
    {

        $booking->update(['state' => 'Completed']);

        Notification::where('receiver_id', Auth::user()->id)
            ->where('booking_id', $booking->id)
            ->update(
                ['content' => $booking->state]
            );


        // Notify the Customer ... and the Admin
        (new FCMController)->booking_notification($booking , $booking->user_id, 'Booking Completed', 1);

        if (!is_null($booking->coupon)) {
            DB::table('coupon_user')->where('id', $booking->coupon_user_id)->update(['used' => 'Yes']);
        }

        $setting = Setting::where('key', 'points')->where('status', 'On')->first();

        if (!is_null($setting)) {
            $val = $setting->value;
            $user = User::where('id', $booking->user_id)->first();
            $amount = $booking->amount;
            $points = ($amount / 1000) * $val;
            $points += $user->points;
            $user->update(['points' => $points]);
        }

        return redirect()->route(Auth::user()->role . '.bookings.index')
            ->withSuccess(__('Booking Completed !'));
    }


    public function filter(Request $request)
    {
        $bookingsDB = Booking::all();
        if (Auth::user()->role === 'provider') {
            $provider_id = Provider::where('system_user_id', Auth::user()->id)->value('id');

            $bookingsDB = $bookingsDB->where('provider_id', $provider_id);
        }

        if (Auth::user()->role === 'handyman') {
            $handyman_id = Handyman::where('system_user_id', Auth::user()->id)->value('id');

            $bookingsDB = $bookingsDB->where('handyman_id', $handyman_id);
        }

        $all_count = $bookingsDB->count();
        $pending_count = $bookingsDB->where('state', 'Pending')->count();
        $ongoing_count = $bookingsDB->where('state', 'Ongoing')->count();
        $completed_count = $bookingsDB->where('state', 'Completed')->count();
        $cancelled_count = $bookingsDB->where('state', 'Cancelled')->count();

        $response['all_count'] = $all_count;
        $response['pending_count'] = $pending_count;
        $response['ongoing_count'] = $ongoing_count;
        $response['completed_count'] = $completed_count;
        $response['cancelled_count'] = $cancelled_count;


        if ($request->state != 'All') {
            $bookingsDB = $bookingsDB->where('state', $request->state);
        }

        if ($bookingsDB->isEmpty()) {
            return $response;
        } else {

            $index = 0;
            $bookings = [];
            foreach ($bookingsDB as $booking) {
                if (Auth::user()->role === 'admin') {
                    $route = route('bookings.edit', $booking);
                }else{
                    $route = route(Auth::user()->role . '.bookings.edit', $booking);
                }

                $routes[$index] = $route;

                $bookings[$index]['service_name'] = $booking->service->name;
                $bookings[$index]['service_type'] = $booking->service->type;
                $bookings[$index]['description'] = $booking->description;
                $bookings[$index]['state'] = $booking->state;
                $bookings[$index]['date'] = $booking->date;
                $bookings[$index]['min_time'] = $booking->min_time;
                $bookings[$index]['max_time'] = $booking->max_time;
                $bookings[$index]['time'] = $booking->time === null ? '' : $booking->time;
                $bookings[$index]['customer_name'] = $booking->user->name;
                $bookings[$index]['customer_phone'] = $booking->phone;
                $bookings[$index]['customer_address'] = $booking->address;
                $bookings[$index]['provider_name'] = $booking->service->provider->user->name;
                $bookings[$index]['handyman_name'] = $booking->handyman != null ? $booking->handyman->user->name : $booking->service->provider->user->name;
                $bookings[$index]['booking_state'] = $booking->state;
                $bookings[$index]['coupon'] = $booking->coupon_code == null ? '' : $booking->coupon_code;
                $bookings[$index]['amount'] = $booking->amount;
                $bookings[$index]['reason_of_rejection'] = $booking->reason === null ? '' : $booking->reason;

                $index++;
            }
            $response['bookings'] = $bookings;
            $response['routes'] = $routes;

            return $response;
        }
    }


}
