<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();

        if ($user->role === 'provider') {
            $provider = Provider::where('system_user_id', $user->id)->first();
            //$provider_rating = $provider->rating();

            return view('profile.index', compact('user', 'provider'));
         }

        return view('profile.index', compact('user'));
    }

    public function show($user_id)
    {
        $user = User::where('id', $user_id)->first();

        if ($user->role === 'provider') {
           $provider = Provider::where('system_user_id', $user->id)->first();
           $provider_rating = $provider->rating();

           return view('profile.index', compact('user', 'provider_rating'));
        }

        return view('profile.index', compact('user'));
    }


    public function update(Request $request, User $user)
    {

        $this->validate($request,[
            'name' => 'required',
            'gender' => 'in:Male,Female,Other',
            // 'phone' => 'min:10|max:10|regex:/^[0-9]+$/',
            'user_image' =>  'image|mimes:jpg,png,jpeg|max:2048'
            ],
            [
            'name.required' => 'Name field is required',
            'gender.in' => 'Gender must be on of (Male,Female,Other)',
            // 'phone.regex' => 'Phone field can be only numbers',
            // 'phone.min' => 'Phone field must be only 10 numbers',
            // 'phone.max' => 'Phone field must be only 10 numbers',
        ]);


        $old_image = $user->image;
        $new_image = null;
        if($request->has('image'))
        {
            $image = $request->image;
            $new_image = time() . '-' . $request->image->getClientOriginalName();
            $image->move('images/profile-images/',$new_image);
            //$post->photo = 'uploads/posts/'.$newPhoto;
            File::delete($old_image);
        }

        $image_Store = $new_image == null ? $old_image : 'images/profile-images/' . $new_image;

        $user->update([
            'name' => $request->name,
            //'email' => $request->email,
            // 'password' => $request->password,
            'gender' => $request->gender,
            'address' => $request->address,
            // 'phone' => $request->phone,
            'user_image' => $image_Store
        ]);

        if ($request->password != '' || $request->password != null)
        {
            $user->update([
                'password' => $request->password,
            ]);
        }

        return redirect()->back()
            ->withSuccess(__('Profile updated successfully'));
    }
}
