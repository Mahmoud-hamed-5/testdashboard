<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Provider extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'description',
        'system_user_id'
    ];


    public function rating()
    {
        $provider_rates = 0;
        $reviews = DB::table('providers_reviews')->where('provider_id', $this->id)->get();
        foreach ($reviews as $review)
        {
            $provider_rates += $review->rate;
        }
        $reviews_count = $reviews->count();
        $provider_rate = $reviews_count == 0 ? 0 : $provider_rates / $reviews_count;

        return $provider_rate;
    }

    /**
     * Get the user that owns the Provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'system_user_id', 'id');
    }


    /**
     * Get all of the handymen for the Provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function handymen(): HasMany
    {
        return $this->hasMany(Handyman::class, 'provider_id', 'id');
    }

    /**
     * The services that belong to the Provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services(): HasMany
    {
        return $this->hasMany(Service::class);
    }

}
