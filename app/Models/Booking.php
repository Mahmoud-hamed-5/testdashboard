<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_id',
        'description',
        'date',
        'time',
        'min_time',
        'max_time',
        'user_id',
        'phone',
        'address',
        'provider_id',
        'handyman_id',
        'state',
        'amount',
        'payment_status',
        'reason',
        'coupon_code',

        'lat',
        'lng',

    ];



    /**
     * Get the service that owns the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }


     /**
     * Get the provider that owns the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }


    /**
     * Get the user that owns the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    /**
     * Get the handyman that owns the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function handyman(): BelongsTo
    {
        return $this->belongsTo(Handyman::class, 'handyman_id', 'id');
    }


    /**
     * Get the coupon that owns the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon(): BelongsTo
    {
        return $this->belongsTo(Coupon::class);
    }



    /**
     * Get all of the booking_images for the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function booking_images(): HasMany
    {
        return $this->hasMany(Booking_images::class);
    }

}
