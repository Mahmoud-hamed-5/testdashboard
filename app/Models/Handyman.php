<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Handyman extends Model
{
    use HasFactory;

    protected $fillable = [
        //'name',
        // 'type',
        //'phone',
        //'address',
        'provider_id',
        //'status',
        'system_user_id'
    ];


    /**
     * Get the user that owns the Handyman
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'system_user_id', 'id');
    }


    /**
     * Get the provider that owns the Handyman
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }

}
