<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Administraton Lock</title>
    <!--favicon-->
    <link rel="icon" href="{{asset('assets/auth/images/favicon-32x32.png')}}" type="image/png" />
    <!-- loader-->
    <link href="{{asset('assets/auth/css/pace.min.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/auth/js/pace.min.js')}}"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/auth/css/bootstrap.min.css')}}" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto&display=swap" />
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{asset('assets/auth/css/icons.css')}}" />
    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('assets/auth/css/app.css')}}" />
</head>

@php
    $genderArray = ['Male' , 'Female'];
@endphp

<body class="bg-register">
    <!-- wrapper -->
    <div class="wrapper">
        <div class="section-authentication-register d-flex align-items-center justify-content-center">
            <div class="row">
                <div class="col-12 col-lg-10 mx-auto">
                    <div class="card radius-15 overflow-hidden">
                        <div class="row g-0">
                            <div class="col-xl-6">
                                <div class="card-body p-md-5">
                                    <div class="text-center">
                                        <img src="assets/auth/images/logo-icon.png" width="80" alt="">
                                        <h3 class="mt-4 font-weight-bold">Create an Account</h3>
                                    </div>
                                    <div class="">
                                        <div class="d-grid">
                                            <a class="btn my-4 shadow-sm btn-white" href="javascript:;"> <span
                                                    class="d-flex justify-content-center align-items-center">
                                                    <img class="me-2" src="assets/auth/images/icons/search.svg"
                                                        width="16" alt="Image Description">
                                                    <span>Sign Up with Google</span>
                                                </span>
                                            </a> <a href="javascript:;" class="btn btn-facebook"><i
                                                    class="bx bxl-facebook me-1"></i>Sign Up with Facebook</a>
                                        </div>
                                        <div class="login-separater text-center mb-4"> <span>OR SIGN UP WITH
                                                EMAIL</span>
                                            <hr>
                                        </div>
                                        <div class="form-body">

                                            <form method="POST" action="{{ route('register') }}" class="row g-3">
                                                @csrf
                                                <div class="col-sm-6">
                                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                                    <input type="text" class="form-control" id="name"
                                                        name="name" placeholder="Jhon">
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="gender" class="form-label">{{ __('Grnder') }}</label>
                                                    <select class="form-control" name="gender" id="gender">
                                                            @foreach ($genderArray as $item)
                                                            <option value="{{$item}}">
                                                                {{$item}}
                                                            </option>
                                                            @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-12">
                                                    <label for="email"
                                                        class="form-label">{{ __('Email Address') }}</label>
                                                    <input type="email" class="form-control" id="email"
                                                        name="email" placeholder="Email Address">
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="password"
                                                        class="form-label">{{ __('Password') }}</label>
                                                    <div class="input-group" id="show_hide_password">
                                                        <input type="password" class="form-control border-end-0"
                                                            id="password" name="password" placeholder="Enter Password">
                                                        <a href="javascript:;"
                                                            class="input-group-text bg-transparent"><i
                                                                class="bx bx-hide"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="password"
                                                        class="form-label">{{ __('Confirm Password') }}</label>
                                                    <div class="input-group" id="show_hide_password">
                                                        <input type="password" class="form-control border-end-0"
                                                            id="password-confirm" name="password_confirmation"
                                                            placeholder="Confirm Password">
                                                        <a href="javascript:;"
                                                            class="input-group-text bg-transparent"><i
                                                                class="bx bx-hide"></i></a>
                                                    </div>
                                                </div>


                                                <div class="col-12">
                                                    <label for="phone"
                                                        class="form-label">{{ __('Phone Number') }}</label>
                                                    <input type="text" class="form-control" id="phone"
                                                        name="phone" placeholder="{{ __('Phone Number (Optional)') }}">
                                                </div>

                                                <div class="col-12">
                                                    <label for="address"
                                                        class="form-label">{{ __('Address') }}</label>
                                                    <input type="text" class="form-control" id="address"
                                                        name="address" placeholder="{{ __('Address (Optional)') }}">
                                                </div>

                                                <div class="col-12">
                                                    <div class="d-grid">
                                                        <button type="submit" class="btn btn-primary"><i
                                                                class="bx bx-user me-1"></i>Sign up</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xl-6 bg-login-color d-flex align-items-center justify-content-center">
                                <img src="assets/auth/images/login-images/register-frent-img.jpg" class="img-fluid"
                                    alt="...">
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end wrapper -->
    <!-- JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('assets/auth/js/jquery.min.js')}}"></script>
    <!--Password show & hide js -->
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("bx-hide");
                    $('#show_hide_password i').removeClass("bx-show");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("bx-hide");
                    $('#show_hide_password i').addClass("bx-show");
                }
            });
        });
    </script>
</body>

</html>
