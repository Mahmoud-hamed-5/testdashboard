<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<title>Syndash - Reset</title>
	<!--favicon-->
	<link rel="icon" href="{{asset('assets/auth/images/favicon-32x32.png')}}" type="image/png" />
	<!-- loader-->
	<link href="{{asset('assets/auth/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{asset('assets/auth/js/pace.min.js')}}"></script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{asset('assets/auth/css/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto&display=swap" />
	<!-- Icons CSS -->
	<link rel="stylesheet" href="{{asset('assets/auth/css/icons.css')}}" />
	<!-- App CSS -->
	<link rel="stylesheet" href="{{asset('assets/auth/css/app.css')}}" />
</head>

<body class="bg-forgot">
	<!-- wrapper -->
	<div class="wrapper">
		<div class="authentication-forgot d-flex align-items-center justify-content-center">
			<div class="card shadow-lg forgot-box">
				<div class="card-body p-md-5">
					<div class="text-center">
						<img src="{{asset('assets/auth/images/icons/forgot-2.png')}}" width="140" alt="" />
					</div>
					<h4 class="mt-5 font-weight-bold">Forgot Password?</h4>
					<p class="text-muted">Enter your registered email ID to reset the password</p>

                    <form method="POST" action="{{ route('password.email') }}" class="row g-3">
                        @csrf

                      



					<div class="mb-3 mt-4">
						<label class="form-label">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						{{-- <input type="text" class="form-control form-control-lg radius-30" placeholder="example@user.com" /> --}}
					</div>
                    <div class="d-grid gap-2">
						<button type="submit" class="btn btn-primary btn-lg radius-30">Send</button>

					</div>
                </form>
					<div class="d-grid gap-2">

						 <a href="{{ route('login') }}" class="btn btn-light radius-30"><i class='bx bx-arrow-back mr-1'></i>Back to Login</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->
</body>

</html>
