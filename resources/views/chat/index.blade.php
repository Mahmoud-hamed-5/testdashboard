@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'chat';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                {{-- <h3 id="page">{{ __('Chats') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div> --}}


                <!--breadcrumb-->
                {{-- <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">



                </div> --}}
                <!--end breadcrumb-->

                @php
                    if (Auth::user()->user_image != '') {
                        $my_image = asset(Auth::user()->user_image);
                    } else {
                        if (Auth::user()->gender == 'Male') {
                            $my_image = asset('images/profile-images/blank-profile-male.jpg');
                        }
                        if (Auth::user()->gender == 'Female') {
                            $my_image = asset('images/profile-images/blank-profile-female.jpg');
                        }
                        if (Auth::user()->gender == 'Other') {
                            $my_image = asset('images/profile-images/blank-profile-other.jpg');
                        }
                    }
                @endphp

                <div class="chat-wrapper">

                    <div class="chat-sidebar">

                        <div class="chat-sidebar-header">
                            <div class="d-flex align-items-center">
                                <div class="chat-user-online">
                                    <img src="{{ $my_image }}" width="45" height="45" class="rounded-circle"
                                        alt="" />
                                </div>
                                <div class="flex-grow-1 ms-2">
                                    <p class="mb-0">{{ Auth::user()->name }}</p>
                                </div>
                            </div>
                            <div class="mb-3"></div>
                            <div class="input-group input-group-sm"> <span class="input-group-text bg-transparent"><i
                                        class='bx bx-search'></i></span>
                                <input type="text" class="form-control" onkeyup="sortUsers(window.userRole, this.value);"
                                    placeholder="People...">
                                <span class="input-group-text bg-transparent">
                                    <i class='bx bx-dialpad'></i>
                                </span>
                            </div>


                            <div class="chat-tab-menu mt-3">
                                <ul class="nav nav-pills nav-justified">
                                    <li class="nav-item">
                                        <a role="button" class="nav-link active" data-bs-toggle="pill"
                                            onclick="sortUsers('%', '%')">
                                            <div class="font-24"><i class='bx bx-conversation'></i>
                                            </div>
                                            <div><small>{{__('all')}}</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a role="button" class="nav-link" data-bs-toggle="pill"
                                            onclick="sortUsers('user', '%')">
                                            <div class="font-24"><i class="fa-solid fa-users"></i>
                                            </div>
                                            <div><small>{{__('Customers')}}</small>
                                            </div>
                                        </a>
                                    </li>
                                    @if (Auth::user()->role === 'admin')
                                        <li class="nav-item">
                                            <a role="button" class="nav-link" data-bs-toggle="pill"
                                                onclick="sortUsers('provider', '%')">
                                                <div class="font-24"><i class="fa-solid fa-user-tie"></i>
                                                </div>
                                                <div><small>{{__('Providers')}}</small>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                    <li class="nav-item">
                                        <a role="button" class="nav-link" data-bs-toggle="pill"
                                            onclick="sortUsers('handyman', '%')">
                                            <div class="font-24"><i class="fa-solid fa-helmet-safety"></i>
                                            </div>
                                            <div><small>{{__('Handymen')}}</small>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="chat-sidebar-content">
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-Chats">
{{--
                                    <div class="p-3">
                                        <h3>Users List</h3>
                                    </div> --}}


                                    <div class="chat-list">
                                        <div id="users_list" class="list-group list-group-flush">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" id="chat_header">

                    </div>



                    <div id="chat-content" class="chat-content">


                    </div>



                    <div id="chat-footer">

                    </div>


                    <!--start chat overlay-->
                    <div class="overlay chat-toggle-btn-mobile"></div>
                    <!--end chat overlay-->
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>

    <script>
        // These Lines is to get login_user information to add them on the displayed sent message in chat area
        var from_user_id = "{{ Auth::user()->id }}";
        var myName = "{{ Auth::user()->name }}";
        var login_user_gender = "{{ Auth::user()->gender }}";
        var login_user_image = "{{ Auth::user()->user_image }}";


        if (login_user_image != '') {
            var myImage = `{{ asset('`+login_user_image+`') }}`;
        } else {
            if (login_user_gender == 'Male') {
                var myImage = "{{ asset('images/profile-images/blank-profile-male.jpg') }}";
            } else if (login_user_gender == 'Female') {
                var myImage = "{{ asset('images/profile-images/blank-profile-female.jpg') }}";
            } else if (login_user_gender == 'Other') {
                var myImage = "{{ asset('images/profile-images/blank-profile-other.jpg') }}";
            }
        }


        new PerfectScrollbar('.chat-list');
        new PerfectScrollbar('.chat-content');

        function getTime() {

            var date = new Date();
            //date = new Date().toLocaleDateString('ar-KW', { Hours:"numeric", Minutes:"numeric"})
            date.toLocaleString([], {
                timeZone: 'Asia/Kuwait',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true
            });

            // var MessageDate = new Date().format("YYYY-MM-DD hh:mm a");

            return moment().format('hh:mm A');
        }

        function send() {

            document.querySelector('#btnSubmit').disabled = true;

            var myMessage = $('#message').val();

            var myMessageDate = moment().format('hh:mm A');


            // add my message to the chatBox area (conversation box)
            add_my_msg(myName, myMessage, myImage, myMessageDate);

            $.ajax({
                type: "POST",
                url: "/send-message",
                data: "message=" + myMessage + "&to_user_id=" + to_user_id + "&from_user_id=" + from_user_id,
                success: function(data) {
                    // To Do .. synchronize message time with the server

                    //add_my_msg(myName, myMessage, myImage, myMessageDate);
                }
            });



        }

        function add_my_msg(myName, myMessage, myImage, myMessageDate) {
            document.querySelector('#message').value = '';
            document.querySelector('#btnSubmit').disabled = false;


            html = `
                <div class="chat-content-rightside">
                    <div class="d-flex flex-row-reverse">
                        <img src= "` + myImage + `" width="48"
                            height="48" class="rounded-circle" alt="" />
                            <div class="flex-grow-1 me-2">
                                <p class="mb-0 chat-time text-end"> ` +
                myName + ` , ` + myMessageDate + `</p>
                                <p class="chat-right-msg">` +
                myMessage + ` </p>
                            </div>
                    </div>
                </div>
            `;

            var previous_chat_element = document.querySelector('#chat-content');
            var chat_history_element = document.querySelector('#chat-content');

            chat_history_element.innerHTML = previous_chat_element.innerHTML + html;
            sortUsers(userRole, userName);
            scroll_top();
        }

        function make_chat_area(user_id, to_user_name) {

            var html = `
                    <div class="chat-header d-flex align-items-center">
                            <div class="chat-toggle-btn"><i class='bx bx-menu-alt-left'></i>
                            </div>
                            <div>
                                <h4 class="mb-1 font-weight-bold">Chat with: ` + to_user_name + `</h4>

                                <div class="list-inline d-sm-flex mb-0 d-none"> <a href="javascript:;"
                                        class="list-inline-item d-flex align-items-center text-secondary"><small
                                            class='bx bxs-circle me-1 chart-online'></small>Active Now</a>
                                </div>
                            </div>

                    </div>
            `;

            to_user_id = user_id;

            document.getElementById('chat_header').innerHTML = html;



            var footer = `
                <div class="chat-footer d-flex align-items-center">
                    <div class="flex-grow-1 pe-2">
                        <div class="input-group"> <span class="input-group-text"><i class='bx bx-smile'></i></span>
                            <input type="text" id="message" name="message" class="form-control"
                                placeholder="Type a message">
                        </div>
                    </div>

                    <button id="btnSubmit" onClick="send()" class="btn btn-success">
                        <i class="fadeIn animated bx bx-paper-plane"></i>
                    </button>

                    <div class="chat-footer-menu"> </div>
                </div>
            `;


            document.getElementById('chat-footer').innerHTML = footer;

            var input_element = document.querySelector('#message');

            input_element.addEventListener("keypress", function(event) {
                // If the user presses the "Enter" key on the keyboard
                if (event.key === "Enter") {
                    event.preventDefault();
                    // Trigger the button with a click to send the message
                    document.getElementById("btnSubmit").click();
                }
            });
        }

        function load_chat_data(from_user_id, to_user_id) {

            $.ajax({
                type: "GET",
                url: "/chat-data",
                data: "sender=" + from_user_id + "&receiver=" + to_user_id,
                success: function(data) {
                    chats = data;
                    // alert(chats[0].message);
                    html = '';
                    for (var count = 0; count < chats.length; count++) {
                        sender_id = chats[count].chat.sender_id;
                        sender_name = chats[count].sender.name;
                        receiver_id = chats[count].chat.receiver_id;
                        message = chats[count].chat.message;
                        sender_image = chats[count].sender_image;
                        msg_date = chats[count].msg_date;


                        if (sender_id == from_user_id) {

                            html += `
                            <div class="chat-content-rightside">
                                    <div class="d-flex flex-row-reverse">
                                        <img src= "` + sender_image + `" width="48"
                                            height="48" class="rounded-circle" alt="" />
                                        <div class="flex-grow-1 me-2">
                                            <p class="mb-0 chat-time text-end"> ` +
                                sender_name + ` , ` + msg_date + `</p>
                                            <p class="chat-right-msg">` +
                                message + `
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            `;

                        } else {

                            if (chats[count].chat.status != 'seen') {
                                update_message_status(chats[count].chat.id, sender_id, receiver_id, 'seen');
                            }


                            html += `
                            <div class="chat-content-leftside">
                                    <div class="d-flex">
                                        <img src= "` + sender_image + `" width="48"
                                            height="48" class="rounded-circle" alt="" />
                                        <div class="flex-grow-1 ms-2">
                                            <p class="mb-0 chat-time"> ` +
                                sender_name + ` , ` + msg_date + `</p>
                                            <p class="chat-left-msg"> ` +
                                message + `
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            `;

                            // set the unread messages count to zero
                            var count_unread_message_element_badge = document.getElementById(
                                'user_unread_message_badge_' + sender_id + '');
                            count_unread_message_element_badge.innerHTML = '';

                        }

                    }

                    document.getElementById('chat-content').innerHTML = html;
                    scroll_top();
                }
            });


        }

        // update the message status .. if status=seen , set the notifications count from this user to 0
        function update_message_status(message_id, from_user_id, to_user_id, status) {

            $.ajax({
                type: "POST",
                url: "/update_message_status", // ChatController/update_message_status
                data: "message_id=" + message_id + "&to_user_id=" + to_user_id + "&from_user_id=" + from_user_id +
                    "&status=" + status,
                success: function(data) {

                    // No Data is coming back from the server
                    // just sort the messages in notification area again...
                    // after the update was done to the message status and notification above
                    sortMessages();
                }
            });
        }

        function scroll_top() {
            document.querySelector('#chat-content').scrollTop =
                document.querySelector('#chat-content').scrollHeight;
        }
    </script>

    <script>
        messaging.onMessage((payload) => {
            var title = payload.data.title;
            var body = JSON.parse(payload.data.body);
            messageReceived(title, body);
        });

        function messageReceived(title, data) {
            console.log('Message received, type: ' + title);

            // the {data} comming from FCMController/notification
            // data : {id(chat_id) , sender_id, sender_name, receiver_id, message, msg_date, status, sender_image,
            //           notification(object : {content, status, count, type} )}
            // the type is : notification->type (Message, Notification)

            // console.log(data);

            var html = '';

            if (to_user_id == '' || to_user_id != data.sender_id) {

                var count_unread_message_element = document.getElementById('user_unread_message_' + data
                    .sender_id + '');


                if (count_unread_message_element) {
                    var count_unread_message_badge = document.getElementById('user_unread_message_badge_' +
                        data.sender_id + '');

                    // var unread_message_content_element =
                    //     document.getElementById('user_unread_message_content_' + data.sender_id + '');


                    count_unread_message_badge.hidden = false;
                    if (count_unread_message_badge.innerHTML == 0) {
                        var n = 1;
                    } else {
                        var n = parseInt(count_unread_message_badge.innerHTML) + 1;
                    }

                    count_unread_message_badge.innerHTML = n;
                    //unread_message_content_element.innerHTML = data.message;
                }

            }
            if (to_user_id != '' && to_user_id == data.sender_id) {

                html += `
                            <div class="chat-content-leftside">
                                <div class="d-flex">
                                    <img src= "` + data.sender_image + `" width="48"
                                            height="48" class="rounded-circle" alt="" />
                                    <div class="flex-grow-1 ms-2">
                                        <p class="mb-0 chat-time"> ` +
                    data.sender_name + ` , ` + data.msg_date + `</p>
                                        <p class="chat-left-msg"> ` +
                    data.message + ` </p>
                                    </div>
                                </div>
                            </div>

                        `;

                update_message_status(data.id, data.sender_id, data.receiver_id, 'seen');
            }


            if (html != '') {
                var previous_chat_element = document.querySelector('#chat-content');
                var chat_history_element = document.querySelector('#chat-content');

                chat_history_element.innerHTML = previous_chat_element.innerHTML + html;
            }

            //check_unread_message();
            sortUsers(userRole, userName);
            scroll_top();

        }
    </script>


    <script>
        function sortUsers(userRole, userName) {

            // var option = document.querySelector('#sort-options').value;

            // var op = $('#sort-options').val();
            window.userRole = userRole;
            window.userName = userName;

            $.ajax({
                type: "GET",
                url: "/sort-users",
                data: "userRole=" + userRole + "&userName=" + userName,
                success: function(data) {
                    // Display message back to the user here
                    //  console.log(data);
                    load_users_list(data);
                }
            });

        }


        function load_users_list(data) {
            var html = '';
            var login_user_id = "{{ Auth::user()->id }}";
            // console.log(data);

            data.forEach(element => {

                //console.log(element);
                user = element.user;
                user_image = element.user_image;
                last_message_content = element.last_message == null ? '' : element.last_message.message;
                last_message_date = element.last_message_date == null ? '' : element.last_message_date;
                unread_messages_count = element.unread;


                var hide = '';
                if (unread_messages_count == 0) {
                    hide = 'hidden';
                }

                html += `
                    <a href = "javascript:;"class = "list-group-item"
                        onclick = "make_chat_area(` + user.id + ` , '` + user.name + `' );
                        load_chat_data(` + login_user_id + ` , ` + user.id + `); ">
                        <div class = "d-flex" >
                            <div class = "chat-user-online" >
                                <img src = ` + user_image + ` width = "42" height = "42"
                                    class = "rounded-circle" alt = "" />
                                <span value = "` + unread_messages_count + `" class = "unread_message"
                                    data-id = "` + user.id + `" id = "user_unread_message_` + user.id + `" >
                                    <span  ` + hide + `
                                        id = "user_unread_message_badge_` + user.id + `"
                                        class = "badge bg-danger rounded-pill">` + unread_messages_count + `
                                    </span>
                                </span>
                            </div>
                `;

                html += `
                    <div class = "flex-grow-1 ms-2" >
                        <h6 class = "mb-0 chat-title"> ` + user.name + `</h6>
                        <p class = "mb-0 chat-msg">` + last_message_content + ` </p>
                    </div>
                    <div id = "last_msg_date" class = "chat-time" dir = "ltr" >
                       ` + last_message_date + `
                    </div>
                    </div>
                    </a>
                `;



            });

            // console.log(html);
            document.getElementById('users_list').innerHTML = html;
        }

        $(document).ready(function() {
            window.userRole = '%';
            window.userName = '%';
            sortUsers(window.userRole, window.userName);
        });
    </script>
@endsection
