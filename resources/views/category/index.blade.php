@extends('layouts.dashboard')

@section('content')
    @php
        $page = 'category';
    @endphp
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Categories\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        @if (Auth::user()->role === 'admin')
                            <a href="{{ route('categories.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Add new category') }}</a>
                        @endif
                    </div>

                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Categories table') }}</h5>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Name in Arabic') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Image') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <th scope="row">{{ $category->id }}</th>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->name_ar }}</td>
                                            <td><p>{{ $category->details }}</p></td>
                                            <td>{{ $category->status }}</td>
                                            <td>
                                                <img src="{{asset($category->image)}}" class="user-img" alt="">
                                            </td>
                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('categories.show', $category) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                    &nbsp; &nbsp;
                                                    <a href="{{ route('categories.edit', $category) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>

                                                    &nbsp; &nbsp;
                                                    @if ($category->status === 'On')
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['categories.disable', $category],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="far fa-2x fa-trash-alt icon-size"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-danger',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @else
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['categories.enable', $category],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="lni lni-reload"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-primary btn-lg',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Name in Arabic') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Image') }}</th>
                                        <th>{{ __('Actions') }}</th>


                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            $('#example').DataTable();
        });
    </script>
@endsection
