@extends('layouts.dashboard')
@section('content')
@php
$page = 'category';
@endphp
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <h3>{{ __('Category Information: ') . $category->name }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">


                    </div>

                </div>
                <!--end breadcrumb-->


                <div class="row">
                    <div class="col-12 col-lg-4 col-xl-4">
                        <div class="card">
                            <img src="{{ asset($category->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h2 class="card-title">{{ $category->name }}</h2>
                                <p class="card-text">{{ $category->details }}</p>

                                <hr>
                                <h4 class="card-title">{{ __('Sub Categories') }}</h4>
                                <ul class="list-group list-group-flush">
                                    @foreach ($category->subcategories as $subcategory)
                                        <li class="list-group-item">{{ $subcategory->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
