@extends('layouts.dashboard')

@section('content')
    @php

        $statusArray = ['On', 'Off'];

    @endphp

@php
$page = 'category';
@endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h2>{{ __('Update on category: ') . $category->name }}</h2>

                            <hr />

                            <form method="POST" action="{{ route('categories.update', $category) }}"
                                enctype="multipart/form-data">
                                @method('patch')
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                    <input type="text" class="form-control" name="name" value="{{ $category->name }}"
                                        required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="name_ar" class="form-label">{{ __('Name in Arabic') }}</label>
                                    <input value="{{ old('name_ar') }}" type="text" class="form-control" name="name_ar"
                                        value="{{ $category->name_ar }} required>

                                    @if ($errors->has('name_ar'))
                                        <span class="text-danger text-left">{{ $errors->first('name_ar') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="details" class="form-label">{{ __('Details') }}</label>
                                    <textarea type="text" class="form-control" name="details" rows="3"
                                        placeholder="Category Description (optional)">{{ $category->details }}</textarea>

                                    @if ($errors->has('details'))
                                        <span class="text-danger text-left">{{ $errors->first('details') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="status" class="form-label">{{ __('Status') }}</label>
                                    <select class="form-control" name="status" id="status">
                                        @foreach ($statusArray as $item)
                                            <option value="{{ $item }}"
                                                {{ $category->status == $item ? 'selected' : '' }}>
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="image" class="form-label">{{ __('Image') }}</label>
                                    <input type="file" class="form-control" name="image" />
                                    @if ($errors->has('image'))
                                        <span class="text-danger text-left">{{ $errors->first('image') }}</span>
                                    @endif

                                    <img src="{{ asset($category->image) }}" width="150" class="img-thumbnail" />
                                    <input type="hidden" name="hidden_image" />
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('categories.index') }}"
                                    class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
