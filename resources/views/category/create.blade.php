@extends('layouts.dashboard')

@section('content')
    @php

        $statusArray = ['On', 'Off'];

    @endphp
@php
$page = 'category';
@endphp
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h2>{{ __('Add new category') }}</h2>

                            <hr />

                            <form method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                    <input value="{{ old('name') }}" type="text" class="form-control" name="name"
                                        placeholder="Name" required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="name_ar" class="form-label">{{ __('Name in Arabic') }}</label>
                                    <input value="{{ old('name_ar') }}" type="text" class="form-control" name="name_ar"
                                        placeholder="Name in Arabic" required>

                                    @if ($errors->has('name_ar'))
                                        <span class="text-danger text-left">{{ $errors->first('name_ar') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="details" class="form-label">{{ __('Details') }}</label>
                                    <textarea type="text" class="form-control" name="details" rows="3"
                                        placeholder="Category Description (optional)"></textarea>
                                    @if ($errors->has('details'))
                                        <span class="text-danger text-left">{{ $errors->first('details') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="status" class="form-label">{{ __('Status') }}</label>
                                    <select class="form-control" name="status" id="status">
                                        @foreach ($statusArray as $item)
                                            <option value="{{ $item }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="image" class="form-label">{{ __('Image') }}</label>
                                    <input type="file" class="form-control" name="image" />
                                    @if ($errors->has('image'))
                                        <span class="text-danger text-left">{{ $errors->first('image') }}</span>
                                    @endif
                                    <input type="hidden" name="hidden_image" />
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('categories.index') }}"
                                    class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
