@extends('layouts.dashboard')

@section('content')

@php

    $statusArray = ['On' , 'Off'];

@endphp

@php
$page = 'setting';
@endphp

<div class="page-wrapper">
    <!--page-content-wrapper-->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h2>{{ __('Update on setting: ') . $setting->key }}</h2>

                        <hr />

                        <form method="POST" action="{{ route('settings.update', $setting) }}">
                            @method('patch')
                            @csrf

                            <div class="mb-3">
                                <label for="key" class="form-label">{{__('Key')}}</label>
                                <input type="text" class="form-control" name="key"
                                    value="{{$setting->key}}"
                                    disabled>

                                @if ($errors->has('key'))
                                    <span class="text-danger text-left">{{ $errors->first('key') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="value" class="form-label">{{__('Value')}}</label>
                                <input type="text" class="form-control" name="value"
                                    value="{{$setting->value}}"
                                    required>

                                @if ($errors->has('value'))
                                    <span class="text-danger text-left">{{ $errors->first('value') }}</span>
                                @endif
                            </div>

{{--
                            <div class="mb-3">
                                <label for="status" class="form-label">{{__('Status')}}</label>
                                <select class="form-control" name="status" id="status">
                                    @foreach ($statusArray as $item)
                                    <option value="{{$item}}" {{($setting->status == $item) ? 'selected':''}}>
                                        {{$item}}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('status'))
                                    <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                @endif
                            </div> --}}


                            <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                            <a href="{{ route('settings.index') }}" class="btn btn-default">{{__('Cancel')}}</button>
                        </form>


                    </div>
                </div>
            </div>



        </div>
    </div>
    <!--end page-content-wrapper-->
</div>
<!--end page-wrapper-->

@endsection


