@extends('layouts.dashboard')

@section('content')
    @php
        $genderArray = ['Other' , 'Male' , 'Female'];
        $statusArray = ['On' , 'Off'];
        $providerTypeArray = ['Company', 'Freelance'];
    @endphp

@php
$page = 'provider';
@endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->



                <div class="card">
                    <div class="card-body">
                        <div>
                            <h5>{{ __('Edit Provider') }}</h5>

                            <hr />


                            <form method="post" action="{{ route('providers.update', $provider->user->id) }}">
                                @method('patch')
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                    <input value="{{ $user->name }}" type="text" class="form-control" name="name"
                                        placeholder="Name" required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="email" class="form-label">{{ __('Email') }}</label>
                                    <input value="{{ $user->email }}" type="email" class="form-control" name="email"
                                        placeholder="Email address" required>
                                    @if ($errors->has('email'))
                                        <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="password" class="form-label">{{ __('Password') }}</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control border-end-0" id="password"
                                            name="password" placeholder="Enter Password">
                                        <a href="javascript:;" class="input-group-text bg-transparent"><i
                                                class="bx bx-hide"></i></a>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="gender" class="form-label">{{ __('Gender') }}</label>
                                    <select class="form-control" name="gender" id="gender">
                                        @foreach ($genderArray as $item)
                                            <option value="{{ $item }}"
                                                {{ $user->gender == $item ? 'selected' : '' }}>
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="text-danger text-left">{{ $errors->first('gender') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="phone" class="form-label">{{ __('Phone Number') }}</label>
                                    <input value="{{ $user->phone }}" type="text" class="form-control" name="phone"
                                        placeholder="Phone Number">
                                    @if ($errors->has('phone'))
                                        <span class="text-danger text-left">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="address" class="form-label">{{ __('Address') }}</label>
                                    <input value="{{ $user->address }}" type="text" class="form-control" name="address"
                                        placeholder="Address">
                                    @if ($errors->has('address'))
                                        <span class="text-danger text-left">{{ $errors->first('address') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="description" class="form-label">{{ __('Description') }}</label>
                                    <textarea type="text" class="form-control" name="description" rows="3"
                                        placeholder="Provider Description">{{ $provider->details }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger text-left">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="status" class="form-label">{{ __('Status') }}</label>
                                    <select class="form-control" name="status" id="status">
                                        @foreach ($statusArray as $item)
                                            <option value="{{ $item }}"
                                                {{ $user->status == $item ? 'selected' : '' }}>
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                                {{--
                            <div class="mb-3">
                                <label for="role" class="form-label">{{__('Role')}}</label>
                                <select class="form-control" onchange="EnableDisable(this)" id="role"
                                    name="role" required>
                                    <option value="">{{__('Select role')}}</option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}"
                                            {{ in_array($role->name, $userRole)
                                                ? 'selected'
                                                : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="status" class="form-label">{{__('Provider Type')}}</label>
                                <select class="form-control" name="type" id="type"
                                        @if (is_null($provider)) disabled="true"  @endif>

                                    @foreach ($providerTypeArray as $item)
                                    <option value="{{$item}}"
                                    @if (!is_null($provider))
                                        {{($provider->type == $item) ? 'selected':''}}
                                    @endif >
                                        {{$item}}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                    <span class="text-danger text-left">{{ $errors->first('type') }}</span>
                                @endif
                            </div> --}}

                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('providers.index') }}"
                                    class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

<script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
<!--Password show & hide js -->
<script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("bx-hide");
                $('#show_hide_password i').removeClass("bx-show");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("bx-hide");
                $('#show_hide_password i').addClass("bx-show");
            }
        });
    });
</script>

<script type="text/javascript">
    function EnableDisable(role) {

        var typeSelect = document.getElementById("type");


        var str = role.options[role.selectedIndex].text;

        if (str == "provider") {

            typeSelect.disabled = false;
        } else {

            typeSelect.disabled = true;
        }
    };
</script>
