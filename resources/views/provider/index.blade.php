@extends('layouts.dashboard')

@section('content')

@php
$page = 'provider';
@endphp


    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Providers\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">

                        <a href="{{ route('providers.create') }}" class="btn btn-primary btn-sm float-right">{{__('Add new provider')}}</a>
                    </div>
                </div>

                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Providers table') }}</h5>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Gender') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Role') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Rate') }}</th>
                                        <th>{{ __('Actions') }}</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($providers as $provider)
                                        <tr>
                                            <th scope="row">{{ $provider->system_user_id }}</th>
                                            <td>{{ $provider->user->name }}</td>
                                            <td>{{ $provider->user->email }}</td>
                                            <td>{{ $provider->user->gender }}</td>
                                            <td>{{ $provider->user->phone }}</td>
                                            <td>{{ $provider->user->address }}</td>
                                            <td><p>{{ $provider->description }}</p></td>
                                            <td>{{ $provider->user->status }}</td>
                                            <td>{{ $provider->user->role }}</td>
                                            <td>{{ $provider->type }}</td>

                                            <td>{{ $providers_ratings['provider' . $provider->id] }}</td>

                                            <td>
                                                <a href="{{ route('profiles.show', $provider->system_user_id) }}">
                                                    <i class="fas fa-eye text-success fa-2x"></i>
                                                </a>
                                                &nbsp; &nbsp;
                                                <a href="{{ route('providers.edit', $provider) }}">
                                                    <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                </a>
                                            </td>


                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Gender') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Role') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Rate') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
 <!--Data Tables js-->
 <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

 <script>
    $(document).ready(function() {
        $("#toggle-btn").click();
        $('#example').DataTable();
    });
</script>

@endsection
