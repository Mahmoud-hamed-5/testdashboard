@extends('layouts.dashboard')

@section('content')
    @php
        $genderArray = ['Other' , 'Male' , 'Female'];

    @endphp
    @php
    $page = 'profile';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">{{__('Profile')}}</div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class='bx bx-home-alt'></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="user-profile-page">
                    <div class="card radius-15">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-7 border-right">
                                    <div class="d-md-flex align-items-center">
                                        <div class="mb-md-0 mb-3">

                                            @php

                                                if ($user->user_image != '') {
                                                    $user_image = asset($user->user_image);
                                                } else {
                                                    if ($user->gender == 'Male') {
                                                        $user_image = asset('images/profile-images/blank-profile-male.jpg');
                                                    }if($user->gender == 'Female') {
                                                        $user_image = asset('images/profile-images/blank-profile-female.jpg');
                                                    }if($user->gender == 'Other') {
                                                        $user_image = asset('images/profile-images/blank-profile-other.jpg');
                                                    }
                                                }
                                            @endphp

                                            <img src="{{ $user_image }}" class="rounded-circle shadow" width="130"
                                                height="130" alt="" />
                                        </div>
                                        <div class="ms-md-4 flex-grow-1">
                                            <div class="d-flex align-items-center mb-1">
                                                <h4 class="mb-0">{{ $user->name }}</h4>
                                                {{-- <p class="mb-0 ms-auto">$44/hr</p> --}}
                                            </div>
                                            <p class="mb-0 text-muted">{{ __('Role: ') . $user->role }}</p>
                                            @if (Auth::user()->role === 'provider')
                                                <p class="mb-0 text-muted">{{ __('Type: ') . $provider->type }}</p>
                                            @endif
                                            <p class="text-primary"><i class="lni lni-mobile"></i>{{ $user->phone }}</p>
                                            {{-- <button type="button" class="btn btn-primary">Connect</button> --}}
                                            {{-- <button type="button" class="btn btn-outline-secondary ml-2">Resume</button> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <table class="table table-sm table-borderless mt-md-0 mt-3">
                                        <tbody>

                                            @if ($user->role === 'provider')
                                                <tr>
                                                    <th>{{ __('Rating') }}:</th>
                                                    <td> <span class="badge badge-success">{{ $provider->rating() }}</span>
                                                    </td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <th>{{ __('Gender') }}:</th>
                                                <td>{{ $user->gender }}</td>
                                            </tr>
                                            <tr>
                                                <th>{{ __('Address') }}:</th>
                                                <td>{{ $user->address }}</td>
                                            </tr>
                                            <tr>
                                                <th>{{ __('Email') }}:</th>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="mb-3 mb-lg-0">
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i class='bx bxl-github'></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i
                                                class='bx bxl-twitter'></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i
                                                class='bx bxl-facebook'></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i
                                                class='bx bxl-linkedin'></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i
                                                class='bx bxl-dribbble'></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-link"><i
                                                class='bx bxl-stack-overflow'></i></a>
                                    </div>
                                </div>
                            </div>
                            <!--end row-->

                            @if (Auth::user()->id === $user->id || Auth::user()->role === 'admin')
                                <ul class="nav nav-pills">

                                    <li class="nav-item"> <a class="nav-link" data-bs-toggle="tab"
                                            href="#Edit-Profile"><span class="p-tab-name">{{__('Edit Profile')}}</span><i
                                                class='bx bx-message-edit font-24 d-sm-none'></i></a>
                                    </li>

                                </ul>

                                <div class="tab-content mt-3">

                                    {{-- Edit Profile --}}
                                    <div class="tab-pane fade" id="Edit-Profile">
                                        <div class="card shadow-none border mb-0 radius-15">
                                            <div class="card-body">
                                                <div class="form-body">
                                                    <div class="row">

                                                        <div class="col-12 col-lg-5 border-right">
                                                            <form class="row g-3" method="POST"
                                                                action="{{ route('profiles.update', $user) }}"
                                                                enctype="multipart/form-data">

                                                                @csrf

                                                                <div class="col-6">
                                                                    <label for="name"
                                                                        class="form-label">{{ __('Name') }}</label>
                                                                    <input value="{{ $user->name }}" type="text"
                                                                        class="form-control" name="name" required>
                                                                    @if ($errors->has('name'))
                                                                        <span
                                                                            class="text-danger text-left">{{ $errors->first('name') }}</span>
                                                                    @endif
                                                                </div>


                                                                <div class="col-6">
                                                                    <label for="gender"
                                                                        class="form-label">{{ __('Gender') }}</label>
                                                                    <select class="form-control" name="gender"
                                                                        id="gender">
                                                                        @foreach ($genderArray as $item)
                                                                            <option value="{{ $item }}"
                                                                                {{ $user->gender == $item ? 'selected' : '' }}>
                                                                                {{ $item }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('gender'))
                                                                        <span
                                                                            class="text-danger text-left">{{ $errors->first('gender') }}</span>
                                                                    @endif
                                                                </div>


                                                                <div class="col-12">
                                                                    <label for="email"
                                                                        class="form-label">{{ __('Email') }}</label>
                                                                    <input value="{{ $user->email }}" type="email"
                                                                        class="form-control" name="email" required>
                                                                    @if ($errors->has('email'))
                                                                        <span
                                                                            class="text-danger text-left">{{ $errors->first('email') }}</span>
                                                                    @endif
                                                                </div>


                                                                <div class="col-12">
                                                                    <label for="password"
                                                                        class="form-label">{{ __('Password') }}</label>
                                                                    <div class="input-group" id="show_hide_password">
                                                                        <input type="password"
                                                                            class="form-control border-end-0"
                                                                            id="password" name="password"
                                                                            placeholder="Enter Password">
                                                                        <a href="javascript:;"
                                                                            class="input-group-text bg-transparent"><i
                                                                                class="bx bx-hide"></i></a>
                                                                    </div>
                                                                </div>

                                                        </div>

                                                        <div class="col-12 col-lg-7">

                                                            <div class="col-12">
                                                                <label for="address"
                                                                    class="form-label">{{ __('Address') }}</label>
                                                                <input value="{{ $user->address }}" type="text"
                                                                    class="form-control" name="address" id="address">
                                                                @if ($errors->has('address'))
                                                                    <span
                                                                        class="text-danger text-left">{{ $errors->first('address') }}</span>
                                                                @endif
                                                            </div>

                                                            <div class="col-12">
                                                                <label for="phone"
                                                                    class="form-label">{{ __('Phone Number') }}</label>
                                                                <input value="{{ $user->phone }}" type="text"
                                                                    class="form-control" name="phone" id="phone">
                                                                @if ($errors->has('phone'))
                                                                    <span
                                                                        class="text-danger text-left">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>

                                                            <div class="col-12">
                                                                <div class="row g-3">
                                                                    <div class="col-12">
                                                                        <div class="mb-3">
                                                                            <label for="image"
                                                                                class="form-label">{{ __('Image') }}</label>
                                                                            <input type="file" class="form-control"
                                                                                name="image" />
                                                                            @if ($errors->has('image'))
                                                                                <span
                                                                                    class="text-danger text-left">{{ $errors->first('image') }}</span>
                                                                            @endif

                                                                         @php
                                                                                if ($user->user_image != '') {
                                                                                $user_image = asset($user->user_image);
                                                                            } else {
                                                                                if ($user->gender == 'Male') {
                                                                                    $user_image = asset('images/profile-images/blank-profile-male.jpg');
                                                                                } else {
                                                                                    $user_image = asset('images/profile-images/blank-profile-female.jpg');
                                                                                }
                                                                            }
                                                                         @endphp

                                                                            <img src="{{ $user_image }}"
                                                                                width="150" class="img-thumbnail" />
                                                                            <input type="hidden" name="hidden_image" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-12 col-lg-7">
                                                            <button type="submit"
                                                                class="btn btn-primary">{{ __('Save') }}</button>
                                                        </div>
                                                        </form>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


<script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
<!--Password show & hide js -->
<script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("bx-hide");
                $('#show_hide_password i').removeClass("bx-show");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("bx-hide");
                $('#show_hide_password i').addClass("bx-show");
            }
        });
    });
</script>
