@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'notification';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Notifications\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        @if (Auth::user()->role === 'admin')
                            <a href="{{ route('notifications.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Broadcast new notification') }}</a>
                        @endif

                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Notifications List') }}</h5>
                        </div>

                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Content') }}</th>
                                        <th>{{ __('Image') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($notifications as $notification)
                                        <tr>
                                            <th scope="row">{{ $notification->id }}</th>
                                            <td>{{ $notification->title }}</td>
                                            <td><p>{{ $notification->content }}</p></td>
                                            <td>
                                                <img src="{{ asset($notification->image) }}" class="user-img" alt="">
                                            </td>

                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('services.show', $notification) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                    &nbsp; &nbsp;
                                                    <a href="{{ route('services.edit', $notification) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Content') }}</th>
                                        <th>{{ __('Image') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            $('#example').DataTable();
        });
    </script>
@endsection
