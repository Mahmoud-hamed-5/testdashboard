@extends('layouts.dashboard')

@section('style')
    <style>
        img {
            max-width: 180px;
        }

        input[type=file] {
            padding: 10px;
            /* background: #2d2d2d; */
        }
    </style>
@endsection

@section('content')
    @php
        $page = 'notification';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Notifications</div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Broadcast Notification</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->

                <div class="row">
                    <div class="col-xl-10 mx-auto">
                        <h6 class="mb-0 text-uppercase">New Broadcast</h6>
                        <hr>
                        <div class="card border-top border-0 border-4 border-primary">
                            <div class="card-body p-5">
                                <div class="card-title d-flex align-items-center">
                                    <div><i class="fadeIn animated bx bx-broadcast me-1 font-22 text-primary"></i>
                                    </div>
                                    <h5 class="mb-0 text-primary">Customize Broadcast</h5>
                                </div>
                                <hr>

                                <form method="POST" action="{{ route('notifications.broadcast') }}" class="row g-3"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-md-6">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title"
                                            value="{{ old('title') }}">

                                        @if ($errors->has('title'))
                                            <span class="text-danger text-left">{{ $errors->first('title') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-12">
                                        <label for="content" class="form-label">Content</label>
                                        <textarea class="form-control" name="content" id="content" placeholder="Content..." rows="3">{{ old('content') }}</textarea>

                                        @if ($errors->has('content'))
                                            <span class="text-danger text-left">{{ $errors->first('content') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-6">
                                        {{-- <label for="image" class="form-label row mr-2">Image</label>
                                        <input id="image" type="file" name="image" accept=".jpg, .png, image/jpeg, image/png"> --}}

                                        <label for="image" class="form-label row mr-2">Image</label>
                                        <input type='file' id="image" name="image" accept=".jpg, .png, image/jpeg, image/png" onchange="readURL(this);" />
                                        <img id="blah" src="image" alt="your image" />

                                        <div>
                                            @if ($errors->has('image'))
                                                <span class="text-danger text-left">{{ $errors->first('image') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary px-5">Push Notification</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    {{-- <script src="{{asset('assets/auth/plugins/Drag-And-Drop/dist/imageuploadify.min.js')}}"></script> --}}

    {{-- <script>
    $(document).ready(function () {
        $('#image-uploadify').imageuploadify();
    })
</script> --}}
@endsection
