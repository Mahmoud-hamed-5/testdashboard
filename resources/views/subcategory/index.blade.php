@extends('layouts.dashboard')

@section('content')
    @php
        $page = 'subcategory';
    @endphp


    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Sub Categories\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        @if (Auth::user()->role === 'admin')
                            <a href="{{ route('subcategories.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Add new subcategory') }}</a>
                        @endif
                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Sub Categories table') }}</h5>
                        </div>
                        <hr/>

                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Category') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($subcategories as $subcategory)
                                        <tr>
                                            <th scope="row">{{ $subcategory->id }}</th>
                                            <td>{{ $subcategory->name }}</td>
                                            <td>{{ $subcategory->category->name }}</td>
                                            <td>{{ $subcategory->details }}</td>
                                            <td>{{ $subcategory->status }}</td>

                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('subcategories.show', $subcategory) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>

                                                    &nbsp; &nbsp;

                                                    <a href="{{ route('subcategories.edit', $subcategory) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>

                                                    &nbsp; &nbsp;

                                                    @if ($subcategory->status === 'On')
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['subcategories.disable', $subcategory],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="far fa-2x fa-trash-alt icon-size"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-danger',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @else
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['subcategories.enable', $subcategory],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="lni lni-reload"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-primary btn-lg',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @endif

                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Category') }}</th>
                                        <th>{{ __('Details') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            $('#example').DataTable();
        });
    </script>
@endsection
