@extends('layouts.dashboard')
@section('content')
@php
$page = 'subcategory';
@endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <h3>{{ __('Sub Category Information: ') . $subcategory->name }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">


                    </div>

                </div>
                <!--end breadcrumb-->


                <div class="row">
                    <div class="col-12 col-lg-4 col-xl-4">
                        <div class="card">
                            <img src="{{ asset($subcategory->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h2 class="card-title">{{ $subcategory->name }}</h2>
                                <p class="card-text">{{ $subcategory->details }}</p>

                                <hr>
                                <h4 class="card-title">{{ __('Main Category') }}</h4>
                                <ul class="list-group list-group-flush">

                                    <li class="list-group-item">{{ $subcategory->category->name }}</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
