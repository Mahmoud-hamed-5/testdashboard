@extends('layouts.dashboard')

@section('content')
    @php
        $page = 'handyman';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Handymen Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">

                        @php
                            if (Auth::user()->role === 'provider') {
                                $route = 'provider.handymen.create';
                            } elseif (Auth::user()->role === 'admin') {
                                $route = 'handymen.create';
                            }
                        @endphp

                        <a href="{{ route($route) }}"
                            class="btn btn-primary btn-sm float-right">{{ __('Add new Handyman') }}</a>
                    </div>

                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Handeymen table') }}</h5>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('User id') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Gender') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Role') }}</th>
                                        <th>{{ __('Provider') }}</th>
                                        <th>{{ __('Actions') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($handymen as $handyman)
                                        <tr>
                                            <th scope="row">{{ $handyman->id }}</th>
                                            <th scope="row">{{ $handyman->system_user_id }}</th>
                                            <td>{{ $handyman->user->name }}</td>
                                            <td>{{ $handyman->user->email }}</td>
                                            <td>{{ $handyman->user->gender }}</td>
                                            <td>{{ $handyman->user->phone }}</td>
                                            <td>{{ $handyman->user->address }}</td>
                                            <td>{{ $handyman->user->status }}</td>
                                            <td>{{ $handyman->user->role }}</td>

                                            <td>{{ $handyman->provider->user->name }}</td>
                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('handymen.show', $handyman) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                    &nbsp; &nbsp;

                                                    <a href="{{ route('handymen.edit', $handyman) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('User id') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Email') }}</th>
                                    <th>{{ __('Gender') }}</th>
                                    <th>{{ __('Phone Number') }}</th>
                                    <th>{{ __('Address') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Role') }}</th>
                                    <th>{{ __('Provider') }}</th>
                                    <th>{{ __('Actions') }}</th>


                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection



@section('script')
 <!--Data Tables js-->
 <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

 <script>
    $(document).ready(function() {
        $("#toggle-btn").click();
        $('#example').DataTable();
    });
</script>

@endsection
