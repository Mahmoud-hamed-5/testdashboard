@extends('layouts.dashboard')

@section('content')

    @php
        $genderArray = ['Other', 'Male', 'Female'];
        $statusArray = ['On', 'Off'];
        $providerTypeArray = ['Company', 'Freelance'];
    @endphp

    @php
        $page = 'handyman';
    @endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h2>{{ __('Add new Handyman') }}</h2>

                            <hr />
                            <div class="lead">

                                @php
                                    if (Auth::user()->role === 'provider') {
                                        $route1 = 'provider.handymen.store';
                                        $route2 = 'provider.handymen.index';
                                    }
                                    if (Auth::user()->role === 'admin') {
                                        $route1 = 'handymen.store';
                                        $route2 = 'handymen.index';
                                    }

                                @endphp
                                @if (Auth::user()->role === 'provider')
                                    {{ __('Add new Handyman to my company') }}
                                @endif
                                @if (Auth::user()->role === 'admin')
                                    {{ __('Add new Handyman and assign provider') }}
                                @endif

                            </div>

                            <form method="POST" action="{{ route($route1) }}">
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                    <input value="{{ old('name') }}" type="text" class="form-control" name="name"
                                        placeholder="Name" required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="email" class="form-label">{{ __('Email') }}</label>
                                    <input value="{{ old('email') }}" type="email" class="form-control" name="email"
                                        placeholder="Email address" required>
                                    @if ($errors->has('email'))
                                        <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="password" class="form-label">{{ __('Password') }}</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control border-end-0" id="password"
                                            name="password" placeholder="Enter Password">
                                        <a href="javascript:;" class="input-group-text bg-transparent"><i
                                                class="bx bx-hide"></i></a>
                                    </div>
                                </div>


                                <div class="mb-3">
                                    <label for="gender" class="form-label">{{ __('Gender') }}</label>
                                    <select class="form-control" name="gender" id="gender">
                                        @foreach ($genderArray as $item)
                                            <option value="{{ $item }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="text-danger text-left">{{ $errors->first('gender') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="phone" class="form-label">{{ __('Phone Number') }}</label>
                                    <input value="{{ old('phone') }}" type="text" class="form-control" name="phone"
                                        placeholder="{{ __('Phone Number (Optional)') }}">
                                    @if ($errors->has('phone'))
                                        <span class="text-danger text-left">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="address" class="form-label">{{ __('Address') }}</label>
                                    <input value="{{ old('address') }}" type="text" class="form-control" name="address"
                                        placeholder="{{ __('Address (Optional)') }}">
                                    @if ($errors->has('address'))
                                        <span class="text-danger text-left">{{ $errors->first('address') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="status" class="form-label">{{ __('Status') }}</label>
                                    <select class="form-control" name="status" id="status">
                                        @foreach ($statusArray as $item)
                                            <option value="{{ $item }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>


                                <div class="mb-3">
                                    <label for="role" class="form-label">{{ __('Role') }}</label>
                                    <select class="form-control" id="role" disabled=true name="role" required>
                                        <option value="">{{ __('handyman') }}</option>
                                    </select>
                                    @if ($errors->has('role'))
                                        <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                    @endif
                                </div>

                                @if (Auth::user()->role === 'admin')
                                    <div class="mb-3">
                                        <label for="provider" class="form-label">{{ __('Provider') }}</label>
                                        <select class="form-control" id="provider" name="provider" required>
                                            <option value="">{{ __('Select Provider') }}</option>
                                            @foreach ($providers as $provider)
                                                <option value="{{ $provider->id }}">{{ $provider->user->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('role'))
                                            <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                        @endif
                                    </div>
                                @endif


                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route($route2) }}" class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->

@endsection



<script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
<!--Password show & hide js -->
<script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("bx-hide");
                $('#show_hide_password i').removeClass("bx-show");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("bx-hide");
                $('#show_hide_password i').addClass("bx-show");
            }
        });
    });
</script>
