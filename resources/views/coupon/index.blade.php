@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'coupon';
    @endphp
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Coupons\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        @if (Auth::user()->role === 'admin')
                            <a href="{{ route('coupons.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Add new coupon') }}</a>
                        @endif

                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Coupons List') }}</h5>
                        </div>

                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Code') }}</th>
                                        <th>{{ __('Points') }}</th>
                                        <th>{{ __('Value') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <th scope="row">{{ $coupon->id }}</th>
                                            <td>{{ $coupon->name }}</td>
                                            <td>{{ $coupon->code }}</td>
                                            <td>{{ $coupon->points }}</td>
                                            <td>{{ $coupon->value }} {{ __('S.P') }}</td>
                                            <td>{{ $coupon->status }}</td>
                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('coupons.show', $coupon) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                    &nbsp; &nbsp;
                                                    <a href="{{ route('coupons.edit', $coupon) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>

                                                    &nbsp; &nbsp;

                                                    @if ($coupon->status === 'On')
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['coupons.disable', $coupon],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="far fa-2x fa-trash-alt icon-size"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-danger',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @else
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['coupons.enable', $coupon],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="lni lni-reload"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-primary btn-lg',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Code') }}</th>
                                        <th>{{ __('Points') }}</th>
                                        <th>{{ __('Value') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>

                                    </tr>
                                </tfoot>
                            </table>
                        </div>


                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            $('#example').DataTable();
        });
    </script>
@endsection
