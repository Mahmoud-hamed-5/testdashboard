@extends('layouts.dashboard')

@section('content')
    @php

        $statusArray = ['On', 'Off'];

    @endphp
@php
$page = 'coupon';
@endphp
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h2>{{ __('Add new coupon') }}</h2>

                            <hr />

                            <form method="POST" action="{{ route('coupons.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Name') }}</label>
                                    <input value="{{ old('name') }}" type="text" class="form-control" name="name"
                                        placeholder="name" required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="points" class="form-label">{{ __('Points') }}</label>
                                    <input value="{{ old('points') }}" type="number" class="form-control" name="points"
                                        placeholder="points" required>

                                    @if ($errors->has('points'))
                                        <span class="text-danger text-left">{{ $errors->first('points') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="value" class="form-label">{{ __('Value') }}</label>
                                    <input value="{{ old('value') }}" type="number" class="form-control" name="value"
                                        placeholder="value" required>

                                    @if ($errors->has('value'))
                                        <span class="text-danger text-left">{{ $errors->first('value') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="status" class="form-label">{{ __('Status') }}</label>
                                    <select class="form-control" name="status" id="status">
                                        @foreach ($statusArray as $item)
                                            <option value="{{ $item }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="image" class="form-label">{{ __('Image') }}</label>
                                    <input type="file" class="form-control" name="image" />
                                    @if ($errors->has('image'))
                                        <span class="text-danger text-left">{{ $errors->first('image') }}</span>
                                    @endif
                                    <input type="hidden" name="hidden_image" />
                                </div>


                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('coupons.index') }}"
                                    class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
