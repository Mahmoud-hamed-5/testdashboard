@extends('layouts.dashboard')

@section('content')
@php
    $page = 'roles';
@endphp
<div class="page-wrapper">
    <!--page-content-wrapper-->
    <div class="page-content-wrapper">
        <div class="page-content">


    <div class="bg-light p-4 rounded">
        <h1>{{ __(ucfirst($role->name)) }}</h1>
        <div class="lead">

        </div>

        <div class="container mt-4">

            <h3>{{__('Assigned permissions')}}</h3>

            <table class="table table-striped">
                <thead>
                    <th scope="col" width="20%">Name</th>
                    <th scope="col" width="1%">Guard</th>
                </thead>

                @foreach($rolePermissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->guard_name }}</td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>


    <div class="mt-4">
        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-info">{{__('Edit')}}</a>
        <a href="{{ route('roles.index') }}" class="btn btn-default">{{__('Back')}}</a>
    </div>

</div>
</div>
<!--end page-content-wrapper-->
</div>
<!--end page-wrapper-->
@endsection






