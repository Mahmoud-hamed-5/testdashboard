@extends('layouts.dashboard')
@section('content')
@php
    $page = 'roles';
@endphp
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Roles\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">

                        <a href="{{ route('roles.create') }}"
                            class="btn btn-primary btn-sm float-right">{{ __('Add new role') }}</a>
                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('ٌRoles table') }}</h5>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>

                                        <th></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $key => $role)
                                        <tr>
                                            <td>{{ $role->id }}</td>
                                            <td>{{ $role->name }}</td>

                                            <td>
                                                <a href="{{ route('roles.show', $role) }}">
                                                    <i class="fas fa-eye text-success fa-2x"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('roles.edit', $role) }}">
                                                    <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                </a>
                                            </td>

                                            <td>
                                                <a href="{{ route('roles.destroy', $role) }}">
                                                    <i class="fa-solid text-danger fa-2x fa-trash-can"></i>
                                                </a>
                                            </td>



                                        </tr>
                                    @endforeach
                                </tbody>


                            </table>

                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
