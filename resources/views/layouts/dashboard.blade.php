<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!--favicon-->
    <link rel="icon" href="{{ asset('assets/auth/images/DR-FIX.png') }}" type="image/png" />

    <!-- Fonts -->


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />


    <!-- Styles -->

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">


    <!--Data Tables -->
    <link href="{{ asset('assets/auth/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css">
        <link href="{{asset('assets/auth/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Vector CSS -->
    {{-- <link href="{{ asset('assets/auth/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" /> --}}

    <!--plugins-->
    <link rel="stylesheet" href="{{ asset('assets/auth/plugins/notifications/css/lobibox.min.css') }}" />
    <link href="{{ asset('assets/auth/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/auth/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/auth/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
    <link href="{{asset('assets/auth/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/auth/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />

    <link href="{{ asset('assets/auth/plugins/datetimepicker/css/classic.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/auth/plugins/datetimepicker/css/classic.time.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/auth/plugins/datetimepicker/css/classic.date.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('assets/auth/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css')}}">

    {{-- <link href="{{asset('assets/auth/plugins/fancy-file-uploader/fancy_fileupload.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/auth/plugins/Drag-And-Drop/dist/imageuploadify.min.css')}}" rel="stylesheet" /> --}}

    <!-- loader-->
    <link href="{{ asset('assets/auth/css/pace.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/auth/js/pace.min.js') }}"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/bootstrap.min.css') }}" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto&display=swap" />
    <link href="{{ asset('assets/auth/css/animate.css') }}" rel="stylesheet" type="text/css" />

    {{-- <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto&display=swap" /> --}}

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@800;1000&display=swap" rel="stylesheet">

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;500;800;1000&display=swap');
    </style>

    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/icons.css') }}" />
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/auth/css/dark-sidebar.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/auth/css/dark-theme.css') }}" />
    @yield('style')

    <!-- JQuery-->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios@1.1.2/dist/axios.min.js"></script>

    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->

    @php
        $page;

    @endphp

    <script>
        page = "{{ $page }}";
        var to_user_id = "";
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyBho9byIol1dSE-Uu2pfcSGUndMIS8AWN0",
            authDomain: "dr-fix-d4aa6.firebaseapp.com",
            databaseURL: "https://dr-fix-d4aa6-default-rtdb.firebaseio.com",
            projectId: "dr-fix-d4aa6",
            storageBucket: "dr-fix-d4aa6.appspot.com",
            messagingSenderId: "425020268625",
            appId: "1:425020268625:web:d9d3f90cf402d9e53f1870",
            measurementId: "G-S9JS3LHNNZ"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        const messaging = firebase.messaging();

        function initFirebaseMessagingRegistration() {


            messaging.requestPermission().then(function() {
                return messaging.getToken()
            }).then(function(token) {

                axios.post("{{ route('fcmToken') }}", {
                    _method: "POST",
                    token
                }).then(({
                    data
                }) => {
                    console.log(data)
                }).catch(({
                    response: {
                        data
                    }
                }) => {
                    console.error(data)
                })

            }).catch(function(err) {
                console.log(`Token Error :: ${err}`);
            });
        }

        initFirebaseMessagingRegistration();

        function onTokenRefresh() {
            initFirebaseMessagingRegistration();

        }

        navigator.serviceWorker.onmessage = (event) => {
            // Background .. the browser is minimized .. or not in App?!
            if (event.data.title == 'background Message Notification') {
                //console.log(title);

                var title = event.data.title;
                var data = event.data.body;
                var type = event.data.type;
                // console.log('Type : ' + type);
                if (type == 'Message') {
                    notifyUser(type, data);
                }
                if (type == 'Booking') {
                    console.log(type + ' background');
                    // console.log(data);
                    //anim5_noti('New Booking, From: ' + data.user_name, 'For Service: ' + data.service_name, sound);

                    notifyUser(type, data);
                }
                if (type == 'Custom') {
                    console.log(type + ' background');
                    // console.log(data);
                    //anim5_noti('New Booking, From: ' + data.user_name, 'For Service: ' + data.service_name, sound);

                    notifyUser(type, data);
                }

                /////////// ...................................................................////////////////
                //.. if the Browser is open, But not in App/Chat page
            } else if (event.data.data.title == 'Message' && page != 'chat') {
                console.log('Foreground Message Notification');

                var type = event.data.data.title;
                var data = JSON.parse(event.data.data.body);
                // console.log('Type : ' + type);

                notifyUser(type, data);
            } else if (event.data.data.title == 'Booking') {
                // To Do with app notifications
                var type = event.data.data.title;
                var data = JSON.parse(event.data.data.body);
                console.log('Booking Notification');
                // console.log(data);
                //anim5_noti('New Booking, From: ' + data.user_name, 'For Service: ' + data.service_name, sound);

                notifyUser(type, data);
            }


        }


        // the {data} comming from FCMController/notification
        // data : {id(chat_id) , sender_id, sender_name, receiver_id, message, msg_date, status, sender_image,
        //           notification(object : {content, status, count, type} )}
        // the type is : notification->type (Message, Notification)
        function notifyUser(type, data) {


            if (type === 'Message') {
                sound = "{{ asset('sounds/sound2') }}";

                // show an image notification with sound, display senderName and message , direct url to chat page
                // this function is located in (public\assets\auth\plugins\notifications\js\notification-custom-script.js)
                img_default_noti(data.sender_name, data.message, data.sender_image, sound, "{{ route('chats.index') }}");

                // sort the messages in notification area ...
                // after the above message notification was received
                sortMessages();
            }

            if (type === 'Booking') {
                sound = "{{ asset('sounds/sound4') }}";

                notificationTitle = data.title;
                notificationBody = 'Service: ' + data.service_name;
                anim5_noti(notificationTitle, notificationBody, sound);

                sortNotifications();
            }

        }

        // not longer used !! .. it can be deleted
        function update_notification_area(body) {

            sortMessages();
        }
    </script>

    <script>
        function sortMessages() {
            // request data form NotificationController/sort_messages function
            $.ajax({
                type: "GET",
                url: "/sort-messages",
                // data: "option=" + option,
                success: function(data) {
                    // call function load_messages_list to put the messages notifications
                    // in the notifications area

                    // console.log(data);
                    load_messages_list(data);
                }
            });

        }


        // the {data} comming from NotificationController/sort_messages
        // data: { notification(object : {content, status, count, type} ,sender_image, notification_date,
        // sender(object : {all sender informations (id,name,gender,...)} }
        function load_messages_list(data) {

            var html = '';
            var login_user_id = "{{ Auth::user()->id }}";
            // console.log(data);

            data.forEach(element => {

                sender_id = element.sender.id;
                sender_name = element.sender.name
                sender_image = element.sender_image;

                unread_messages_count = element.notification.count;
                unread_message_content = element.notification.content;
                unread_message_date = element.notification_date;

                chats_route = "{{route('chats.index')}}"

                var hide = '';
                if (unread_messages_count == 0) {
                    hide = 'hidden';
                }

                html += `
                    <a id="user_notification_element_` + sender_id + `"
                        class="dropdown-item" href="`+ chats_route +`">

                        <div class="d-flex align-items-center">
                            <div class="user-online">
                                <img src = "` + sender_image + `"
                                    class="msg-avatar" alt="user avatar">

                                <span value="` + unread_messages_count + `" class="unread_message"
                                    data_id="` + sender_id + `" id="user_unread_message_userid">
                                     <span id="user_unread_notification_badge_` + sender_id + `"
                                        class="badge bg-danger rounded-pill">` + unread_messages_count + `</span>
                                </span>

                            </div>
                            <div class="flex-grow-1">
                                <h6 class="msg-name">` + sender_name + `
                                    <span class="msg-time float-end" dir="ltr">` + unread_message_date + `</span>
                                </h6>
                                <p id="user_unread_notification_content_` + sender_id + `" class="msg-info"> ` +
                    unread_message_content + `</p>
                            </div>
                        </div>
                    </a>

                `;



            });

            var unseen_conversations_count = data.length;

            document.getElementById('messages_list').innerHTML = html;
            document.getElementById('unseen_conversations_count').innerHTML = unseen_conversations_count + ' New';
            document.getElementById('unseen_conversations_count_badge').innerHTML = unseen_conversations_count;
            document.getElementById('unseen_conversations_count_badge2').innerHTML = unseen_conversations_count;
        }
    </script>


    <script>
        function sortNotifications() {
            // request data form NotificationController/sort_notifications function
            $.ajax({
                type: "GET",
                url: "/sort-notifications",
                // data: "option=" + option,
                success: function(data) {
                    // call function load_messages_list to put the messages notifications
                    // in the notifications area

                    // console.log(data);
                    load_notifications_list(data);
                }
            });

        }


        // the {data} comming from NotificationController/sort_messages
        // data: {
        //    1. ordered_notifications(object :{
        //               1.1 - notification (object : {content, status, count, type, booking_id} ),
        //               1.2 - sender (object : {all sender informations [id,name,gender,...] } ),
        //               1.3 - notification_date,
        //               1.4 - service_name
        //              }),
        //    2. unseen_notifications_count}
        function load_notifications_list(data) {

            var html = '';
            var login_user_role = "{{ Auth::user()->role }}";
            var login_user_id = "{{ Auth::user()->id }}";
            // console.log(data);

            data.ordered_notifications.forEach(element => {

                sender_id = element.sender.id;
                sender_name = element.sender.name;

                service_name = element.service_name;

                // unread_messages_count = element.notification.count;
                notification_state = element.notification.content;
                notification_date = element.notification_date;

                booking_id = element.notification.booking_id;

                var hide = '';
                if (element.notification.status == 'seen') {
                    hide = 'hidden';
                }

                var booking_route = '';
                if (login_user_role === 'admin'){
                    booking_route = `{{ route('bookings.edit' ,':booking_id') }}`;
                    booking_route = booking_route.replace(':booking_id', booking_id);
                }if (login_user_role === 'provider'){
                    booking_route = "{{ route('provider.bookings.edit' ,'') }}" + "/" + booking_id;
                }



                html += `
                    <a class="dropdown-item" href="`+booking_route+`">
                        <div class="d-flex align-items-center">
                            <div class="notify bg-light-primary text-primary"><i
                                class="bx bx-cart-alt"></i>

                            </div>

                           <div class="flex-grow-1">
                                <h6 class="msg-name">` + service_name + `
                                    <span class="msg-time float-end" dir="ltr">` + notification_date + `</span>
                                </h6>

                                <div class="msg-name">
                                    <span class="msg-time float-end">
                                        <span class="unread_message">
                                            <span id="unseen_notification_badge_` + sender_id + `" ` + hide + `
                                                class="badge bg-danger rounded-pill">!
                                            </span>
                                        </span>
                                    </span>
                                </div>

                                <p class="msg-info">` + notification_state + `</p>
                            </div>
                        </div>
                    </a>

                `;

            });

            var unseen_notifications_count = data.unseen_notifications_count;

            document.getElementById('notifications_list').innerHTML = html;
            document.getElementById('unseen_notifications_count').innerHTML = unseen_notifications_count + ' New';
            document.getElementById('unseen_notifications_count_badge').innerHTML = unseen_notifications_count;
            document.getElementById('unseen_notifications_count_badge2').innerHTML = unseen_notifications_count;
        }
    </script>

</head>


<body>
    <!-- wrapper -->
    <div class="wrapper">
        <!--sidebar-wrapper-->
        <div class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
                <div class="">
                    <img src="{{ asset('assets/auth/images/DR-FIX.png') }}" class="logo-icon-2" alt="logo" />
                </div>
                <div>
                    <h4 class="logo-text">{{ __('FIX') }}</h4>
                    <button type="button" id="activate_page" onclick="clickToActivate()" hidden></button>
                </div>
                <a href="javascript:;" class="toggle-btn ms-auto"> <i class="bx bx-menu"></i>
                </a>
            </div>

            <!--navigation-->

            <ul class="metismenu" id="menu" >

                {{-- Main --}}
                <li>
                    <a href="{{route('admin.index')}}">
                        <div class="parent-icon icon-color-1"><i class="lni lni-dashboard"></i>
                        </div>
                        <div class="menu-title">{{ __('Dashboard') }}</div>
                    </a>

                </li>


                {{-- Services --}}
                <li class="menu-label">{{ __('SERVICES') }}</li>

                {{-- Categories --}}
                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-10"><i class="lni lni-gallery"></i>
                        </div>
                        <div class="menu-title">{{ __('Categories') }}</div>
                    </a>
                    @if (Auth::user()->role === 'admin')
                        <ul>
                            <li> <a href="{{ route('categories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Categories List') }}</a>
                            </li>
                            <li> <a href="{{ route('categories.create') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Add Category') }}</a>
                            </li>
                        </ul>
                    @endif
                    @if (Auth::user()->role === 'provider')
                        <ul>
                            <li> <a href="{{ route('provider.categories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Categories List') }}</a>
                            </li>
                        </ul>
                    @endif

                    @if (Auth::user()->role === 'handyman')
                        <ul>
                            <li> <a href="{{ route('handyman.categories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Categories List') }}</a>
                            </li>
                        </ul>
                    @endif

                </li>

                {{-- Sub Categories --}}
                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-11"><i class="fa-solid fa-folder-tree"></i>
                        </div>
                        <div class="menu-title">{{ __('Sub Categories') }}</div>
                    </a>
                    @if (Auth::user()->role === 'admin')
                        <ul>
                            <li> <a href="{{ route('subcategories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Sub Categories List') }}</a>
                            </li>
                            <li> <a href="{{ route('subcategories.create') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Add Sub Category') }}</a>
                            </li>
                        </ul>
                    @endif
                    @if (Auth::user()->role === 'provider')
                        <ul>
                            <li> <a href="{{ route('provider.subcategories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Sub Categories List') }}</a>
                            </li>
                        </ul>
                    @endif
                    @if (Auth::user()->role === 'handyman')
                        <ul>
                            <li> <a href="{{ route('handyman.subcategories.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Sub Categories List') }}</a>
                            </li>
                        </ul>
                    @endif
                </li>

                {{-- Services --}}
                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-12"> <i class="bx bx-donate-blood"></i>
                        </div>
                        <div class="menu-title">{{ __('Services') }}</div>
                    </a>
                    @if (Auth::user()->role === 'admin')
                        <ul>
                            <li> <a href="{{ route('services.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Services List') }}</a>
                            </li>
                            <li> <a href="{{ route('services.create') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Add Service') }}</a>
                            </li>
                        </ul>
                    @endif
                    @if (Auth::user()->role === 'provider')
                        <ul>
                            <li> <a href="{{ route('provider.services.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Services List') }}</a>
                            </li>
                            <li> <a href="{{ route('provider.services.create') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Add Service') }}</a>
                            </li>
                        </ul>
                    @endif
                </li>

                {{-- Tables --}}
                <li class="menu-label">{{ __('Tables') }}</li>

                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-2"><i class="bx bx-grid-alt"></i>
                        </div>
                        <div class="menu-title">{{ __('Tables') }}</div>
                    </a>
                    <ul>


                        @if (Auth::user()->role === 'admin')
                            <li> <a href="{{ route('users.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Customers') }}</a>
                            <li> <a href="{{ route('providers.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Providers') }}</a>
                            </li>
                            <li> <a href="{{ route('handymen.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Handymen') }}</a>
                            </li>

                            {{-- <li> <a href="{{ route('roles.index') }}"><i
                                          class="bx bx-right-arrow-alt"></i>{{ __('Roles') }}</a>
                              </li>
                              <li> <a href="{{ route('permissions.index') }}"><i
                                          class="bx bx-right-arrow-alt"></i>{{ __('Permissions') }}</a>
                              </li> --}}
                        @endif
                        @php
                            $provider_type = Auth::user()->providers->where('system_user_id', Auth::user()->id)->value('type');
                        @endphp
                        @if (Auth::user()->role === 'provider' && $provider_type === 'Company')
                            <li> <a href="{{ route('provider.handymen.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Handymen') }}</a>
                            </li>
                        @endif

                    </ul>
                </li>

                {{-- Promotions --}}
                <li class="menu-label">{{ __('PROMOTIONS') }}</li>

                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-10"><i class="fa-solid fa-gift"></i>
                        </div>
                        <div class="menu-title">{{ __('Coupons') }}</div>
                    </a>
                    <ul>
                        @if (Auth::user()->role === 'admin')
                            <li> <a href="{{ route('coupons.index') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Coupons List') }}</a>
                            <li> <a href="{{ route('coupons.create') }}"><i
                                        class="bx bx-right-arrow-alt"></i>{{ __('Add new coupon') }}</a>
                            </li>
                        @endif
                    </ul>
                </li>



                {{-- Booking --}}
                <li class="menu-label">{{ __('BOOKING') }}</li>

                <li>
                    @php
                        switch (Auth::user()->role) {
                            case 'admin':
                                $route = route('bookings.index');
                                break;

                            case 'handyman':
                                $route = route('handyman.bookings.index');
                                break;

                            case 'provider':
                                $route = route('provider.bookings.index');
                                break;

                            default:
                                $route = '#';
                                break;
                        }
                    @endphp
                    <div class="flex-grow-1">


                        <a href="{{ $route }}">

                            <div class="parent-icon icon-color-6"><i class="bx bx-task"></i>

                            </div>

                            <div class="menu-title">{{ __('Bookings') }}
                                <span class="badge bg-success rounded-pill">
                                    <span id="unseen_notifications_count_badge2" class="msg-time float-end"></span>
                                </span>
                            </div>
                        </a>
                    </div>
                </li>

                {{-- Apps --}}
                <li class="menu-label">{{ __('Apps') }}</li>
                {{-- <li>
					<a href="emailbox.html">
						<div class="parent-icon icon-color-2"><i class="bx bx-envelope"></i>
						</div>
						<div class="menu-title">Email</div>
					</a>
				</li> --}}
                <li>
                    <a href="{{ route('chats.index') }}">
                        <div class="parent-icon icon-color-3"> <i class="bx bx-conversation"></i>

                        </div>
                        <div class="menu-title">{{ __('Chat Box') }}
                            <span class="badge bg-info rounded-pill">
                                <span id="unseen_conversations_count_badge2" class="msg-time float-end"></span>
                            </span></div>
                    </a>

                    <a href="{{route('notifications.index')}}">
						<div class="parent-icon icon-color-2"><i class="fadeIn animated bx bx-broadcast"></i>
						</div>
						<div class="menu-title">{{ __('Broadcast') }}</div>
					</a>
                </li>




                {{-- Transactions --}}
                {{-- <li class="menu-label">{{ __('TRANSACTIONS') }}</li>

                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon icon-color-10"><i class="bx bx-spa"></i>
                        </div>
                        <div class="menu-title">{{ __('Payment') }}</div>
                    </a>
                    <ul>
                        <li> <a href="component-alerts.html"><i class="bx bx-right-arrow-alt"></i>Alerts</a>
                        </li>
                        <li> <a href="component-bedges.html"><i class="bx bx-right-arrow-alt"></i>Badge</a>
                        </li>
                    </ul>
                </li> --}}



                {{-- System --}}
                @if (Auth::user()->role === 'admin')
                    <li class="menu-label">{{ __('SYSTEM') }}</li>
                    <li>
                        <a href="{{ route('settings.index') }}">
                            <div class="parent-icon icon-color-10"><i class="fa-solid fa-gear"></i>
                            </div>
                            <div class="menu-title">{{ __('Settings') }}</div>
                        </a>
                    </li>
                @endif


            </ul>

            <!--end navigation-->


        </div>
        <!--end sidebar-wrapper-->


        <!--header-->
        <header class="top-header">
            <nav class="navbar navbar-expand">
                <div class="left-topbar d-flex align-items-center">
                    <a href="javascript:;" id="toggle-btn" class="toggle-btn"> <i class="bx bx-menu"></i>
                    </a>
                </div>
                <div class="flex-grow-1 search-bar">
                    <div class="input-group">

                    </div>
                </div>
                <div class="right-topbar ms-auto">
                    <ul class="navbar-nav">
                        <li class="nav-item search-btn-mobile">
                            <a class="nav-link position-relative" href="javascript:;"> <i
                                    class="bx bx-search vertical-align-middle"></i>
                            </a>
                        </li>


                        {{-- start Messages --}}
                        <li class="nav-item dropdown dropdown-lg">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative"
                                href="javascript:;" data-bs-toggle="dropdown"> <span
                                    id="unseen_conversations_count_badge" class="msg-count"></span>
                                <i class="bx bx-comment-detail vertical-align-middle"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="javascript:;">
                                    <div class="msg-header">
                                        <h6 id="unseen_conversations_count" class="msg-header-title" dir="ltr">
                                        </h6>
                                        <p class="msg-header-subtitle">Chat Messages</p>
                                    </div>
                                </a>

                                <div id="messages_list" class="header-message-list">

                                </div>

                                <a href="/chats">
                                    <div class="text-center msg-footer">View All Messages</div>
                                </a>
                            </div>
                        </li>
                        {{-- End Messages --}}

                        {{-- start Notifications --}}
                        <li class="nav-item dropdown dropdown-lg">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative"
                                href="javascript:;" data-bs-toggle="dropdown"> <i
                                    class="bx bx-bell vertical-align-middle"></i>
                                <span id="unseen_notifications_count_badge" class="msg-count"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">

                                <a href="javascript:;">
                                    <div class="msg-header">
                                        <h6 id="unseen_notifications_count" class="msg-header-title" dir="ltr">
                                        </h6>
                                        <p class="msg-header-subtitle">Application Notifications</p>
                                    </div>
                                </a>

                                <div id="notifications_list" class="header-notifications-list">

                                    @php
                                        if (Auth::user()->role === 'admin'){
                                            $booking_route = route('bookings.index');
                                        } else{
                                            $booking_route = route(Auth::user()->role .'.bookings.index');
                                        }
                                    @endphp

                                </div>
                                <a href="{{$booking_route}}">
                                    <div class="text-center msg-footer">View All Notifications</div>
                                </a>
                            </div>
                        </li>
                        {{-- End Notifications --}}


                        {{-- start Profile --}}

                        @php

                            if (Auth::user()->user_image != '') {
                                $user_image = asset(Auth::user()->user_image);
                            } else {
                                if (Auth::user()->gender == 'Male') {
                                    $user_image = asset('images/profile-images/blank-profile-male.jpg');
                                }
                                if (Auth::user()->gender == 'Female') {
                                    $user_image = asset('images/profile-images/blank-profile-female.jpg');
                                }
                                if (Auth::user()->gender == 'Other') {
                                    $user_image = asset('images/profile-images/blank-profile-other.jpg');
                                }
                            }
                        @endphp

                        <li class="nav-item dropdown dropdown-user-profile">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;"
                                data-bs-toggle="dropdown">
                                <div class="d-flex user-box align-items-center">
                                    <div class="user-info">
                                    </div>

                                    <img src="{{ $user_image }}" class="user-img" alt="user avatar" />
                                    &nbsp;

                                    <p class="user-name mb-0">{{ Auth::user()->name }}</p>

                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="{{ route('profiles.index') }}"><i
                                        class="bx bx-user"></i><span>{{ __('Profile') }}</span></a>


                                <div class="dropdown-divider mb-0"></div>

                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="dropdown-item text-danger">
                                        <i class="bx bx-power-off"></i>
                                        <span>{{ __('Logout') }}</span></button>
                                </form>

                            </div>
                        </li>
                        {{-- End Profile --}}


                        {{-- start Language --}}
                        <li class="nav-item dropdown dropdown-language">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;"
                                data-bs-toggle="dropdown">
                                <div class="lang d-flex">
                                    <div>
                                        <i class="{{ $locales_flags[$current_locale] }}"></i>

                                        <span>{{ __('Language') }}</span>
                                    </div>
                                </div>
                            </a>

                            <div class="dropdown-menu dropdown-menu-end">

                                @foreach ($available_locales as $locale_name => $available_locale)
                                    @if ($available_locale === $current_locale)
                                        {{-- <span class="{{$locales_flags[$locale_name]}}">{{ $locale_name }}</span> --}}
                                        {{-- href="language/{{ $available_locale }}" --}}
                                        <span class="dropdown-item">
                                            <i class="{{ $locales_flags[$available_locale] }}"></i>
                                            <span>{{ $locale_name }}</span>
                                        </span>
                                    @else
                                        <a class="dropdown-item" href="{{ url('language/' . $available_locale) }}">
                                            <i class="{{ $locales_flags[$available_locale] }}"></i>
                                            <span>{{ $locale_name }}</span>
                                        </a>
                                    @endif
                                @endforeach



                            </div>
                        </li>
                        {{-- End Language --}}


                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->



        @yield('content')


        <!--start overlay-->
        <div class="overlay toggle-btn-mobile"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i
                class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->
        <!--footer -->
        <div class="footer">
            <p class="mb-0">Handyman Service @2023 | Developed By : <a href="#" target="_blank">Dr Social</a>
            </p>

        </div>
        <!-- end footer -->
    </div>
    <!-- end wrapper -->
    <!--start switcher-->
    {{-- <div class="switcher-body">
        <button class="btn btn-primary btn-switcher shadow-sm" type="button" data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><i
                class="bx bx-cog bx-spin"></i></button>
        <div class="offcanvas offcanvas-end shadow border-start-0 p-2" data-bs-scroll="true" data-bs-backdrop="false"
            tabindex="-1" id="offcanvasScrolling">
            <div class="offcanvas-header border-bottom">
                <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Theme Customizer</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
            </div>
            <div class="offcanvas-body">
                <h6 class="mb-0">Theme Variation</h6>
                <hr>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="lightmode"
                        value="option1" checked>
                    <label class="form-check-label" for="lightmode">Light</label>
                </div>
                <hr>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="darkmode"
                        value="option2">
                    <label class="form-check-label" for="darkmode">Dark</label>
                </div>
                <hr>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="darksidebar"
                        value="option3">
                    <label class="form-check-label" for="darksidebar">Semi Dark</label>
                </div>
                <hr>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="ColorLessIcons"
                        value="option3">
                    <label class="form-check-label" for="ColorLessIcons">Color Less Icons</label>
                </div>
            </div>
        </div>
    </div> --}}
    <!--end switcher-->



    <!-- JavaScript -->

    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/auth/js/bootstrap.bundle.min.js') }}"></script>

    <!--plugins-->
    <script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/simplebar/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/metismenu/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>

    <!--notification js -->
    <script src="{{ asset('assets/auth/plugins/notifications/js/lobibox.min.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/notifications/js/notifications.min.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/notifications/js/notification-custom-script.js') }}"></script>

    <!-- App JS -->
    <script src="{{ asset('assets/auth/js/app.js') }}"></script>

    <!--Data Tables js-->
    {{-- <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script> --}}

    <script>
        $(document).ready(function() {
            //$('#example').DataTable();
            $('#activate_page').click();

            sortMessages();
            sortNotifications();
        });

        function clickToActivate() {
            console.log('activated');
        }
    </script>




    <script>
        // update the message status .. if status=seen , set the notifications count from this user to 0
        function update_notification_status(message_id, from_user_id, to_user_id, status) {

            $.ajax({
                type: "POST",
                url: "/update_message_status", // ChatController/update_message_status
                data: "message_id=" + message_id + "&to_user_id=" + to_user_id + "&from_user_id=" + from_user_id +
                    "&status=" + status,
                success: function(data) {

                    // No Data is coming back from the server
                    // just sort the notifications in notification area again...
                    // after the update was done to the notification status above
                    sortNotifications();
                }
            });
        }
    </script>


    @yield('script')
    {{-- <script>
		new PerfectScrollbar('.chat-list');
		new PerfectScrollbar('.chat-content');
	</script> --}}

</body>



</html>
