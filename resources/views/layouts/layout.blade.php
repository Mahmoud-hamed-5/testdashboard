<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Handyman Service</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>


    <!--Data Tables -->
    <link href="{{ asset('assets/auth/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css">
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios@1.1.2/dist/axios.min.js"></script>


    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->

    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyBho9byIol1dSE-Uu2pfcSGUndMIS8AWN0",
            authDomain: "dr-fix-d4aa6.firebaseapp.com",
            databaseURL: "https://dr-fix-d4aa6-default-rtdb.firebaseio.com",
            projectId: "dr-fix-d4aa6",
            storageBucket: "dr-fix-d4aa6.appspot.com",
            messagingSenderId: "425020268625",
            appId: "1:425020268625:web:d9d3f90cf402d9e53f1870",
            measurementId: "G-S9JS3LHNNZ"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        const messaging = firebase.messaging();

        function initFirebaseMessagingRegistration() {
            messaging.requestPermission().then(function() {
                return messaging.getToken()
            }).then(function(token) {

                axios.post("{{ route('fcmToken') }}", {
                    _method: "PATCH",
                    token
                }).then(({
                    data
                }) => {
                    console.log(data)
                }).catch(({
                    response: {
                        data
                    }
                }) => {
                    console.error(data)
                })

            }).catch(function(err) {
                console.log(`Token Error :: ${err}`);
            });
        }

        initFirebaseMessagingRegistration();


        function onTokenRefresh() {
            initFirebaseMessagingRegistration();
        }

        //  messaging.onMessage(function({data:{body,title}}){
        //      new Notification(title, {body});
        // console.log('Message received');
        // console.log(body);
        //  });

        messaging.onMessage((payload) => {
            alert(payload.title + " " + payload.message);

            var html = '';
            if (payload.from_user_id == Auth::user()->id) {

                var icon_style = '';

                html += `
			    <div class="row">
				    <div class="col col-3">&nbsp;</div>
				    <div class="col col-9 alert alert-success text-dark shadow-sm">
					    ` + payload.message + `
				    </div>
			    </div>
                `;

            } else {
               // if (to_user_id != '') {
                    html += `
				        <div class="row">
					        <div class="col col-9 alert alert-light text-dark shadow-sm">
					            ` + payload.message + `
					        </div>
				        </div>
				    `;

                    //update_message_status(data.chat_message_id, data.from_user_id, data.to_user_id, 'Seen');
               // }
            }

            if (html != '') {
                var previous_chat_element = document.querySelector('#chat-content');
                var chat_history_element = document.querySelector('#chat-content');

                chat_history_element.innerHTML = previous_chat_element.innerHTML + html;
            }

            //check_unread_message();
            scroll_top();
        });
    </script>


</head>

<body>

    <nav class="navbar navbar-light navbar-expand-lg mb-5" style="background-color: #e3f2fd;">
        <div class="container">
            <a class="navbar-brand mr-auto" href="#">Handyman Service</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">

                <ul class="navbar-nav">
                    @guest

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Register</a>
                        </li>
                    @else
                        <li class="nav-item">
                            @if (Auth::user()->user_image != '')
                                <a class="nav-link" href="#">
                                    <b>Welcom <img src="{{ asset('images/profile-images/' . Auth::user()->user_image) }}"
                                            width="35" class="rounded-circle" />
                                        {{ Auth::user()->name }}
                                    </b>
                                </a>
                            @else
                                <a class="nav-link" href="#">
                                    <b>Welcom
                                        @if (Auth::user()->gender == 'Male')
                                            <img src="{{ asset('images/profile-images/blank-profile-male.jpg') }}"
                                                width="35" class="rounded-circle" />
                                        @else
                                            <img src="{{ asset('images/profile-images/blank-profile-female.jpg') }}"
                                                width="35" class="rounded-circle" />
                                        @endif

                                        &nbsp;
                                        {{ Auth::user()->name }}
                                    </b>
                                </a>
                            @endif
                        </li>

                        <li class="nav-item">
                            {{-- <a class="nav-link" href="{{ route('profile') }}">Profile</a> --}}
                        </li>

                        <li class="nav-item">



                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="dropdown-item text-danger">
                                    <i class="bx bx-power-off"></i>
                                    <span>{{ __('Logout') }}</span></button>
                            </form>
                        </li>
                        <li class="nav-item">
                            {{-- <a class="nav-link" href="{{ route('Chat') }}">Go Chat</a> --}}
                        </li>

                    @endguest
                </ul>

            </div>
        </div>
    </nav>
    <div class="container mt-5">

        @yield('content')

    </div>
    {{-- <script src="{{asset('js/app.js')}}">

</script> --}}
    @yield('script')
</body>

</html>
