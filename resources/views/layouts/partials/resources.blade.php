<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!--favicon-->
    <link rel="icon" href="{{ asset('assets/auth/images/favicon-32x32.png') }}" type="image/png" />

    {{-- <link rel="icon" href="{{asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" type="image/png" /> --}}



    <!--Data Tables -->
    <link href="{{ asset('assets/auth/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('assets/auth/plugins/datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css">

    <!-- Vector CSS -->
    <link href="{{ asset('assets/auth/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <!--plugins-->
    <link href="{{ asset('assets/auth/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/auth/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/auth/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
    <!-- loader-->
    <link href="{{ asset('assets/auth/css/pace.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/auth/js/pace.min.js') }}"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/bootstrap.min.css') }}" />

    {{-- <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto&display=swap" /> --}}

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@800;1000&display=swap" rel="stylesheet">

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;500;800;1000&display=swap');
    </style>

<!--Data Tables js-->
<script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/icons.css') }}" />
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset('assets/auth/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/auth/css/dark-sidebar.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/auth/css/dark-theme.css') }}" />
</head>
