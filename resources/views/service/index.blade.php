@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'service';
    @endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Services\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        @if (Auth::user()->role === 'admin')
                            <a href="{{ route('services.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Add new service') }}</a>
                        @endif
                        @if (Auth::user()->role === 'provider')
                            <a href="{{ route('provider.services.create') }}"
                                class="btn btn-primary btn-sm float-right">{{ __('Add new service') }}</a>
                        @endif

                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Services List') }}</h5>
                        </div>

                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Provider') }}</th>
                                        <th>{{ __('Category') }}</th>
                                        <th>{{ __('Sub Category') }}</th>
                                        <th>{{ __('Service Description') }}</th>
                                        <th>{{ __('Service Type') }}</th>
                                        <th>{{ __('Price') }}</th>
                                        <th>{{ __('Discount') }}</th>
                                        <th>{{ __('Featured') }}</th>
                                        <th>{{ __('Rating') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($services as $service)
                                        <tr>
                                            <th scope="row">{{ $service->id }}</th>
                                            <td>{{ $service->name }}</td>
                                            <td>{{ $service->provider->user->name }}</td>
                                            <td>{{ $service->category->name }}</td>
                                            <td>{{ $service->subcategory == null ? '' : $service->subcategory->name }}
                                            </td>
                                            <td><p>{{ $service->description }}</p></td>
                                            <td>{{ $service->type }}</td>

                                            @php
                                                $price = $service->min_price;
                                                if ($service->type != 'Fixed') {
                                                    $price = $service->min_price . '-' . $service->max_price;
                                                }
                                            @endphp

                                            <td>{{ $price . '$' }}</td>
                                            <td>{{ $service->discount . '%' }}</td>
                                            <td>{{ $service->featured === 1 ? 'Yes' : 'No'}}</td>

                                            <td>{{ $services_ratings['service' . $service->id] }}</td>
                                            <td>{{ $service->status }}</td>
                                            <td>
                                                @if (Auth::user()->role === 'admin')
                                                    <a href="{{ route('services.show', $service) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                    &nbsp; &nbsp;
                                                    <a href="{{ route('services.edit', $service) }}">
                                                        <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                    </a>

                                                    &nbsp; &nbsp;

                                                    @if ($service->status === 'On')
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['services.disable', $service],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="far fa-2x fa-trash-alt icon-size"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-danger',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @else
                                                        {!! Form::open([
                                                            'method' => 'POST',
                                                            'route' => ['services.enable', $service],
                                                            'style' => 'display:inline',
                                                        ]) !!}
                                                        {!! Form::button('<i class="lni lni-reload"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'submit-btn text-primary btn-lg',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                                @endif

                                                @if (Auth::user()->role === 'provider')
                                                    <a href="{{ route('provider.services.show', $service) }}">
                                                        <i class="fas fa-eye text-success fa-2x"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Provider') }}</th>
                                        <th>{{ __('Category') }}</th>
                                        <th>{{ __('Sub Category') }}</th>
                                        <th>{{ __('Service Description') }}</th>
                                        <th>{{ __('Service Type') }}</th>
                                        <th>{{ __('Price') }}</th>
                                        <th>{{ __('Discount') }}</th>
                                        <th>{{ __('Featured') }}</th>
                                        <th>{{ __('Rate') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection


@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            $('#example').DataTable();
        });
    </script>
@endsection
