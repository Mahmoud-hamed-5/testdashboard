@extends('layouts.dashboard')

@section('content')
    @php
        $statusArray = ['On', 'Off'];
        $typeArray = ['Fixed', 'Variant'];
    @endphp

    @php
        $page = 'service';
    @endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h2>{{ __('Add new service') }}</h2>

                            <hr />
                            @php
                                if (Auth::user()->role === 'admin') {
                                    $stoe_route = 'services.store';
                                    $cancel_route = 'services.index';
                                } else {
                                    $stoe_route = 'provider.services.store';
                                    $cancel_route = 'provider.services.index';
                                }

                            @endphp


                            <form method="POST" action="{{ route($stoe_route) }}" enctype="multipart/form-data">
                                @csrf

                                <div class="mb-3">
                                    <label for="name" class="form-label">{{ __('Service Name') }}</label>
                                    <input value="{{ old('name') }}" type="text" class="form-control" name="name"
                                        placeholder="Name" required>

                                    @if ($errors->has('name'))
                                        <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                @if (Auth::user()->role === 'admin')
                                    <div class="mb-3">
                                        <label for="provider" class="form-label">{{ __('Provider') }}</label>
                                        <select class="form-control" id="provider" name="provider" required>
                                            <option value="0">{{ __('Select provider') }}</option>
                                            @foreach ($providers as $provider)
                                                <option value="{{ $provider->id }}">{{ $provider->user->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('provider'))
                                            <span class="text-danger text-left">{{ $errors->first('provider') }}</span>
                                        @endif
                                    </div>
                                @endif

                                <div class="mb-3">
                                    <label for="category" class="form-label">{{ __('Category') }}</label>
                                    <select class="form-control" id="category" onchange="get_subCategories(this)"
                                        name="category" required>
                                        <option value="">{{ __('Select category') }}</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="text-danger text-left">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>


                                <div class="mb-3">
                                    <label for="subcategory" class="form-label">{{ __('Sub Category') }}</label>
                                    <select class="form-control" id="subcategory" name="subcategory">
                                        <option value="">{{ __('Select subcategory') }}</option>

                                    </select>
                                    @if ($errors->has('subcategory'))
                                        <span class="text-danger text-left">{{ $errors->first('subcategory') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="description" class="form-label">{{ __('Description') }}</label>
                                    <textarea type="text" class="form-control" name="description" rows="3" placeholder="Provider Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger text-left">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="description" class="form-label">{{ __('Service Location(s)') }}</label>
                                    <select class="multiple-select" required
                                        name="cities[]" id="cities"
                                        data-placeholder="Choose anything"
                                        multiple="multiple">
                                        @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('cities'))
                                        <span class="text-danger text-left">{{ $errors->first('cities') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="type" class="form-label">{{ __('Service Type') }}</label>
                                    <select class="form-control" name="type" onchange="EnableDisable(this)"
                                        id="type" required>
                                        @foreach ($typeArray as $item)
                                            <option value="{{ $item }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="text-danger text-left">{{ $errors->first('type') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="min_price" id="min_price_label"
                                        class="form-label">{{ __('Price') }}</label>
                                    <input value="0" type="number" class="form-control" name="min_price"
                                        id="min_price" required>
                                    @if ($errors->has('min_price'))
                                        <span class="text-danger text-left">{{ $errors->first('min_price') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="max_price" class="form-label">{{ __('Max Price') }}</label>
                                    <input value="0" type="number" class="form-control" disabled="true" id="max_price"
                                        name="max_price" required>
                                    @if ($errors->has('max_price'))
                                        <span class="text-danger text-left">{{ $errors->first('max_price') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="discount" class="form-label">{{ __('Discount') }}</label>
                                    <input value="0" type="number" class="form-control" name="discount" required>
                                    @if ($errors->has('discount'))
                                        <span class="text-danger text-left">{{ $errors->first('discount') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label class="form-check-label" for="flexSwitchCheckChecked">{{__('Featured')}}</label>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="featured" name="featured" checked>
                                    </div>

                                </div>


                                @if (Auth::user()->role === 'admin')
                                    <div class="mb-3">
                                        <label for="status" class="form-label">{{ __('Status') }}</label>
                                        <select class="form-control" name="status" id="status" required>
                                            @foreach ($statusArray as $item)
                                                <option value="{{ $item }}">
                                                    {{ $item }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                        @endif
                                    </div>
                                @endif

                                <div class="mb-3">
                                    <label for="image" class="form-label">{{ __('Image') }}</label>
                                    <input type="file" class="form-control" name="image" />
                                    @if ($errors->has('image'))
                                        <span class="text-danger text-left">{{ $errors->first('image') }}</span>
                                    @endif
                                    <input type="hidden" name="hidden_image" />
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route($cancel_route) }}"
                                    class="btn btn-default">{{ __('Cancel') }}</button>
                            </form>


                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@section('script')
<script src="{{asset('assets/auth/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $('.multiple-select').select2({
        theme: 'bootstrap4',
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
</script>
@endsection


<script>

    function get_subCategories(categorySelect) {
        var xhr = new XMLHttpRequest();

        var url = '/subcategories-list/' + categorySelect.value;

        xhr.open("GET", url, false);

        xhr.onload = function() {
            response = this.responseText;

            let emptyOption = new Option('Select subcategory', '');
            const select = document.getElementById("subcategory");
            while (select.options.length > 0) {
                select.remove(0);
            }
            select.add(emptyOption, undefined);

            var subcategories = JSON.parse(response);

            for (var i = 0; i < subcategories.length; i++) {
                var subcategory = subcategories[i];
                option = new Option(subcategory.name, subcategory.id);
                select.add(option, undefined);
            }
        }

        xhr.send();


    }


    function EnableDisable(serviceType) {

        var maxPrice = document.getElementById("max_price");

        var minPriceLabel = document.getElementById("min_price_label");

        var str = serviceType.options[serviceType.selectedIndex].text;

        if (str == "Fixed") {
            minPriceLabel.innerHTML = "{{ __('Price') }}";
            maxPrice.disabled = true;
        } else {
            minPriceLabel.innerHTML = "{{ __('Min Price') }}";
            maxPrice.disabled = false;
        }
    };
</script>
