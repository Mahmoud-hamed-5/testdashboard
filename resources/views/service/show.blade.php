@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'service';
    @endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>


                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">
                        <h3>{{ __('Service Information') }} : {{ $service->name }}</h3>
                    </div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class='bx bx-home-alt'></i></a>
                                </li>
                                {{-- <li class="breadcrumb-item active" aria-current="page">User Profile</li> --}}
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
                        <a href="{{ route('services.edit', $service) }}">
                            <i class="text-primary" data-feather="edit"></i>
                        </a>


                    </div>

                </div>
                <!--end breadcrumb-->
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-6">
                        <div class="card mb-3">
                            <img src="{{ asset($service->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">

                                <table class="table table-sm table-borderless mt-md-0 mt-3">
                                    <tbody>
                                        <tr>
                                            <th>
                                                <h2 class="card-title">{{ $service->name }}</h2>
                                            </th>
                                            <td> <span class="badge badge-success">
                                                    {{ __('Rating') }} : {{ $service->rating() }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <hr>

                                <h4 class="card-title"> {{ __('Service Description') }} : </h4>
                                <p>{{ $service->description }}</p>

                                <hr>

                                <h4 class="card-title"> {{ __('Category') }} : {{ $service->category->name }}
                                    {{ is_null($service->subcategory) ? '' : ' / ' . $service->subcategory->name }}</h4>

                                <hr>

                                @php
                                    if ($service->discount === 0) {
                                        $discount = '';
                                    } else {
                                        $discount = 'with ( ' . $service->discount . ' % ) Discount';
                                    }
                                @endphp

                                <h4 class="card-title"> {{ __('Price') }} : {{ $service->type }}
                                    {{ $service->type === 'Fixed'
                                        ? ' : ' . $service->min_price . '$'
                                        : ' : ' . $service->min_price . ' - ' . $service->max_price . ' $ ' . ' / ' . $discount }}
                                </h4>
                                <hr>

                                <h4 class="card-title"> {{ __('Provider') }} : {{ $service->provider->user->name }}

                                </h4>
                                <ul class="list-group list-group-flush">

                                    <li class="list-group-item"></li>

                                </ul>

                                <hr>
                                <h4 class="card-title"> {{ __('Service Locations') }} : </h4>
                                <ul class="list-group list-group-flush">
                                    @foreach ($service->cities as $city)
                                        <li class="list-group-item">{{$city->city}}</li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>


                    </div>


                </div>

            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection




