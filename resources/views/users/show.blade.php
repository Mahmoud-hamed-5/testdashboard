@extends('layouts.dashboard')
 @section('content')
 @php
 $page = 'user';
 @endphp
 <div class="page-wrapper">
    <!--page-content-wrapper-->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!--breadcrumb-->


            <div class="card">
                <div class="card-body">
                    <div>
                        <h5>{{ __('User Information') }}</h5>

                        <hr />

                        <div class="container mt-4">
                            <div>
                                {{ __('Name') }} : {{ $user->name }}
                            </div>
                            <div>
                                {{ __('Email') }} : {{ $user->email }}
                            </div>

                        </div>


                    </div>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-info">{{__('Edit')}}</a>
                <a href="{{ route('users') }}" class="btn btn-default">{{__('Back')}}</a>
            </div>

        </div>
    </div>
    <!--end page-content-wrapper-->
</div>
<!--end page-wrapper-->







@endsection
