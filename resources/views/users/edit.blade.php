@extends('layouts.dashboard')

@section('content')

@php
    $genderArray = ['Other' , 'Male' , 'Female'];
    $statusArray = ['On' , 'Off'];
    $providerTypeArray = ['Company', 'Freelance'];
@endphp

@php
$page = 'user';
@endphp

<div class="page-wrapper">
    <!--page-content-wrapper-->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!--breadcrumb-->



            <div class="card">
                <div class="card-body">
                    <div>
                        <h5>{{ __('Edit User') }}</h5>

                        <hr />


                        <form method="post" action="{{ route('users.update', $user) }}">
                            @method('patch')
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">{{__('Name')}}</label>
                                <input value="{{ $user->name }}"
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            {{-- <div class="mb-3">
                                <label for="email" class="form-label">{{__('Email')}}</label>
                                <input value="{{ $user->email }}"
                                    type="email"
                                    class="form-control"
                                    name="email"
                                    placeholder="Email address" required>
                                @if ($errors->has('email'))
                                    <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                @endif
                            </div> --}}

                            {{-- <div class="mb-3">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                                <div class="input-group" id="show_hide_password">
                                    <input type="password" class="form-control border-end-0" id="password"
                                        name="password" placeholder="Enter Password">
                                    <a href="javascript:;" class="input-group-text bg-transparent"><i
                                            class="bx bx-hide"></i></a>
                                </div>
                            </div> --}}

                            <div class="mb-3">
                                <label for="gender" class="form-label">{{__('Gender')}}</label>
                                <select class="form-control" name="gender" id="gender">
                                    @foreach ($genderArray as $item)
                                    <option value="{{$item}}" {{($user->gender == $item) ? 'selected':''}}>
                                        {{$item}}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('gender'))
                                    <span class="text-danger text-left">{{ $errors->first('gender') }}</span>
                                @endif
                            </div>

                            {{-- <div class="mb-3">
                                <label for="phone" class="form-label">{{__('Phone Number')}}</label>
                                <input value="{{ $user->phone }}"
                                    type="text"
                                    class="form-control"
                                    name="phone"
                                    placeholder="Phone Number">
                                @if ($errors->has('phone'))
                                    <span class="text-danger text-left">{{ $errors->first('phone') }}</span>
                                @endif
                            </div> --}}

                            {{-- <div class="mb-3">
                                <label for="address" class="form-label">{{__('Address')}}</label>
                                <input value="{{ $user->address }}"
                                    type="text"
                                    class="form-control"
                                    name="address"
                                    placeholder="Address">
                                @if ($errors->has('address'))
                                    <span class="text-danger text-left">{{ $errors->first('address') }}</span>
                                @endif
                            </div> --}}

                            <div class="mb-3">
                                <label for="status" class="form-label">{{__('Status')}}</label>
                                <select class="form-control" name="status" id="status">
                                    @foreach ($statusArray as $item)
                                    <option value="{{$item}}" {{($user->status == $item) ? 'selected':''}}>
                                        {{$item}}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('status'))
                                    <span class="text-danger text-left">{{ $errors->first('status') }}</span>
                                @endif
                            </div>

                            @if ($user->role === 'user')

                            <div class="mb-3">
                                <label for="points" class="form-label">{{ __('Points') }}</label>
                                <input value="{{$user->points}}" type="number" class="form-control" name="points"
                                    id="points" required>
                                @if ($errors->has('points'))
                                    <span class="text-danger text-left">{{ $errors->first('points') }}</span>
                                @endif
                            </div>


                            @endif


                            <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                            <a href="{{ route('users.index') }}" class="btn btn-default">{{__('Cancel')}}</button>
                        </form>


                    </div>
                </div>
            </div>



        </div>
    </div>
    <!--end page-content-wrapper-->
</div>
<!--end page-wrapper-->

@endsection

<script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
<!--Password show & hide js -->
<script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("bx-hide");
                $('#show_hide_password i').removeClass("bx-show");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("bx-hide");
                $('#show_hide_password i').addClass("bx-show");
            }
        });
    });
</script>

<script type="text/javascript">
    function EnableDisable() {

        var typeSelect = document.getElementById("type");

        var role = document.getElementById("role");

        var str = role.options[role.selectedIndex].text;

        if (str == "provider") {

            typeSelect.disabled = false;
       } else {

           typeSelect.disabled = true;
       }
    };


</script>

