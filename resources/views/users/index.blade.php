@extends('layouts.dashboard')

@section('content')

@php
$page = 'user';
@endphp

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Customers\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                    <div class="lead">
{{--
                        <a href="{{ route('users.create') }}"
                            class="btn btn-primary btn-sm float-right">{{ __('Add new user') }}</a> --}}
                    </div>
                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Customers table') }}</h5>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Gender') }}</th>
                                        <th>{{ __('Phone Number') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th>{{ __('Points') }}</th>
                                        <th>{{ __('Role') }}</th>
                                        <th>{{ __('Status') }}</th>
                                        <th>{{ __('Actions') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <th scope="row">{{ $user->id }}</th>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->gender }}</td>
                                            <td>{{ $user->register_phone }}</td>
                                            <td>{{ $user->address }}</td>
                                            <td>{{ $user->points }}</td>

                                            <td><span class="badge bg-primary">{{ $user->role }}</span></td>
                                            {{-- <td>{{ $send_data['types' . $user->id] }}</td> --}}
                                            <td>{{ $user->status }}</td>


                                            <td>
                                                <a href="{{ route('profiles.show', $user) }}">
                                                    <i class="fas fa-eye text-success fa-2x"></i>
                                                </a>
                                                &nbsp; &nbsp;
                                                <a href="{{ route('users.edit', $user) }}">
                                                    <i class="fa-solid fa-2x fa-pen-to-square"></i>
                                                </a>

                                                &nbsp; &nbsp;
                                                @if ($user->status === 'On')
                                                    {!! Form::open([
                                                        'method' => 'POST',
                                                        'route' => ['users.disable', $user],
                                                        'style' => 'display:inline',
                                                    ]) !!}
                                                    {!! Form::button('<i class="far fa-2x fa-trash-alt icon-size"></i>', [
                                                        'type' => 'submit',
                                                        'class' => 'submit-btn text-danger',
                                                    ]) !!}
                                                    {!! Form::close() !!}
                                                @else
                                                    {!! Form::open([
                                                        'method' => 'POST',
                                                        'route' => ['users.enable', $user],
                                                        'style' => 'display:inline',
                                                    ]) !!}
                                                    {!! Form::button('<i class="lni lni-reload"></i>', [
                                                        'type' => 'submit',
                                                        'class' => 'submit-btn text-primary btn-lg',
                                                    ]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Gender') }}</th>
                                    <th>{{ __('Phone Number') }}</th>
                                    <th>{{ __('Address') }}</th>
                                    <th>{{ __('Points') }}</th>
                                    <th>{{ __('Role') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Actions') }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection



@section('script')
 <!--Data Tables js-->
 <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

 <script>
    $(document).ready(function() {
        $("#toggle-btn").click();
        $('#example').DataTable();
    });
</script>

@endsection
