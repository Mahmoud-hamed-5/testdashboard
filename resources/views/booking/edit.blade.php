@extends('layouts.dashboard')

@section('content')
    @php
        $genderArray = ['Male', 'Female'];
        $statusArray = ['On', 'Off'];
        $providerTypeArray = ['Company', 'Freelance'];
    @endphp

    @php
        $page = 'booking';
    @endphp

    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content" dir={{ session('dir') }}>
                <!--breadcrumb-->

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                </div>
                <!--end breadcrumb-->

                <div class="row">

                    <div class="col-12 col-lg-6 col-xl-6">

                        <div class="card-body">
                            <h3 class="card-title">{{ __('Service Name') }} : {{ $booking->service->name }}</h3>
                        </div>
                        {{-- Customer Informations --}}
                        <h3 class="card-title">{{ __('Customer Information') }}</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h5>{{ __('Customer Name') }} : <b>{{ $booking->user->name }}</b></h5>
                            </li>
                            <li class="list-group-item">
                                <h5>{{ __('Customer Address') }} : <b>{{ $booking->address }}</b></h5>
                            </li>
                            <li class="list-group-item">
                                <h5>{{ __('Customer Phone') }} : <b>{{ $booking->phone }}</b></h5>
                            </li>
                        </ul>
                        <hr>

                        {{-- Problem Description --}}
                        <h3 class="card-title">{{ __('Issue Description') }}</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <p style="font-size: 24px">{{ $booking->description }}</p>
                            </li>
                            <div class="card-group">
                                @foreach ($booking->booking_images as $image)
                                    <div class="card">
                                        <img src="{{ asset($image->url) }}" class="d-block w-200" alt="...">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </ul>
                        <hr>

                        {{-- Booking Details --}}
                        <h3 class="card-title">{{ __('Booking Details') }}</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h5> {{ __('Booking State') }} : <b>{{ __($booking->state) }} </b></h5>
                            </li>

                            <li class="list-group-item">
                                <h5> {{ __('Date') }} : <b>{{ $booking->date }} </b></h5>
                            </li>

                            @if ($booking->state === 'Pending')
                                <li class="list-group-item">
                                    <h5> {{ __('Time') }} :
                                        <b>{{ $booking->min_time }} {{ ' --- ' }}
                                            {{ $booking->max_time }} </b>
                                    </h5>
                                </li>
                            @endif

                            @if ($booking->service->type === 'Variant' && $booking->state === 'Pending')
                                <li class="list-group-item">
                                    <h5> {{ __('Price') }} :
                                        <b> {{ $booking->service->min_price }} {{ '  --  ' }}
                                            {{ $booking->service->max_price }} {{ __('S.P') }}</b>
                                    </h5>
                                </li>
                            @endif

                            @if ($booking->service->type === 'Fixed')
                                <li class="list-group-item">
                                    <h5> {{ __('Price') }} :
                                        <b>{{ $booking->service->min_price }} {{ __('S.P') }} </b>
                                    </h5>
                                </li>
                            @endif

                            @if ($booking->state === 'Ongoing' || $booking->state === 'Completed')
                                <li class="list-group-item">
                                    <h5> {{ __('Time') }} :
                                        <b>{{ $booking->time }}</b>
                                    </h5>
                                </li>
                                <li class="list-group-item">
                                    <h5> {{ __('Amount') }} :
                                        {{ $booking->amount }} {{ __('S.P') }}</b></h5>
                                </li>
                            @endif

                            <li class="list-group-item">
                                <h5> {{ __('Discount') }} : <b>{{ $booking->service->discount }} {{ __('%') }}</b>
                                </h5>
                            </li>

                        </ul>

                        <hr>
                        @if (!is_null($booking->coupon))
                            <h3 class="card-title">{{ __('Coupon Information') }}</h3>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h5>{{ __('Coupon Name') }} : <b>{{ $booking->coupon->name }}</b></h5>
                                </li>
                                <li class="list-group-item">
                                    <h5>{{ __('Coupon Value') }} : <b>{{ $booking->coupon->value }}</b></h5>
                                    {{ __('S.P') }}
                                </li>

                            </ul>
                            <hr>
                        @endif

                        @if ($booking->state === 'Completed' || $booking->state === 'Ongoing')
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h5>{{ __('Total Amount') }} : <b>{{ $booking->amount }} {{ __('S.P') }}</b></h5>
                                </li>


                            </ul>
                            <hr>
                        @endif

                        @if ($booking->state === 'Cancelled')
                            <h3 class="card-title">{{ __('Reason of Rejection') }}</h3>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <p style="font-size: 24px">{{ $booking->reason }}</p>
                                </li>

                            </ul>
                            <hr>
                        @endif

                    </div>

                    {{-- Customer Location on Google maps --}}
                    <div class="col-12 col-lg-6 col-xl-6">
                        <div class="card">
                            <div class="card-header">{{ __('Customer Location') }}</div>
                            <div class="card-body">
                                <div id="marker-map" class="gmaps"></div>
                            </div>
                        </div>
                    </div>

                </div>

                @if (Auth::user()->role === 'provider')
                    <div class="row">
                        @if ($booking->state === 'Pending')


                            {{-- Approve to Booking --}}
                            <div class="col-8 col-lg-6 col-xl-6">
                                <div class="card radius-15">
                                    <div class="card-body">
                                        <div class="card-title d-flex align-items-center">
                                            <h5 class="mb-0 text-primary">{{ __('Approve') }}</h5>
                                        </div>
                                        <hr>

                                        <form class="row g-3" method="POST"
                                            action="{{ route(Auth::user()->role . '.bookings.approve', $booking) }}">
                                            @csrf
                                            {{-- Time Picker --}}
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <span class="input-group-text">{{ __('Service Time') }}</span>
                                                    <input class="result form-control" type="text" id="time"
                                                        name="time" placeholder="Pick Time..." required>
                                                </div>
                                                @if ($errors->has('time'))
                                                    <span class="text-danger text-left">{{ $errors->first('time') }}</span>
                                                @endif
                                            </div>

                                            {{-- Price --}}
                                            <div class="col-md-8">
                                                @if ($booking->service->type === 'Fixed')
                                                    <div class="input-group">
                                                        <span class="input-group-text">{{ __('Total Amount') }}</span>
                                                        <input class="result form-control" name="price" id="price"
                                                            value="{{ $booking->service->min_price }}" class="col-8"
                                                            type="number" readonly>
                                                    </div>
                                                @else
                                                    <div class="input-group">
                                                        <span class="input-group-text">{{ __('Total Amount') }}</span>
                                                        <input class="result form-control" type="number" name="price"
                                                            id="price" required>
                                                    </div>
                                                @endif
                                                @if ($errors->has('price'))
                                                    <span
                                                        class="text-danger text-left">{{ $errors->first('price') }}</span>
                                                @endif
                                            </div>

                                            {{-- Assign Handyman --}}
                                            @if ($booking->service->provider->type === 'Company')
                                                <div class="col-md-8">

                                                    <div class="input-group">
                                                        <span class="input-group-text">{{ __('Assign Handyman') }}</span>
                                                        <select class="result form-control" name="handyman_id"
                                                            id="handyman_id" required>

                                                            <option value="">{{ __('Select handyman') }}</option>
                                                            @foreach ($handymen as $handyman)
                                                                <option value="{{ $handyman->id }}">
                                                                    {{ $handyman->user->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div>
                                                        @if ($errors->has('handyman_id'))
                                                            <span
                                                                class="text-danger text-left">{{ $errors->first('handyman_id') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="col-12">
                                                <button type="submit"
                                                    class="btn btn-primary px-5">{{ __('Approve') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            {{-- Reject the Booking --}}
                            <div class="col-12 col-lg-6 col-xl-6">
                                <div class="card radius-15">
                                    <div class="card-body">
                                        <div class="card-title d-flex">
                                            <h5 class="mb-0 text-green">{{ __('Reject') }}</h5>
                                        </div>
                                        <hr>
                                        <form class="row g-3" method="POST"
                                            action="{{ route(Auth::user()->role . '.bookings.reject', $booking) }}">
                                            @csrf

                                            <div class="mb-3">
                                                <label for="reason" class="form-label">{{ __('Reason') }}</label>
                                                <textarea type="text" class="form-control" name="reason" rows="3"
                                                    placeholder="{{ __('Explain reason of rejection') }}"></textarea>
                                                @if ($errors->has('reason'))
                                                    <span
                                                        class="text-danger text-left">{{ $errors->first('reason') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-8">
                                                <button type="submit"
                                                    class="btn btn-danger px-5">{{ __('Reject') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif

                        {{-- Finish the Booking --}}
                        @if ($booking->state === 'Ongoing')
                            <div class="col-8 col-lg-6 col-xl-6">
                                <div class="card radius-15">
                                    <div class="card-body">
                                        <div class="card-title d-flex align-items-center">
                                            <h5 class="mb-0 text-primary">{{ __('Finish') }}</h5>
                                        </div>
                                        <hr>

                                        <form class="row g-3" method="POST"
                                            action="{{ route(Auth::user()->role . '.bookings.finish', $booking) }}">
                                            @csrf

                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <span
                                                        class="input-group-text">{{ __('All Work is done?  Finish it!') }}</span>

                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit"
                                                    class="btn btn-primary px-5">{{ __('Finish') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif


                    </div>
                @endif

            </div>
            <!-- end page-content -->
        </div>
        <!-- end page-content-wrapper -->
    </div>
    <!-- end page-wrapper -->
@endsection


@section('script')
    <!-- google maps api -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKXKdHQdtqgPVl2HI2RnUa_1bjCxRCQo4&callback=initMap" async
        defer></script>
    <script src="{{ asset('assets/auth/plugins/gmaps/map-custom-script.js') }}"></script>
    <script>
        window.lat = '{{ (float) $booking->lat }}';
        window.lng = '{{ (float) $booking->lng }}';
    </script>


    <script src="{{ asset('assets/auth/plugins/datetimepicker/js/legacy.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/datetimepicker/js/picker.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/datetimepicker/js/picker.time.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/datetimepicker/js/picker.date.js') }}"></script>
    <script src="{{ asset('assets/auth/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
    <script
        src="{{ asset('assets/auth/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}">
    </script>
    <script>
        $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: true
            }),
            $('.timepicker').pickatime()
    </script>
    <script>
        $(function() {
            $('#date-time').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD HH:mm'
            });
            $('#date').bootstrapMaterialDatePicker({
                time: false
            });
            $('#time').bootstrapMaterialDatePicker({
                date: false,
                format: 'HH:mm'
            });
        });
    </script>
@endsection
