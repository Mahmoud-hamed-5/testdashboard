@extends('layouts.dashboard')
@section('content')
    @php
        $page = 'booking';
    @endphp
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">

                <h3>{{ __('Bookings\' Management') }}</h3>

                <div class="mt-2">
                    @include('layouts.partials.messages')
                </div>

                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-md-flex align-items-center mb-3">

                </div>
                <!--end breadcrumb-->


                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>{{ __('Bookings List') }}</h5>
                        </div>

                        <div class="card-title">
                            <button type="button" value="All" class="btn btn-dark"
                                onclick="filter_bookings(this.value)">
                                {{ __('All') }} <span id="all_count_bedge" class="badge bg-light text-dark"></span>
                            </button>
                            <button type="button" value="Pending" class="btn btn-primary"
                                onclick="filter_bookings(this.value)">
                                {{ __('Pending') }} <span id="pending_count_bedge" class="badge bg-light text-dark"></span>
                            </button>

                            <button type="button" value="Ongoing" class="btn btn-info"
                                onclick="filter_bookings(this.value)">
                                {{ __('Ongoing') }} <span id="ongoing_count_bedge" class="badge bg-light text-dark"></span>
                            </button>
                            <button type="button" value="Completed" class="btn btn-success"
                                onclick="filter_bookings(this.value)">
                                {{ __('Completed') }} <span id="completed_count_bedge"
                                    class="badge bg-light text-dark"></span>
                            </button>
                            <button type="button" value="Cancelled" class="btn btn-danger"
                                onclick="filter_bookings(this.value)">
                                {{ __('Cancelled') }} <span id="cancelled_count_bedge"
                                    class="badge bg-light text-dark"></span>
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>{{ __('Actions') }}</th>
                                        <th>{{ __('Service Name') }}</th>
                                        <th>{{ __('Issue Description') }}</th>
                                        <th>{{ __('Date') }}</th>
                                        <th>{{ __('Time') }}</th>
                                        <th>{{ __('Customer Name') }}</th>
                                        <th>{{ __('Customer Phone') }}</th>
                                        <th>{{ __('Customer Address') }}</th>
                                        <th>{{ __('Provider') }}</th>
                                        <th>{{ __('Handyman') }}</th>
                                        <th>{{ __('Booking State') }}</th>
                                        <th>{{ __('Coupon') }}</th>
                                        <th>{{ __('Amount') }}</th>
                                        {{-- <th>{{ __('Payment Status') }}</th> --}}
                                        <th>{{ __('Reason of Rejection') }}</th>
                                    </tr>
                                </thead>

                                <tbody id="table-body">

                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>{{ __('Actions') }}</th>
                                        <th>{{ __('Service Name') }}</th>
                                        <th>{{ __('Issue Description') }}</th>
                                        <th>{{ __('Date') }}</th>
                                        <th>{{ __('Time') }}</th>
                                        <th>{{ __('Customer Name') }}</th>
                                        <th>{{ __('Customer Phone') }}</th>
                                        <th>{{ __('Customer Address') }}</th>
                                        <th>{{ __('Provider') }}</th>
                                        <th>{{ __('Handyman') }}</th>
                                        <th>{{ __('Booking State') }}</th>
                                        <th>{{ __('Coupon') }}</th>
                                        <th>{{ __('Amount') }}</th>
                                        {{-- <th>{{ __('Payment Status') }}</th> --}}
                                        <th>{{ __('Reason of Rejection') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection



@section('script')
    <!--Data Tables js-->
    <script src="{{ asset('assets/auth/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#toggle-btn").click();
            filter_bookings('All');
            $('#example').DataTable();
            var table = $('#example2').DataTable({
                lengthChange: false,
                buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
            });
            table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
        });
    </script>

    <script>
        function filter_bookings(state) {
            $.ajax({
                type: "GET",
                url: "/bookings-filter",
                data: "state=" + state,
                success: function(response) {
                    var tablebody = document.getElementById('table-body');
                    document.getElementById('all_count_bedge').innerHTML = response.all_count;
                    document.getElementById('pending_count_bedge').innerHTML = response.pending_count;
                    document.getElementById('ongoing_count_bedge').innerHTML = response.ongoing_count;
                    document.getElementById('completed_count_bedge').innerHTML = response.completed_count;
                    document.getElementById('cancelled_count_bedge').innerHTML = response.cancelled_count;

                    var html = '';
                    if (response.bookings) {
                        for (let index = 0; index < response.routes.length; index++) {
                            var bookings = response.bookings;
                            var routes = response.routes;

                            route = routes[index];

                            if (bookings[index].state === 'Pending') {
                                var time = bookings[index].min_time + ' -- ' + bookings[index].max_time;

                            } else {
                                time = bookings[index].time;
                            }

                            html += `
                            <tr>
                                <td>
                                    <a href="` + route + `">
                                        <i class="fas fa-eye text-success fa-2x"></i>
                                    </a>
                                </td>
                                <td>` + bookings[index].service_name + `</td>
                                <td><p>` + bookings[index].description + `</p></td>
                                <td>` + bookings[index].date + `</td>

                                <td>` + time + `</td>

                                <td>` + bookings[index].customer_name + `</td>
                                <td>` + bookings[index].customer_phone + `</td>
                                <td>` + bookings[index].customer_address + `</td>
                                <td>` + bookings[index].provider_name + `</td>
                                <td>` + bookings[index].handyman_name + `</td>
                                <td>` + bookings[index].booking_state + `</td>
                                <td>` + bookings[index].coupon + `</td>
                                <td>` + bookings[index].amount + `</td>
                                <td>` + bookings[index].reason_of_rejection + `</td>
                            </tr>
                        `;
                        }
                    }
                    tablebody.innerHTML = html;
                }
            });
        }
    </script>
@endsection
