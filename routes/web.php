<?php

use App\Http\Controllers\BookingController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FCMController;
use App\Http\Controllers\HandymanController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.admin-lock-screen');
});


Auth::routes();
Route::get('/adminlockscreen', function () {
    return view('auth.admin-lock-screen');
})->name('admin.lock');



Route::group(['middleware' => ['role:admin|provider|handyman']], function () {

    Route::get('/subcategories-list/{category_id}', [ServiceController::class, 'subcategories_list']);

    Route::get('admin/index', [DashboardController::class, 'index'])->name('admin.index');

    Route::controller(ProfileController::class)->group(function(){
        Route::get('profiles', 'index')->name('profiles.index');
        Route::post('profiles/{user}', 'update')->name('profiles.update');
    });

    Route::controller(NotificationController::class)->group(function(){
        Route::get('sort-messages', 'sort_messages')->name('sort.messages');
        Route::get('sort-notifications', 'sort_notifications')->name('sort.notifications');
    });

    Route::controller(ChatController::class)->group(function(){
        Route::get('/chats/index', 'index')->name('chats.index');

        Route::get('/chat-data', 'chat_data')->name('chat.data');
        Route::get('sort-users', 'sort_users')->name('sort.users');
        Route::post('/send-message', 'send_message')->name('message.send');
        Route::post('/update_message_status', 'update_message_status')->name('message.status.update');
    });

    Route::post('/fcm-token', [FCMController::class, 'updateToken'])->name('fcmToken');

    Route::get('/bookings-filter', [BookingController::class, 'filter'])->name('bookings.filter');
});


Route::group(['prefix' => 'provider'], function () {

    Route::group(['middleware' => ['role:provider']], function () {

        Route::get('handymen', [HandymanController::class, 'index'])->name('provider.handymen.index');
        Route::get('handymen/create', [HandymanController::class, 'create'])->name('provider.handymen.create');
        Route::post('handymen/store', [HandymanController::class, 'store'])->name('provider.handymen.store');


        Route::get('bookings', [BookingController::class, 'index'])->name('provider.bookings.index');
        Route::get('bookings/edit/{booking}', [BookingController::class, 'edit'])->name('provider.bookings.edit');
        Route::post('bookings/assign/{booking}', [BookingController::class, 'assign_handyman'])->name('provider.bookings.assign');
        Route::post('bookings/approve/{booking}', [BookingController::class, 'provider_approve'])->name('provider.bookings.approve');
        Route::post('bookings/reject/{booking}', [BookingController::class, 'reject'])->name('provider.bookings.reject');
        Route::post('bookings/finish/{booking}', [BookingController::class, 'finish'])->name('provider.bookings.finish');


        Route::get('services', [ServiceController::class, 'index'])->name('provider.services.index');
        Route::get('services/show/{service}', [ServiceController::class, 'show'])->name('provider.services.show');
        Route::get('services/create', [ServiceController::class, 'create'])->name('provider.services.create');
        Route::post('services/store', [ServiceController::class, 'store'])->name('provider.services.store');

        Route::get('categories', [CategoryController::class, 'index'])->name('provider.categories.index');
        Route::get('subcategories', [SubcategoryController::class, 'index'])->name('provider.subcategories.index');
    });
});


Route::group(['prefix' => 'handyman'], function () {


    Route::group(['middleware' => ['role:handyman']], function () {

        Route::get('bookings', [BookingController::class, 'index'])->name('handyman.bookings.index');
        Route::get('bookings/edit{booking}', [BookingController::class, 'edit'])->name('handyman.bookings.edit');

        Route::get('categories', [CategoryController::class, 'index'])->name('handyman.categories.index');
        Route::get('subcategories', [SubcategoryController::class, 'index'])->name('handyman.subcategories.index');
    });
});


Route::group(['prefix' => 'admin'], function () {

    Route::group(['middleware' => ['role:admin']], function () {

        Route::get('profiles/{id}', [ProfileController::class, 'show'])->name('profiles.show');

        Route::resource('users', UserController::class);
        Route::post('users/disable/{user}', [UserController::class, 'disable'])->name('users.disable');
        Route::post('users/enable/{user}', [UserController::class, 'enable'])->name('users.enable');


        Route::resource('handymen', HandymanController::class);

        Route::resource('providers', ProviderController::class);

        Route::resource('services', ServiceController::class);
        Route::post('services/disable/{service}', [ServiceController::class, 'disable'])->name('services.disable');
        Route::post('services/enable/{service}', [ServiceController::class, 'enable'])->name('services.enable');

        Route::resource('categories', CategoryController::class);
        Route::post('categories/disable/{category}', [CategoryController::class, 'disable'])->name('categories.disable');
        Route::post('categories/enable/{category}', [CategoryController::class, 'enable'])->name('categories.enable');

        Route::resource('subcategories', SubcategoryController::class);
        Route::post('subcategories/disable/{subcategory}', [SubcategoryController::class, 'disable'])->name('subcategories.disable');
        Route::post('subcategories/enable/{subcategory}', [SubcategoryController::class, 'enable'])->name('subcategories.enable');

        Route::resource('bookings', BookingController::class);

        Route::resource('settings', SettingController::class);
        Route::post('settings/disable/{setting}', [SettingController::class, 'disable'])->name('settings.disable');
        Route::post('settings/enable/{setting}', [SettingController::class, 'enable'])->name('settings.enable');

        Route::resource('coupons', CouponController::class);
        Route::post('coupons/disable/{coupon}', [CouponController::class, 'disable'])->name('coupons.disable');
        Route::post('coupons/enable/{coupon}', [CouponController::class, 'enable'])->name('coupons.enable');

        Route::get('notifications/index', [NotificationController::class, 'index'])->name('notifications.index');
        Route::get('notifications/create', [NotificationController::class, 'create'])->name('notifications.create');
        Route::post('notifications/broadcast', [NotificationController::class, 'broadcast_notification'])->name('notifications.broadcast');

        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionsController::class);
    });
});


Route::get('language/{locale}', function ($locale) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    $dir = $locale === 'en' ? 'ltr' : 'rtl';
    session()->put('dir', $dir);
    return redirect()->back();
});



