<?php

use App\Http\Controllers\API\API_BookingController;
use App\Http\Controllers\API\API_CategoryController;
use App\Http\Controllers\API\API_ChatController;
use App\Http\Controllers\API\API_CouponController;
use App\Http\Controllers\API\API_NotificationController;
use App\Http\Controllers\API\API_ProviderController;
use App\Http\Controllers\API\API_ServiceController;
use App\Http\Controllers\API\API_UserController;
use App\Http\Controllers\API\AuthController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::controller(AuthController::class)->group(function () {

    Route::post('login', 'login')->name('login');
    Route::post('login/phone', 'login_phone')->name('login.phone');

    Route::post('register', 'register')->name('register');
    Route::post('register/phone', 'register_phone')->name('register.phone');

    Route::post('verify/phone', 'verify_OTP')->name('verify.phone');
    // Route::get('get-otp/{phone}', 'send_OTP')->name('send.OTP');
    Route::get('get-otp/{country_code}/{phone}', 'send_OTP')->name('send.OTP');
});

Route::controller(API_CouponController::class)->group(function () {
    Route::get('coupons', 'index')->name('coupons.all');
});

Route::controller(API_CategoryController::class)->group(function () {
    Route::get('categories', 'index')->name('categories.all');
    Route::get('categories/{category_id}', 'show')->name('category.show');
});

Route::controller(API_ServiceController::class)->group(function () {
    Route::get('services', 'index')->name('services.all');
    Route::get('services/{service_id}', 'show')->name('service.show');
    Route::get('featured-services', 'featured')->name('services.featured');
    Route::post('services/search/city', 'search_by_city')->name('services.search.city');
    Route::post('services/featured-city', 'featured_city')->name('featured.city');
});


// Route::get('verify/phone/{otp}', [AuthController::class, 'verify_OTP'] );

Route::middleware('auth:api')->group(function () {

    Route::controller(API_UserController::class)->group(function () {
        Route::get('user', 'show')->name('show');
        Route::post('user', 'update')->name('update');
    });

    Route::controller(API_CouponController::class)->group(function () {
        Route::post('coupons/buy/{coupon_id}', 'buy_coupon')->name('buy.coupon');
        Route::get('customer/coupons', 'my_coupons')->name('customer.coupons');
    });

    Route::controller(API_BookingController::class)->group(function () {
        Route::get('customer/bookings', 'my_bookings')->name('customer.bookings');
        Route::post('customer/service/book', 'book')->name('service.book');

        Route::post('customer/bookings/approve/{booking}', 'approve')->name('customer.booking.approve');
        Route::post('customer/bookings/reject/{booking}', 'reject')->name('customer.booking.reject');
    });


    Route::controller(API_ServiceController::class)->group(function () {
        Route::post('services/rate', 'rate_service')->name('service.rate');

        // Route::post('providers/rate', 'rate_provider')->name('provider.rate');
    });

    Route::controller(API_ProviderController::class)->group(function () {
        Route::get('provider-info/{provider_id}', 'provider_info')->name('provider.info');
        Route::post('providers/rate', 'rate_provider')->name('provider.rate');
    });


    Route::controller(API_ChatController::class)->group(function () {
        Route::post('chat-history', 'chat_data')->name('chat.history');
        Route::post('send-message', 'send_message')->name('message.send');
    });

    Route::controller(API_NotificationController::class)->group(function () {
        Route::get('custom-notifications', 'user_custom_notifications')->name('custom.notifications');
        Route::get('custom-notification/{custom_id}', 'custom_notification')->name('custom.notification');

        Route::get('booking-notifications', 'user_booking_notifications')->name('booking.notifications');
    });
});



//Route::post('/save-token', 'FCMController@index');
