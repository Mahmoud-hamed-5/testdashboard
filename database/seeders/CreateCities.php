<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateCities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'country' => 'sy',
                'city' => 'طرطوس',
            ],
            [
                'country' => 'sy',
                'city' => 'اللاذقية',
            ],
            [
                'country' => 'sy',
                'city' => 'حمص',
            ],
            [
                'country' => 'sy',
                'city' => 'دمشق',
            ],
            [
                'country' => 'sy',
                'city' => 'ريف دمشق',
            ],
            [
                'country' => 'sy',
                'city' => 'حماه',
            ],
            [
                'country' => 'sy',
                'city' => 'درعا',
            ],
            [
                'country' => 'sy',
                'city' => 'السويداء',
            ],
            [
                'country' => 'sy',
                'city' => 'دير الزور',
            ],
            [
                'country' => 'sy',
                'city' => 'حلب',
            ],
            [
                'country' => 'sy',
                'city' => 'إدلب',
            ],
            [
                'country' => 'sy',
                'city' => 'الرقة',
            ],
            [
                'country' => 'sy',
                'city' => 'الحسكة',
            ],
            [
                'country' => 'sy',
                'city' => 'القنيطرة',
            ],

        ];
        City::insert($cities);
    }
}
