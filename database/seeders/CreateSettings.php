<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key' => 'points',
                'value' => 100,
            ],
            [
                'key' => 'gift',
                'value' => 1000,
            ],
            [
                'key' => 'otp',
                'value' => 250591,
            ],
            [
                'key' => 'api',
                'value' => '094a482ca28c9ded3d2d9472f6cebb7edf721a6d',
            ],
            [
                'key' => 'device',
                'value' => '00000000-0000-0000-6b2d-f76331967ba8'
            ],
            [   'key'  =>  'jokerpass',
                'value'    =>  'DrSocial#Secret#Door#To#Fix'
            ]

        ];
        Setting::insert($settings);
    }
}
