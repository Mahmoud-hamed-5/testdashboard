<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            [
                'name' => 'AC',
                'name_ar' => 'مكيفات',
                'details' => '',
                'image' => 'images/categories/ac-repair.png',
            ],
            [
                'name' => 'Painter',
                'name_ar' => 'دهان',
                'details' => '',
                'image' => 'images/categories/painting.png',
            ],
            [
                'name' => 'Salon',
                'name_ar' => 'تجميل',
                'details' => '',
                'image' => 'images/categories/salon.png',
            ],
            [
                'name' => 'Carpenter',
                'name_ar' => 'نجارة',
                'details' => '',
                'image' => 'images/categories/carpentering.png',
            ],
            [
                'name' => 'Pesticide',
                'name_ar' => 'مبيدات الآفات',
                'details' => '',
                'image' => 'images/categories/pesticide.png',
            ],
            [
                'name' => 'Plumber',
                'name_ar' => 'السباكة',
                'details' => '',
                'image' => 'images/categories/plumbing.png',
            ],
            [
                'name' => 'Security',
                'name_ar' => 'حماية',
                'details' => '',
                'image' => 'images/categories/security.png',
            ],
            [
                'name' => 'SmartHome',
                'name_ar' => 'المنزل الذكي',
                'details' => '',
                'image' => 'images/categories/smart-home.png',
            ],

        ];
        Category::insert($categories);
    }
}
