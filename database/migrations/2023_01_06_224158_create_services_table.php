<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('provider_id');
            $table->integer('category_id');
            $table->integer('subcategory_id')->nullable();
            $table->longText('description')->nullable();
            $table->double('min_price')->default(0);
            $table->double('max_price')->default(0)->nullable();
            $table->double('discount')->default(0);


            $table->enum('status', ['On', 'Off'])->default('Off');
            $table->enum('type', ['Fixed', 'Variant'])->default('Fixed');
            $table->boolean('featured')->default(true);
            $table->string('image')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
};
