<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('country_code')->after('name')->nullable();
            $table->string('register_phone')->after('country_code')->unique()->nullable();
            $table->boolean('is_verified')->after('register_phone')->default(0);

            $table->enum('gender', ['Male', 'Female' , 'Other'])->default('Other');
            $table->enum('role', ['user', 'handyman', 'provider', 'admin'])->default('user');
            $table->string('phone', 15)->nullable();
            $table->string('address')->nullable();
            $table->enum('status', ['On', 'Off'])->default('On');
            $table->integer('points')->default(0);
            $table->string('user_image')->default('')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('country_code');
            $table->dropColumn('register_phone');
            $table->dropColumn('is_verified');
            
            $table->dropColumn('gender');
            $table->dropColumn('role');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('status');
            $table->dropColumn('points');
            $table->dropColumn('user_image');
        });
    }
};
