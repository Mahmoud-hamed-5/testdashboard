<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_id');
            $table->integer('user_id');
            $table->integer('provider_id');
            $table->integer('handyman_id')->nullable();

            $table->longText('description');

            $table->date('date');
            $table->time('time')->nullable();
            $table->time('min_time');
            $table->time('max_time');

            $table->string('phone');
            $table->string('address');

            $table->enum('state', ['Pending', 'Pending Approval', 'Approved', 'Ongoing', 'Completed', 'Cancelled'])->default('Pending');
            $table->double('amount')->default(0);
            $table->enum('payment_status', ['Pending', 'Paid'])->default('Pending');
            $table->longText('reason')->nullable();
            $table->string('coupon_code')->nullable();

            $table->double('lat');
            $table->double('lng');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
