<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->string('broadcast_ids')->default('')->nullable();
            $table->string('title')->default('')->nullable();
            $table->longText('content')->nullable();
            $table->integer('count')->default(0)->nullable();
            $table->enum('status', ['unseen', 'seen'])->default('unseen');
            $table->enum('type', ['Message', 'Notification', 'Booking', 'Custom']);
            $table->integer('booking_id')->nullable();
            $table->string('image')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
};
